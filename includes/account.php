<?php
if(is_user()) {
	if(isset($_GET["success"]))alert(text("changes_saved"),1);
	echo "<h2>".text('user2')." <b>".ucfirst(userinfo("username"))."</b> ".text('user3')."</h2>";
	if(isset($_POST['usersubmit'])&&!empty($_POST['usersubmit'])) {
		if($_FILES['profilepic']['name']!="" && pref("profilepics")!="no") {
			if($_FILES['profilepic']['type']=="image/jpeg")
				$ending=".jpeg";
			elseif($_FILES['profilepic']['type']=="image/png")
				$ending=".png";
			elseif($_FILES['profilepic']['type']=="image/gif")
				$ending=".gif";
			else {
				$ending=".jpg";
			}
			if(filesize($_FILES['profilepic']['tmp_name']) > 1500000) {
				alert($text["smaller_than"],1);
				$return=true;
			}
			if(!isset($return)) {
				if(!is_dir("files/profilepics/")) mkdir("files/profilepics/");
				$file = "files/profilepics/".time().rand(0,999999).$ending;
				$result = $db->query("select * from ".$db_prefix."user where id = '".userid()."'");
				while($row=$result->fetch_object()) {
					$trashfile=$row->profilepic;
				}
				if(move_uploaded_file($_FILES['profilepic']['tmp_name'], $file)) {
					if(conf("delete_trashfiles")) @unlink($trashfile);
					else {smkdir(conf("trashpath"));@rename($trashfile,conf("trashpath").time().rand(0,99999999999).ending("../".$trashfile));}
					copy($file,str_replace(".","hd.",$file));
					$db->query("update ".$db_prefix."user set `profilepic` = '".$file."' where id = '".userid()."'");
					imgscale($file,"50","50",$file);
					imgscale(str_replace(".","hd.",$file),"500","500",str_replace(".","hd.",$file));
					refresh("?inc=account&success");
				}
			}
		}
		if($_POST['pass1']!="") {
		if(conf("hash_pass_salt_activate"))$salt=conf("hash_pass_salt"); // salzen wir ein bisschen gegen regenbogen tabletten
			if($_POST['pass1']==$_POST['pass2'])$db->query("update ".$db_prefix."user set `password` = '".hash(conf("hash_algo"),$_POST['pass1'].$salt)."' where id = '".userid()."'");else{$return=1;}
		}
		if(!isset($return) && $db->query("update ".$db_prefix."user set `email` = '".$_POST['email']."' where id = '".userid()."'")) {
			alert(text("changes_saved"),1);
			$continue=true;
			addlog("aclogfile","Msg","BENUTZER: Benutzer Profil ".@$_POST['title']." bearbeitet.");
		}
		else {
			alert(text("error"),1);
			$continue=true;
			addlog("aclogfile","Msg","FEHLER: Benutzer Profil ".@$_POST['title']." nicht bearbeitet.");
		}
	}
	else {
		$continue=true;
	}
	if($continue) {
		echo '<form style="font-size:4.5mm;" action="?inc=account" method="post" enctype="multipart/form-data">';
		$result = $db->query("select * from ".$db_prefix."user where id = '".userid()."' limit 1");
		while($row = $result->fetch_assoc()) {
			$result2 = $db->query("select * from ".$db_prefix."groups order by name desc");
			if(pref("profilepics")!="no") {
				echo $text['profilepic']."<br>
				<input class='form-control' style=width:400px; type='file' name='profilepic' id='profilepic'>
				<input class='form-control form-block' style='width:400px;' name='usersubmit' value='".$text["save"]."' type='submit'><br>
				".text("email")."
				<input class='form-control' style='width:400px;border-radius:4px 4px 0px 0px;' type='email' name='email' value='".userinfo("email")."' placeholder='".$text['email']."'>
				<input class='form-control form-block' style='width:400px;' name='usersubmit' value='".$text["save"]."' type='submit'><br>
				";
			}
			echo ucfirst($text['new_password'])."<br>
			<input class='form-control' style='width:400px;border-radius:4px 4px 0px 0px;' type='password' name='pass1' placeholder='".$text['new_password']."'>
			<input class=form-control style='width:400px;margin-top:-1px;border-radius:0px 0px 4px 4px;' type='password' name='pass2' placeholder='".$text['password']." ".$text['repeat']."'>
			<input class='form-control form-block' style='width:400px;' name='usersubmit' value='".$text["save"]."' type='submit'>
			</form>";	
			}
	}
}
?>