<?php
class statistic {
	public function heading() {
		echo "<h1 style='display:inline'>".text("statistics")."</h1>
		<a href=.?inc=userstatistics><span style=float:right class='btn btn-default glyphicon glyphicon-home'></span></a><br><br>";
	}
	public function overview() {
		global $db,$db_prefix;
		echo "<div class=load-statistics><table style='background:white;border-radius:5px' class='table'>";
		if(userinfo("timestamp"))echo "<tr><td class=active>".text("registered")."</td><td>".date("d.m.Y H:i",userinfo("timestamp"))."</td></tr>";
		echo "<tr><td class=active>".text("last_login")."</td><td>".date("d.m.Y H:i",userinfo("last_login"))." <span onmouseup=\"$('.load-statistics').load('bin/ajax_hook.php?action=loaduserstats&stats=last_login');\" style='margin:-4px;float:right;' class='btn btn-default btn-sm glyphicon glyphicon-list'></span></td></tr>";
		echo "<tr><td class=active>".text("logins")."</td><td>".$db->query("select * from ".$db_prefix."statistics where action='login' and userid = '".userid()."'")->num_rows." <span onmouseup=\"$('.load-statistics').load('bin/ajax_hook.php?action=loaduserstats&stats=logins');\" style='margin:-4px;float:right;' class='btn btn-default btn-sm glyphicon glyphicon-list'></span></td></tr>";
		echo "<tr><td class=active>".text("comments")."</td><td>".$db->query("select * from ".$db_prefix."comments where userid = '".userid()."'")->num_rows." <span onmouseup=\"$('.load-statistics').load('bin/ajax_hook.php?action=loaduserstats&stats=comments');\" style='margin:-4px;float:right;' class='btn btn-default btn-sm glyphicon glyphicon-list'></span></td></tr>";
		if(is_allowed("articles"))
			echo "<tr><td class=active>".text("articles")."</td><td>".$db->query("select * from ".$db_prefix."articles where editor = '".userinfo("username")."'")->num_rows." <span onmouseup=\"$('.load-statistics').load('bin/ajax_hook.php?action=loaduserstats&stats=created_articles');\" style='margin:-4px;float:right;' class='btn btn-default btn-sm glyphicon glyphicon-list'></span></td></tr>";
		echo "<tr><td class=active>".text("viewed_articles")."</td><td>".$db->query("select * from ".$db_prefix."statistics where action='article' and userid = '".userid()."'")->num_rows." <span onmouseup=\"$('.load-statistics').load('bin/ajax_hook.php?action=loaduserstats&stats=articles');\" style='margin:-4px;float:right;' class='btn btn-default btn-sm glyphicon glyphicon-list'></span></td></tr>";
		echo "<tr><td class=active>".text("viewed_pages")."</td><td>".$db->query("select * from ".$db_prefix."statistics where action='page' and userid = '".userid()."'")->num_rows." <span onmouseup=\"$('.load-statistics').load('bin/ajax_hook.php?action=loaduserstats&stats=pages');\" style='margin:-4px;float:right;' class='btn btn-default btn-sm glyphicon glyphicon-list'></span></td></tr>";
		echo "<tr><td class=active>".text("searches")."</td><td>".$db->query("select * from ".$db_prefix."statistics where action='search' and userid = '".userid()."'")->num_rows." <span onmouseup=\"$('.load-statistics').load('bin/ajax_hook.php?action=loaduserstats&stats=searches');\" style='margin:-4px;float:right;' class='btn btn-default btn-sm glyphicon glyphicon-list'></span></td></tr>";
		echo "</table></div>";
	}
}
$statistic=new statistic();
$statistic->heading();
$statistic->overview();
?>