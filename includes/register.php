<?php
if(isset($_POST['register']) && isset($_POST['email'])) {
	$user=new user();
	$result = $db->query("select * from ".$db_prefix."user where username like '".$_POST['username']."' limit 1");
	$out=null;
	while($row = $result->fetch_assoc()) {
		$out.=" ".$text["username_is_already_there"];
		$already=true;
	} if(strstr($_POST["username"],"admin") || strstr($_POST["username"],"hack")) {
		$out.=" ".$text["username_is_already_there"];
		$already=true;
	} if(!preg_match("/^[a-zA-Z0-9]+$/s",$_POST["username"])) {
		$out.=" ".$text["username_is_not_avaiable"];
		$already=true;
	} if(!preg_match("/^[a-zA-Z0-9]+$/s",$_POST["username"])) {
		$out.=" ".$text["username_is_not_avaiable"];
		$already=true;
	} if(strlen($_POST['username']) < 3) {
		$out.=" ".$text['username_not_empty'];
		$already=true;
	} if(strlen($_POST['password']) < 3) {
		$out.=" ".$text['password2short'];
		$already=true;
	} if($_POST['password']!=$_POST['password2']) {
		$out.=" ".ucfirst(text('passwords_do_not_match'));
		$already=true;
	} if(!isset($already)) {
		if($user->register($_POST['username'],$_POST['password'],$_POST['name'],$_POST['email'],$_POST["profilepic"])) {
			echo "<div class='alert alert-success'>".$text["registered_success"]."</div>";
			$form=false;
			return;
		}
		else {
			echo $text["register_failed"]."<br>";	
			$form=true;		
		}
	}
}
if(isset($out))alert($out,1);
if(!isset($form)) {
echo '<form id="registerform" style="width:343px" action="?inc=login" method="post" id="login"><h2>'.ucfirst($text["register"]).'</h2>
<input class="form-control form-block" name=name placeholder="'.ucfirst($text["name"]).'" value="'.@$_POST["name"].'" required>
<div class="input-group form-block input-username">
	<input class="form-control" onkeyup="'."var username=$(this).val();$.get('bin/ajax_hook.php?action=check_avaiability_username&username='+username,function(avaiable){if(avaiable=='1'){\$('.glyphicon-username-avaiable').addClass('glyphicon-ok').removeClass('glyphicon-remove');$('.input-username').removeClass('has-error');$('.register-submit').removeAttr('disabled');}else{\$('.glyphicon-username-avaiable').removeClass('glyphicon-ok').addClass('glyphicon-remove');$('.input-username').addClass('has-error');$('.register-submit').attr('disabled','true');}});".'" name=username placeholder="'.ucfirst($text["username"]).'" value="'.@$_POST["username"].'" required>
	<span class="input-group-addon">
		<div><span class="glyphicon glyphicon-username-avaiable glyphicon-remove"></span></div>
	</span>
</div>
<input class="form-control form-block" name=email type="email" placeholder="'.ucfirst($text["email"]).'" value="'.@$_POST["email"].'" required>
<input class="form-control form-block password1" name=password type="password" placeholder="'.ucfirst($text["12"]).'" value="'.@$_POST["password"].'" required>
<div class="form-group has-feedback" style=margin-bottom:0;>
	<input class="form-control form-block password2" name=password2 onkeyup="'."if($('.password1').val()==$('.password2').val()){\$(this).parent().removeClass('has-error');\$(this).next('span').fadeOut();}else{\$(this).parent().addClass('has-error');\$(this).next('span').fadeIn();}".'" type="password" placeholder="'.ucfirst($text["12"])." ".$text["repeat"].'" required>
	<span style=display:none class="glyphicon glyphicon-remove form-control-feedback"></span>
</div>'; 
if(pref("profilepics")!="no") {
	//ucfirst($text["choose_profilepic"]).
	echo '<div id="profilepics" class="register-profilepics-choose">';
	$visible="visible ";
	for($i=1;$i < 7;$i++) {
		echo '<div class="radio-select"><div class="'.$visible.'profilepic changeopacity"'." onmouseup=\"$('.visible').removeClass('visible');$('#profilepic').val('templates/main/images/profilepic".$i.".jpg');$(this).addClass('visible');\" style='cursor:pointer;opacity:.3;margin-left:.5px;width:35px;height:35px;background-image:url(templates/main/images/profilepic".$i.".jpg)';></div></div>";
		$visible=null;
	}
	echo '</div>';
}
echo '<input type=hidden name=profilepic id=profilepic value=templates/main/images/profilepic1.jpg></input>
<input type=submit ';if(!isset($_POST["username"]))echo"disabled";echo' class="btn btn-default register-submit" value="'.ucfirst($text["register"]).'" name="register">
</form>';
}
?>