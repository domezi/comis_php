<?php
include_once('lib/global_functions.php');

include_once('includes/header.php');
include_once('includes/header_exec.php');

$c_hook->call("comis_header");

if(is_sudo() || pref("maintenance")!="yes") {
	$addon=glob("addons/*.mainpage.php");if($addon){foreach($addon as $file)include$file;}
	require("bin/sethooks.php");


	$c_hook->call("template_header");
	$headerfile="templates/".pref('website_template')."/header.php";
	if(is_file($headerfile)) {
		require($headerfile);
	} else {
		require "templates/main/header.php";
		$c_system->load_style("templates/".pref('website_template')."/style.css");
	}
					

	$mainpage=new mainpage();
	$nav=new nav();
	$aside = new aside();
	$comismenu=new comismenu();


	echo "</head>\n<body>\n\t<div id=\"mainpage\" class=\"main-wrapper\"><div class=\"c-header\">\n\t";
	
	$c_hook->call("mainpage");
	require("templates/".pref('website_template')."/mainpage.php");
	
	
		
} else {
	include("includes/maintenance.php");
	$c_hook->call("maintenance");
}

if(is_allowed("articles")) echo '<script type="text/javascript" src="lib/ckeditor/ckeditor.js"></script><link href="lib/ckeditor/contents.css" rel="stylesheet" type="text/css">';	

$c_hook->call("footer");

echo "<footer>";
	$footerfile="templates/".pref('website_template')."/footer.php";
	if(!is_file($footerfile)) $footerfile="templates/main/footer.php";
	include($footerfile);
echo"</footer>";
echo"</body></html>";

?>
