<?php
	$c_system->load_style("lib/bootstrap.css");
	$c_system->load_script("lib/bootstrap.js");
?>
<header>
	<?php if(pref("headpicture_yn")!="no")echo'<img src="'.$mainpage->mkheadpicture(@$_GET["page"],@$_GET["article"]).'" class="img-headpicture" width="100%" alt=headpicture>';?>
	<nav class="navbar navbar-default navbar-fixed-top">
	  <div class="container-fluid">
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
	        <span class="sr-only">Menu</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="."><img style="float:left;display:inline;height:40px;width:40px;margin:-10px 5px -7px -10px;" src="<?php show('shortcut');?>"><?php show("website_title");?></a>
	    </div>
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav navbar-main">
				<?php
					$c_gui->nav_menu($_GET["article"],$_GET["page"],$_GET["inc"]);
				?>
	      </ul>
	      <?php if(pref('search')=="yes") { ?>
			<form class="navbar-form navbar-left hidden-xs hidden-sm" role="search" action=".">
			<div class="input-group">
				<input type="text" list="suggests" name="q" class="form-control" autocomplete="off" placeholder="<?php echo text('search');?>" required="">
				<span class="input-group-btn">
					<button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span></button>
				</span>
	        </div>
	      </form>
	      <?php } ?>	
			<ul class="nav navbar-nav navbar-right">
				<?php
					$c_gui->addcontentmenu();
					$c_gui->loginmenu();
				?>
	      </ul>
	    </div>
	  </div>
	</nav>
</header>
</div>
<br><br>
<br class="clear">
<div class="content-wrapper">
	<div class="row" style="margin-right:-60px">
		<div class="article col-md-9">
			<?php
				if(is_home()) {
					$c_gui->createhomepage();
				}
			
				$c_gui->searchpage();
				$c_system->inc($_GET["inc"]);
				$c_gui->article($_GET['article'],array("header"=>true,"attachments"=>true,"show_editicons"=>true,"comments"=>true));
				$c_gui->page($_GET["page"]);
			?>
		</div><div class="aside col-xs-3">
			<?php if(pref("aside")!="no") {
				$c_gui->aside();
			} ?>
		</div>
	</div>
</div>
<br class="clear">
</div>
