<?php
echo "<!DOCTYPE html>\n<html>\n<head>\n\t<title>".pref("website_title")."</title>\n\t";
echo '<meta name="keywords" content="'.pref('website_keywords').'">'."\n\t";
echo '<meta name="description" content="'.pref('website_description').'">
	<script src="lib/jquery.js" type="text/javascript"></script>
	<script src="lib/jquery-ui.js"></script>
	<link rel="stylesheet" href="lib/jquery-ui-smoothness.css" />
	<link rel="stylesheet" href="lib/jquery-ui.theme.css">
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8">
	<meta http-equiv="content-style-type" content="text/css">
	<link rel="shortcut icon" href="'.pref('shortcut').'" type="image/x-icon"/>
	<script type="text/javascript" src="lib/global_functions.js"></script>';
$dont_change_on_auto_update=false;
if(is_sudo() && isset($_GET["conf"]) && in_array($_GET["conf"],explode(",","magic_area_info,contextmenu"))){if(!conf($_GET["conf"],$_GET["val"]))error(201502011402);}
if(is_sudo() && conf("developer")) {echo"<pre class=devinfo style='margin-top:-10px;margin-left:-8px;padding-left:6px;padding-top:6px;font-size:11px;line-height:11px;position:fixed;letter-spacing:-.6px;max-width:100%;background:hsla(0,0%,100%,.8);max-height:100%;overflow:auto;z-index:99999999999;'><h2 style=margin-bottom:0>DEV INFO</h2>";$tmp=file("etc/about.conf");echo $tmp[1]."<b>\$_COOKIES</b>\n";print_r($_COOKIE);echo "<b>\$_FILES</b>\n";print_r($_FILES);echo "<b>\$_POST</b>\n";print_r($_POST);echo "<b>\$_SESSION</b>\n";print_r($_SESSION);echo "<b>\$_GET</b>\n";print_r($_GET);echo "<b>\$_SERVER <a style=cursor:pointer onmouseup=\"$('#developerinfoserver2434235').toggle();\">(toggle)</a></b>\n<span style=display:none id='developerinfoserver2434235'>";print_r($_SERVER);echo"</span></pre>";}
$style=null;$log=null;
echo '<link href="templates/main/style.css" rel="stylesheet" type="text/css">';

echo "<style>";
if(pref("headpicture")!="images/headpicture.jpeg") $important=" !important";
echo ".topnav{margin-bottom:10px;}.headpicture h1{padding:45px 40px;text-shadow:-2px -2px 22px white,-2px 2px 22px white,2px -2px 22px white,2px 2px 22px white;}.headpicture{height:220px;margin:0px -41px;margin-bottom:-50px;margin-top:-20px;border-radius:5px 5px 0px 0px;background-image:url('".pref("headpicture")."')".$important.";box-shadow: inset 0px 0px 30px -10px hsla(0,0%,0%,.6);background-position:center bottom;background-size:cover;}.blur{-webkit-filter: blur(6px); -moz-filter: blur(6px); -o-filter: blur(6px); -ms-filter: blur(6px); filter: blur(6px);}.headpicture.blur{margin:1%;margin-top:55px;height:50px;position:relative;z-index:1;box-shadow:0px 0px 0px;background-position:center bottom;}.topnav {position:relative;z-index:1;}";
echo ".topnav li{box-shadow:0px 0px 8px 0px hsla(0,0%,0%,.1),inset 0px 0px 10px 0px hsla(0,0%,100%,.25);}";
if(file_exists(pref("background")) && pref("background_yn")!="no")echo "body{background-position:center center;background-image:url('".pref("background")."');background-attachement:fixed;}";
echo "</style>";
?>

	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
	<meta name="apple-mobile-web-app-status-bar-style" content="white">
	<link rel="apple-touch-icon" href="<?php show('shortcut'); ?>">
	<script src="lib/mainpage.js"></script>
	<script src="lib/bootbox.js"></script>
