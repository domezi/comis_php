<?php require("header_install.php");?>
<div style="margin:auto;width:600px;margin-top:90px;">
<?php 
echo '
<script type="text/javascript">
$(function(){$("#continuebtn").html(\'<button type="submit" class="btn btn-warning">'.$text['8'].'</button>\');});
$(document).keyup(function(){passwdcompare();});
function passwdcompare() {
	if($("#admin_passwd1").val()!="") {
		if ($("#admin_passwd1").val()==$("#admin_passwd2").val()) {
			$(".pwcheck").css("background","#0f0");
			$("#continuebtn").html(\'<button type="submit" class="btn btn-success">'.$text['8'].'</button>\');
		}
		else {
			$(".pwcheck").css("background","orange");
			$("#continuebtn").html(\'<button type="submit" class="btn btn-warning">'.$text['8'].'</button>\');
		}
	}
}
</script>';
include_once("../lib/import_function.php");


if(isset($_POST["db_username"])) {
	$_SESSION['db_prefix']=str_replace(".","",str_replace("_", "", $_POST['db_prefix']));
	$_SESSION["db_host"]=$_POST["db_host"];
	$_SESSION["db_username"]=$_POST["db_username"];
	$_SESSION["db_password"]=$_POST["db_password"];
	$_SESSION["db_name"]=$_POST["db_name"];
}

if(!@mysqli_connect($_SESSION['db_host'],$_SESSION['db_username'],$_SESSION['db_password'],$_SESSION['db_name'])) {
	echo "<center><br><p style=font-size:200px;color:white;transform:rotateZ(80deg);-webkit-transform:rotateZ(80deg)>:(</p><br><h2 style=position:relative;z-index:9999; class=err>".$text['14']."</a>";
	return;
}


if($_SESSION['db_prefix'] != "" && strlen($_SESSION['db_prefix']) < 128 && !strstr($_SESSION['db_prefix'],"'") && !strstr($_SESSION['db_prefix']," ") && !strstr($_SESSION['db_prefix'],"/") && !strstr($_SESSION['db_prefix']," * ") && strlen($_SESSION['db_prefix']) > 1) {
	$_SESSION['db_prefix']=str_replace("-", "", $_SESSION['db_prefix']);
	$_SESSION['db_prefix']=$_SESSION['db_prefix'];
} else {
	$_SESSION['db_prefix']="c".substr(hash(conf("hash_algo"),rand(1,999999)), rand(1,5), 8);
}


$avaiable_timezones=import_conf("../etc/avaiable_timezones.list");


echo '
<center>
<h1 style=color:white;>'.$text['0'].'<h1>
</center>
<hr style=width:500px;>
<form style="width:500px;margin:auto;" role="form" method="post" action="write_conf.php" class="installform-inst">
	<b>'.$text['1'].'</b><br>
	<input type="text" class="form-control" name="website_title" placeholder="'.$text['2'].'" value="'.$install_preconf["website_title"].'">
	<input type="text" class="form-control" name="website_description" placeholder="'.$text['3'].'">
	<br><b>'.$text['4'].'</b>
	<input type="text" class="form-control" name="user_name" id="user_name" placeholder="'.$text['5'].'" value="'.$install_preconf["user_name"].'">
	<input type="email" class="form-control" autocomplete="off" name="user_email" placeholder="'.$text['6'].'" value="'.$install_preconf["user_email"].'">
	<input class="form-control" autocomplete="off" name="loginname" placeholder="'.text("username").'" required>
	<input type="password" class="form-control" autocomplete="off" name="admin_passwd1" id="admin_passwd1" placeholder="'.$text['7'].'" required>
	<div class=pwcheck></div>
	<input type="password" class="form-control" autocomplete="off" name="admin_passwd2" id="admin_passwd2" placeholder="'.$text['7'].' '.$text['repeat'].'" required>
	<div class=pwcheck></div>
	<!--<div class="passwdsec" style=height:18px;width:100%;border-radius:background:lightgray;margin-bottom:10px;margin-top:-5px;padding-top:8px;><div id="passwdcompare"></div></div>-->
	<!--<br><b>'.text("timezone").'</b>
	<select name="timezone" style="margin-left:10px;margin-bottom:5px;width:150px;padding:0;color:white;>';
	foreach($avaiable_timezones as $timezone) {
   	if(str_replace(PHP_EOL,null,$install_preconf["timezone"])==str_replace(PHP_EOL,null,$timezone)) {
     		echo '<option selected value="'.$timezone.'">'.$timezone.'</option>';
  		}
   	else {
     		echo '<option value="'.$timezone.'">'.$timezone.'</option>';
  		}
	}
   echo '
  	</select>--><br>
  ';
  if($_SESSION['l'] == "de") {
	  echo '
		<input style=margin-top:0px; type=checkbox value=yes checked name=create_placeholders>
		<b>Beispielinhalte installieren (empfohlen)</b><br>
  		<input style=margin-top:0px; type=hidden value=no name=keep_tables>
  		';
	}
	else {
	  echo '
  		<input style=margin-top:0px; type="checkbox" value="yes" checked name="create_placeholders">
		<b>Install Examplecontent (recommended)</b><br>
  		<input style=margin-top:0px; type=hidden value=no name=keep_tables>
  		';
	}
  echo '<hr style=width:500px;>
	<a href=setup_db.php><button class="btn eee btn-default">'.text("back").'</button></a>
	<div id="continuebtn" style=float:right><button type="submit" class="btn btn-primary">'.$text['8'].'</button></div>
</form>
</body>
';

?>
