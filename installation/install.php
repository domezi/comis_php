<?php require("header_install.php");
if(!file_exists("../etc/dbcon.php")) die("<h1 style=text-align:center;margin-top:50px;color:white>File etc/dbcon.php required! Couldn't find file. <a href=write_conf.php>Go back!</a>");
?>
<style type="text/css">a {color:white;}a:hover{/*text-shadow:0px 0px 10px black;*/color:blue;text-decoration: none;}</style>
<?php 
if(is_file("update.php")) {
	rename("update.php","../files/trash/update_".md5(rand(0,9e5)).".php");
}
?>
<div style="background:hsla(0,0%,100%,.1);position:fixed;top:0;left:0;width:100%;height:100%;">
<div style="margin:5%;height:500px;">
<center>
<?php
require_once("../db.php");

if(conf("hash_pass_salt_activate"))$salt=conf("hash_pass_salt");


if(!isset($_SESSION['keep_tables']) || $_SESSION['keep_tables']!="yes") {
	$db->query("truncate `".$db_prefix."articles`");
	$db->query("truncate `".$db_prefix."user`");
	$db->query("truncate `".$db_prefix."pages`");
	$db->query("truncate `".$db_prefix."preferences`");
	$db->query("truncate `".$db_prefix."logins`");
	$db->query("truncate `".$db_prefix."comments`");
	$db->query("truncate `".$db_prefix."groups`");
}

$create="please";
include("create.php");

if($db->query("insert into `".$db_prefix."user`(timestamp,`admin`,`id`,`name`, `username`, `password`, `email`, hash) values('".time()."','1','1','".$_SESSION['user_name']."','".$_SESSION['loginname']."','".hash(conf("hash_algo"),$_SESSION['admin_passwd1'].$salt)."', '".$_SESSION['user_email']."', '".hash(conf("hash_algo"),rand(1,99999999999999999999)).uniqid()."')"))
$install = true;


if($_SESSION["l"]=="de")
$db->query("
INSERT INTO `".$db_prefix."user` (`id`, `name`, `username`, `email`, `password`, `groupid`, `deactive`, `timestamp`, `last_login`, `icon`, `newsletter`, hash) VALUES
(2, 'Rainer Zufall', 'rainer', 'rainer.zufall@irgendwas.de', '50570a67f16cae628b7648ce4dddf4459a023f536ea87c99d8be2be937733c897d54f988c89fc89979935a6c07c7ddea0f70cf35e17a6293c6a31b0f42652423', '0', '', ".time().", 0, '', 'no', '50570a67f16cae628b7648ce4dddf4459a023f536ea87c99d8be2be937733c897d54f988c89fc89979935a6c07c7ddea0f70cf35e17a6293c6a31b0f42652423'),
(3, 'Max Mustermann', 'max', 'max.musterman@domain.com', '50570a67f16cae628b7648ce4dddf4459a023f536ea87c99d8be2be937733c897d54f988c89fc89979935a6c07c7ddea0f70cf35e17a6293c6a31b0f42652423', '1', '', ".time().", 0, '', 'no', '50570a67f16cae628b7648ce4dddf4459a023f536ea87c99d8be2be937733c897d54f988c89fc89979935a6c07c7ddea0f70cf35e17a6293c6a31b0f42652423');
");
else
$db->query("
INSERT INTO `".$db_prefix."user` (`id`, `name`, `username`, `email`, `password`, `groupid`, `deactive`, `timestamp`, `last_login`, `icon`, `newsletter`, hash) VALUES
(2, 'John Miller', 'john', 'johnny@something.com', '50570a67f16cae628b7648ce4dddf4459a023f536ea87c99d8be2be937733c897d54f988c89fc89979935a6c07c7ddea0f70cf35e17a6293c6a31b0f42652423', 0, '', ".time().", 0, '', 'no', '50570a67f16cae628b7648ce4dddf4459a023f536ea87c99d8be2be937733c897d54f988c89fc89979935a6c07c7ddea0f70cf35e17a6293c6a31b0f42652423'),
(3, 'David Smith', 'david', 'david.smith@domain.com', '50570a67f16cae628b7648ce4dddf4459a023f536ea87c99d8be2be937733c897d54f988c89fc89979935a6c07c7ddea0f70cf35e17a6293c6a31b0f42652423', 0, '', ".time().", 0, '', 'no', '50570a67f16cae628b7648ce4dddf4459a023f536ea87c99d8be2be937733c897d54f988c89fc89979935a6c07c7ddea0f70cf35e17a6293c6a31b0f42652423');
");
$db->query("alter table `".$db_prefix."articles` add fulltext(`code`)");
$db->query("alter table `".$db_prefix."comments` add fulltext(`msg`)");
$db->query("insert into `".$db_prefix."preferences`(`name`, `value`) values('welcome_msg','".$text["0"]."')");
$db->query("insert into `".$db_prefix."preferences`(`name`, `value`) values('website_title','".$_SESSION['website_title']."')");	
$db->query("insert into `".$db_prefix."preferences`(`name`, `value`) values('website_description','".$_SESSION['website_description']."')");	
$db->query("insert into `".$db_prefix."preferences`(`name`, `value`) values('contact_person','1')");	
$db->query("insert into `".$db_prefix."preferences`(`name`, `value`) values('website_template','icecube')");	
$db->query("insert into `".$db_prefix."preferences`(`name`, `value`) values('admin_template','icecube')");
$db->query("insert into `".$db_prefix."preferences`(`name`, `value`) values('aside','yes')");
$db->query("insert into `".$db_prefix."preferences`(`name`, `value`) values('search','yes')");
$db->query("insert into `".$db_prefix."preferences`(`name`, `value`) values('pages_enabled','yes')");
$db->query("insert into `".$db_prefix."preferences`(`name`, `value`) values('language','".$_SESSION['l']."')");	
$db->query("insert into `".$db_prefix."preferences`(`name`, `value`) values('welcome_img','')");	
$db->query("insert into `".$db_prefix."preferences`(`name`, `value`) values('welcome_img_yn','yes')");	
$db->query("insert into `".$db_prefix."preferences`(`name`, `value`) values('headpicture','images/headpicture.jpeg')");	
$db->query("insert into `".$db_prefix."preferences`(`name`, `value`) values('headpicture_yn','yes')");	
$db->query("insert into `".$db_prefix."preferences`(`name`, `value`) values('background_yn','no')");	
$db->query("insert into `".$db_prefix."preferences`(`name`, `value`) values('maintenance','no')");	
$db->query("insert into `".$db_prefix."preferences`(`name`, `value`) values('maintenance_header','".text("maintenance")."')");	
$db->query("insert into `".$db_prefix."preferences`(`name`, `value`) values('maintenance_body','".text("maintenance_body_preset")."')");	
$db->query("insert into `".$db_prefix."preferences`(`name`, `value`) values('background','')");	
$db->query("insert into `".$db_prefix."preferences`(`name`, `value`) values('color_theme','')");	
$db->query("insert into `".$db_prefix."preferences`(`name`, `value`) values('homepage','homefeed')");	
$db->query("insert into `".$db_prefix."preferences`(`name`, `value`) values('comments','yes')");	
$db->query("insert into `".$db_prefix."preferences`(`name`, `value`) values('attachments','true')");	
$db->query("insert into `".$db_prefix."preferences`(`name`, `value`) values('profilepics','yes')");	
$db->query("insert into `".$db_prefix."preferences`(`name`, `value`) values('shortcut','images/favicon.ico')");

$db->query("insert into `".$db_prefix."articles`(`title`, `code`) values('".$text["contact"]."','".$_SESSION["user_name"]."<br><a href=\"mailto:".$_SESSION["user_email"]."\">".$_SESSION["user_email"]."</a>')");

$result=$db->query("select * from `".$db_prefix."user` where admin='1'");
if($result)while($row=$result->fetch_object()) {$admin_acc=true;}

if(!$admin_acc) {die("<br><br><br><h1 style=\"text-align:center;\">ADMIN ACCOUNT WAS NOT CREATED! FATAL ERROR!</h1><br><br><br>");$install=false;}
if($install) {echo "<meta http-equiv=refresh content=0,install_done.php?install>";}
else {echo "<meta http-equiv=refresh content=0,install_done.php>";}

$clean=new cleaner();
$clean->alias();
?>

