<?php require("header_install.php");
?>
<script type="text/javascript">
function dbcheck() {$("#dbcheck").load("dbcheck.php?db_host="+$("#db_host").val()+"&db_username="+$("#db_username").val()+"&db_name="+$("#db_name").val()+"&db_password="+$("#db_password").val());}
$(document).keyup(function(){dbcheck();});
</script>


<div style="margin:auto;width:600px;margin-top:90px;">
	<?php 
	include_once("../lib/import_function.php");
	include_once("../lib/global_functions.php");
	echo '
	<h1 style=text-align:center;color:white;>'.$text['0'].'</h1>
	</center>
	<hr style=width:500px;>
	<form style="width:500px;margin:auto;" role="form" method="post" action="setup_install.php" class="installform-db">
	<p style="color:white;font-style:italic;text-align:justify">'.$text['32'].'</p>
	<br><b>'.$text['10'].'</b>
	<input type="text" class="form-control" value="'.$db_preconf["db_host"].'" id="db_host" name="db_host" placeholder="'.$text['10'].'" required>
	<br><b>'.$text['11'].'</b>
	<input type="text" class="form-control" value="'.$db_preconf["db_username"].'" autocomplete="off" id="db_username" name="db_username" placeholder="'.$text['11'].'" required>
	<br><b>'.$text['12'].'</b>
	<input type="password" class="form-control" value="" autocomplete="off" id="db_password" name="db_password" placeholder="'.$text['12'].'" required>
	<br><b>'.$text['13'].'</b>
	<input type="text" class="form-control" value="'.$db_preconf["db_name"].'" id="db_name" name="db_name" placeholder="'.$text['13'].'" required>
	<br><b>DB-PREFIX (Nur Buchstaben und Zahlen, Optional)</b>
	<input type="text" class="form-control" value="'.str_replace("_","",$db_preconf["db_prefix"]).'" name="db_prefix" placeholder="DB-PREFIX (Optional)">
	<hr style=width:500px;>
	<a href=index.php><button class="btn eee btn-default">'.text("back").'</button></a>
	<div id="dbcheck" style=float:right>
	<button type="submit" class="btn btn-warning">'.$text['8'].'</button>
	</div><br>
	</form>
	</body>
	';
	?>
</div>
