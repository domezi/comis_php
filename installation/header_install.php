<?php

if(is_file("../install"))rename("../install","../INSTALL");
if(is_file("../Install"))rename("../Install","../INSTALL");
if(is_file("../INSTALL.txt"))rename("../INSTALL.txt","../INSTALL");
if(is_file("../install.txt"))rename("../install.txt","../INSTALL");
if(!is_file("../INSTALL")) die("<p style=font-size:40px;margin:50px>Please create an empty file named 'INSTALL' in the same directory as the one where you can find the 'RELEASE_NOTE' file in order to install comis. <br><br><a href=../>&lt; OK > </a></p><br><br><hr>comis");

session_start();

if(!file_exists("../language/".$_SESSION['l'].".php"))
	$_SESSION['l']="en";

include("../language/".$_SESSION['l'].".php");

?><!DOCTYPE html>
<html>
<head>
	<title>COMIS INSTALLATION</title>
	<meta name="ROBOTS" content="NOINDEX, NOFOLLOW">
	<link rel="stylesheet" href="../lib/bootstrap.css">
	<link rel="stylesheet" href="../templates/main/style.css">
	<link rel="stylesheet" href="install.css">
	<script type="text/javascript" src="../lib/jquery.js"></script>
	<script type="text/javascript" src="../lib/jquery-ui.js"></script><?php

	require_once("../lib/global_functions.php");

	if(isset($_GET["abort"])) {unlink("../INSTALL");refresh('../');}

?>


</head>
<body>
	<div class="inst-top"></div><div class="inst-bottom"></div>
