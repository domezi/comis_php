function load(div,link) {
	$(div).load(link);
}
function alertconfirm(msg,link) {
if(confirm(msg)){window.location.href=link;}
}
function text(str) {
	return str;
}
function adminfullscreen(im,tmp) {
	if (im==true) {
		$(".adminfullscreen").addClass("adminfullscreen-true");
		$("#adminheader").animate({opacity: 0,top: "-=100"},0);
		$(".leftnav").animate({opacity: 0,left: "-=250"},0);
		$("#adminheader").hide();$(".leftnav").hide();
			$("#admincontainer").animate({left: "0px",bottom: "0px",right: "0px",top: "0px"}, 0);
	}
	else {
		if ($(".adminfullscreen").hasClass("adminfullscreen-true")) {
			if(tmp==false){$(".adminfullscreen").load("../bin/ajax_store_data.php?var=admin_fullscreen_active&val=false");}
			$(".adminfullscreen").removeClass("adminfullscreen-true");
			$("#adminheader").animate({opacity: 1,top: "+=100"}, 900);
			$(".leftnav").animate({opacity: 1,left: "+=250"}, 900);
			$("#adminheader").show();$(".leftnav").show();
			$("#admincontainer").animate({left: "235px",bottom: "0px",right: "0px",top: "75px"}, 800);
		}
		else {
			if(tmp==false){$(".adminfullscreen").load("../bin/ajax_store_data.php?var=admin_fullscreen_active&val=true");}
			$(".adminfullscreen").addClass("adminfullscreen-true");
			$("#adminheader").animate({opacity: 0,top: "-=100"}, 800);
			$(".leftnav").animate({opacity: 0,left: "-=250"}, 800);
			setTimeout(function () {$("#adminheader").hide();$(".leftnav").hide();},1000);
			$("#admincontainer").animate({left: "0px",bottom: "0px",right: "0px",top: "0px"}, 900);
		}
	}
}
function orderwidgets() {
	$(".orderwidgets-dialog").dialog();
	$( ".orderwidgets-widgets" ).sortable({
		items: "li:not(.iliketomoveitmoveit)",
	   update: function( event, ui ) {
         var order = $(this).sortable('toArray').toString();
         get="reorder.php?order="+order+"&global";
         alert("Es wird versucht, die Anderungen zu ubernehmen, dabei treten momentan noch oft Fehler auf.");
         $.get(get);
	   }
	});
}