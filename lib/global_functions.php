<?php
if(@file_exists("db.php"))$path="";elseif(@file_exists("../db.php"))$path="../";elseif(@file_exists("../../db.php"))$path="../../";
function pref($name) {
	global $db,$db_prefix;
	$value=null;
	if($db) {
		$result = $db->query("select value from `".$db_prefix."preferences` where name = '".$name."'");
		if($result) {
			while($row = $result->fetch_object()) {
				$value = $row->value;
			}
		}
	}
	return $value;
}
function show($name) {
	global $db,$db_prefix;$value=null;
	$result = $db->query("select * from `".$db_prefix."preferences` where name = '".$name."'");
	while($row = $result->fetch_object()) {
		$value = $row->value;
	}
	echo $value;
}
function feedback($msg=null) {
//	global $path;
//	$private=import_conf($path."etc/private.conf");if($msg!=null)$msg=", ".$msg;
//	@file_get_contents("http://domiscms.de/feedback.php?msg=".base64_encode(base64_encode("ID(".$private["id"]."), HTTP(".@$_SERVER["HTTP_REFERER"].")".$msg)));
}
function addlog($log,$action,$value,$userid=null) {
	global $db,$db_prefix;
	if($userid==null)$userid=userid();
//	if($data!=null)$data=json_encode($data);
//	alert("hi");
	if(is_object($db))$db->query("insert into ".$db_prefix."statistics (`action`, `value`, `userid`, `ip`, `timestamp`,`user_agent`) values ('".$action."', '".$value."', '".$userid."', '".getip()."', '".time()."', '".$_SERVER["HTTP_USER_AGENT"]."')");
/*	global $path;
	$logfile=conf($log);
	$log=@file_get_contents($path.$logfile,$log)."\n".time()."#".$type."(".$msg."), IP(".$_SERVER['REMOTE_ADDR']."), Userid(".@$_COOKIE["userid"]."), Username(".@userinfo("username").")";
	file_put_contents($path.$logfile,$log);*/
}
function alert($msg,$bootbox=false) {
	echo "<script>";
	if($bootbox==true)echo"bootbox.";
	echo "alert(\"".$msg."\");</script>";
}
function script($script,$ready=true) {
	echo "<script>";
	if($ready==true)echo "$(function() {";
	echo $script;
	if($ready==true)echo "});";
	echo "</script>";
}
function parse_user_agent($user_agent) {
	if(strstr(strtolower($user_agent),"edge"))$user_agent="edge";
	elseif(strstr(strtolower($user_agent),"opera"))$user_agent="opera";
	elseif(strstr(strtolower($user_agent),"seamonkey"))$user_agent="seamonkey";
	elseif(strstr(strtolower($user_agent),"firefox"))$user_agent="firefox";
	elseif(strstr(strtolower($user_agent),"chrome"))$user_agent="chrome";
	elseif(strstr(strtolower($user_agent),"safari"))$user_agent="safari";
	else$user_agent=text("other");
	return $user_agent;
}
function get_os($user_agent) {
	if(strstr(strtolower($user_agent),"ubuntu") || strstr(strtolower($user_agent),"linux"))$user_agent="linux";
	elseif(strstr(strtolower($user_agent),"apple"))$user_agent="apple";
	elseif(strstr(strtolower($user_agent),"windows"))$user_agent="windows";
	else$user_agent=text("unknown");
	return $user_agent;
}
function show_editicons($id,$pageid=NULL) {
	global $text,$db,$db_prefix;
	$result2=$db->query("select public,comments,page,header,footer from ".$db_prefix."articles where id = '".$id."'");
	if($result2->num_rows) {
		while($row2 = $result2->fetch_object()) {
			$public=($row2->public!="yes")?array("true","-open","public"):array("false","-close","private");
			$comments=($row2->comments!="yes")?array("true","comments_on_short"):array("false","comments_off_short");
			$page=($row2->page!="yes")?array("true","page_on_short"):array("false","page_off_short");
			$footer=($row2->footer!="no")?array("false","footer_off_short"):array("true","footer_on_short");
			$header=($row2->header!="no")?array("false","header_off_short"):array("true","header_on_short");
		}
	}
	if(is_allowed("articles"))
	echo '
	<div class="btn-group" style="float:right;margin-top:-5px;margin-right:4px">
	  <button type="button" onmouseup="window.location.href=\'admin/?action=2&a=edit&id='.$id.'&view_next=true&pageid='.$pageid.'\'" class="btn btn-default"><span class="glyphicon glyphicon-pencil"></span>&ensp;'.text("edit").'</button>
	  <button style=display:none id=article'.$id.'-quicksave type="button" onmouseup="$.ajax({data:\'code=\'+$(\'#article'.$id.'-quickedittextarea\').val(),method:\'post\',url:\'bin/ajax_hook.php?action=update_article_code&id='.$id.'\'});$(\'.article'.$id.'-code\').load(\'bin/ajax_hook.php?action=get_article_code&id='.$id.'\');$(this).toggle();$(\'#article'.$id.'-quickedit\').toggle();" class="btn btn-default"><span class="glyphicon glyphicon-save"></span>&ensp;'.text("save").'</button>
	  <button type="button" class="btn btn-default dropdown-toggle" style="width:45px;" data-toggle="dropdown" aria-expanded="false">
	    <span class="caret" style="margin-right:-5px"></span>&ensp;
	    <span class="sr-only">></span>
	  </button>
	  <ul class="dropdown-menu dropdown-nopadding" data-role="menu">
	    <li><a style=cursor:pointer onmouseup="$(\'.article'.$id.'\').find(\'.load-statistics\').load(\'bin/ajax_hook.php?action=show_statistics&id='.$id.'\').show({effect:\'blind\'});"><span class="glyphicon glyphicon-stats"></span>&ensp;'.text("statistics").'</a></li>
	    <li><a style=cursor:pointer onmouseup="$.get(\'admin/index.php?action=2&a=reset_views&id='.$id.'\',function(){bootbox.alert(\''.text("success").'\',function(){$(\'.article-views\').html(\'0\')});})"><span class="glyphicon glyphicon-repeat"></span>&ensp;'.text("views_set_0").'</a></li>
	    <li><a style=cursor:pointer onmouseup="window.location.href=\'admin/?action=5&article='.$id.'\'"><span class="glyphicon glyphicon-edit"></span>&ensp;'.text("edit_comments").'</a></li>
	    <li><a style=cursor:pointer onmouseup="$.get(\'admin/index.php?action=2&disable_public='.$public[0].'&id='.$id.'\',function(){window.location.href=window.location.href;})"><span class="glyphicon glyphicon-eye'.$public[1].'"></span>&ensp;'.text($public[2]).'</a></li>
	    <li><a style=cursor:pointer onmouseup="$.get(\'admin/?action=2&id='.$id.'&disable_comments='.$comments[0].'\',function(){window.location.href=window.location.href})"><span class="glyphicon glyphicon-comment"></span>&ensp;'.text($comments[1]).'</a></li>
	    <li><a style=cursor:pointer onmouseup="$.get(\'admin/?action=2&id='.$id.'&disable_page='.$page[0].'\',function(){window.location.href=window.location.href})"><span class="glyphicon glyphicon-list"></span>&ensp;'.text($page[1]).'</a></li>
	    <li><a style=cursor:pointer onmouseup="$.get(\'admin/?action=2&id='.$id.'&disable_header='.$header[0].'\',function(){window.location.href=window.location.href})"><span class="glyphicon glyphicon-header"></span>&ensp;'.text($header[1]).'</a></li>
	    <li><a style=cursor:pointer onmouseup="$.get(\'admin/?action=2&id='.$id.'&disable_footer='.$footer[0].'\',function(){window.location.href=window.location.href})"><span class="glyphicon glyphicon-object-align-bottom"></span>&ensp;'.text($footer[1]).'</a></li>
		 <li class=divider></li>
	    <li><a style=cursor:pointer onmouseup="bootbox.confirm(\''.text('sure').'\',function(confirm){if(confirm===true)$.get(\'admin/?action=2&a=delete&id='.$id.'&view_next=true&pageid='.$pageid.'\',function(){bootbox.alert(\''.text("success").'\',function(){window.location.href=window.location.href;})});});"><span class="glyphicon glyphicon-trash"></span>&ensp;'.ucfirst(text("delete")).'</a></li>
	  </ul>
	</div>';/*
	?><style type="text/css">
	.dropdown-toggle { 
	    width: 200px;
	}
	</style><?php*/
	/*echo "<div style=float:right>
	<a class='changeopacity icon' onmousedown=fadeOut(); href=admin/?action=2&a=edit&id=".$id."&view_next=true&pageid=".$pageid."><img src=templates/main/images/edit.png alt='' style=height:27px;margin-top:-7px;margin-bottom:-5px;></a>
	<a class='changeopacity icon removelink' onmousedown=fadeOut(); href='admin/?action=2&a=delete&id=".$id."&view_next=true&pageid=".$pageid."' onmouseup=\"if(confirm('".$text["sure"]."')){window.location.href='admin/?action=2&a=delete&id=".$id."&view_next=true&pageid=".$pageid."';$(this).preventDefault();}\"><img src=templates/main/images/delete.png alt='' style=height:27px;margin:-7px;margin-left:0px;margin-bottom:-5px;></a>
	</div>";*/
}
function is_allowed($what) {
	if(is_sudo())return true;
	global $db,$db_prefix;
	$result2 = $db->query("select * from ".$db_prefix."groups where id = '".userinfo("groupid")."' order by id desc");
	if($result2->num_rows) {
		while($row2 = $result2->fetch_object()) {
			if(!strstr($what,"edit_"))$what="edit_".$what;
			return($row2->$what=="yes")?true:false;
		}
	}
	return false;
}
function is_part_admin() {
	if(is_sudo() || is_allowed("pages") || is_allowed("articles") || is_allowed("user") || is_allowed("groups") || is_allowed("comments") || is_allowed("newsletter") || is_allowed("prefs"))return true;
	return false;
}
function is_sudo() {
	return(userinfo("admin")=="1")?true:false;
}
function userid($username=false) {
	global $db,$db_prefix;
	if($username==false) {
		if(!isset($_COOKIE["hash"]))return false;
		if($db) {
			$result=$db->query("select userid from ".$db_prefix."logins where hash='".$_COOKIE["hash"]."' and expires > '".time()."' and ip='".getip()."'");
			if($result->num_rows)return $result->fetch_object()->userid;
		}
	}
	else {
		$result=$db->query("select id from ".$db_prefix."user where username like '".$username."' limit 1");
		if($result->num_rows)return $result->fetch_object()->id;
	}	
}
function userinfo($col,$userid=false) {
	global $db,$db_prefix;
	$userid=($userid==false)?userid():$userid;
	if($db) {
		$result=$db->query("select * from ".$db_prefix."user where id = '".$userid."' limit 1");
		if($result->num_rows)$info=$result->fetch_object()->$col;
		return (empty($info))?false:$info;
	} else return false;
}
function ending($filename) {
	if(filetype($filename)=="image/jpeg")
		$ending=".jpeg";
	elseif(filetype($filename)=="image/png")
		$ending=".png";
	elseif(filetype($filename)=="image/gif")
		$ending=".gif";
	elseif(filetype($filename)=="audio/wav")
		$ending=".wav";
	elseif(filetype($filename)=="audio/mp3")
		$ending=".mp3";
	elseif(filetype($filename)=="video/mp4")
		$ending=".mp4";
	else {
		$ending=".jpg";
	}
	return $ending;
}
function conf($pref,$val=null) {
	$conf="etc/main.conf";
	if(@file_exists("../".$conf)) $confp="../".$conf;
	elseif(@file_exists("../../".$conf)) $confp="../../".$conf;
	else $confp=$conf;
	$conf=import_conf($confp);
	if($val==null) {
		$confpref=str_replace("\r","",str_replace("\n","",$conf[$pref]));
		if($confpref=="false"||$confpref=="FALSE"||$confpref=="no"||$confpref=="") $confpref=false;
		return $confpref;
	}
	else {
		$conf_in=@file_get_contents($confp);
		$confpref=str_replace("\r","",str_replace("\n","",$conf[$pref]));
		$conf_out=str_replace($pref."=".$confpref,$pref."=".$val,$conf_in);
		@file_put_contents($confp,$conf_out);
		$conf=import_conf($confp);
		$confpref=str_replace("\r","",str_replace("\n","",$conf[$pref]));
		return($confpref==$val)?true:false;
	}
}
function smkdir($dir) {
	if(!is_dir($dir)) mkdir($dir);
}
function error($id) {
	addlog(0,"error",$id);
	feedback("ERROR(".$id.")");
	if(conf("show_errors"))	echo "Error ".$id.". <a href=//v2.domiscms.de/errors/".$id.">More Info</a>";
}
function is_user() {
	if(!isset($_COOKIE["hash"]))return false;
	global $db,$db_prefix;
	$result=$db->query("select userid from ".$db_prefix."logins where hash='".$_COOKIE["hash"]."' and expires > '".time()."' and ip='".getip()."'");
	if(isset($result->num_rows)) {
		return($result->num_rows)?1:0;
	}
}
function reload_page() {
	echo "<script>window.location.href=window.location.href;</script>";
}
function encode($data) {
	if($data=base64_encode(rand(0,9).rand(0,9).base64_encode($data)))return $data;else return false;
}
function decode($data) {
	if($data=base64_decode(substr(base64_decode($data),2)))return $data;else return false;
}
function imgscale($filename,$src_w=null,$src_h=null,$new_img=null) {
	$sz = getimagesize($filename); 
	if(exif_imagetype($filename)==1)$src_im = imagecreatefromgif($filename);
	if(exif_imagetype($filename)==2)$src_im = imagecreatefromjpeg($filename);
	if(exif_imagetype($filename)==3)$src_im = imagecreatefrompng($filename);
	$new_img=(isset($new_img))?$new_img:false;
	if($src_w=="auto" || !isset($src_w))
		$src_w=($src_h/$sz[1])*$sz[0];
	if($src_h=="auto" || !isset($src_h))
		$src_h=($src_w/$sz[0])*$sz[1];
	$dst_im = imagecreatetruecolor($src_w, $src_h);
	imagecopyresampled($dst_im, $src_im, 0, 0, 0, 0, $src_w, $src_h, $sz[0], $sz[1]);
	if(exif_imagetype($filename)==1 && !$new_img)header("Content-type: image/gif");
	if(exif_imagetype($filename)==2 && !$new_img)header("Content-type: image/jpeg");
	if(exif_imagetype($filename)==3 && !$new_img)header("Content-type: image/png");
	$out=exif_imagetype($filename);
	if($new_img) {
		if(strtolower(substr($new_img, (strlen($new_img)-4)))==".gif")	$out=1;
		if(strtolower(substr($new_img, (strlen($new_img)-4)))==".jpg" || strtolower(substr($new_img, (strlen($new_img)-5)))==".jpeg")	$out=2;
		if(strtolower(substr($new_img, (strlen($new_img)-4)))==".png")	$out=3;
	}
	if($out==1)imagegif($dst_im,$new_img);
	if($out==2)imagejpeg($dst_im,$new_img);
	if($out==3)imagepng($dst_im,$new_img);
	imagedestroy($dst_im);
}/*
function comment($input) {
	global $path;
	$profilepic=$input[3];
	$username=$input[2];
	$row["id"]=$input[0];
	$msg=$input[1];
	if(conf("censor_comments")&&conf("censor_comments_live"))
		foreach(file($path."etc/badwords.list") as $badwords) {
			foreach(array($badwords,strtoupper($badwords),strtolower($badwords),ucfirst($badwords)) as $badword) {$badword=str_replace("\n","",$badword);$msg=str_replace($badword,@$badword[0].substr("**************",0,(strlen($badword))-2).@$badword[(strlen($badword)-1)],$msg);}
		}
	if(strstr($msg,"https://www.youtube.com/watch?v=")) {
		$part=explode("https://www.youtube.com/watch?v=",$msg);
		$part=explode(" ",$part[1]);
		$msg="<div class='comment".$row[id]." ".$input[4]."'>Youtube Video // <a style=cursor:pointer onmouseup=\"$('.comment".$row[id]."').html('".str_replace("&#39;","",str_replace("&qout;","",$msg))."');\">".$text["show_me_comment"]."</a>".'<iframe style="margin-top:12px;opacity:.93;margin-bottom:-18px;border-radius:5px" width="100%" height="315" src="https://www.youtube.com/embed/'.$part[0].'" frameborder="0" allowfullscreen></iframe></div>';
	}
	echo "<div class='comment ".$input[4]."'>";if(pref("profilepics")!="no") {echo"<div class='profilepic'"; if($profilepic!="") echo " style='background-image:url(\"".$profilepic."\");'"; echo "></div>";}echo"<b>".ucfirst($username)."</b><br>".$msg."&nbsp;</div>";
}*/
function tmp_bool($var) {
	global $path;
	return(is_file($path."var/tmp_var_".$var)&&file_get_contents($path."var/tmp_var_".$var)=="true")?true:false;
}
function tmp_var($var) {
	global $path;
	if(is_file($path."var/tmp_var_".$var))return file_get_contents($path."var/tmp_var_".$var);
}
function text($str) {
	global $text;
//	return(empty($text[$str]))?strtoupper("<span style=color:red>".$str."</span>"):$text[$str];
	return(empty($text[$str]))?str_replace("_"," ",$str):$text[$str];
}
function refresh($link=null) {
	if($link==null)
		echo "<script>window.location.href=window.location.href;</script><meta http-equiv=refresh content=0>";
	else
		echo "<script>window.location.href='".$link."';</script><meta http-equiv=refresh content='0,".$link."'>";
}
function is_home() {
	return(!isset($_GET["article"])&&!isset($_GET["q"])&&!isset($_GET["page"])&&!isset($_GET["inc"])&&!isset($_GET["do"]))?1:0;
}
function inc($file) {
	global $db,$db_prefix,$text;
	if(file_exists("includes/".$_GET['inc'].".php"))include_once("includes/".$_GET['inc'].".php");
	addlog(0,"inc",$_GET['inc']);
}
function getpath() {
	global $path;
	return $path;
}
function v() {
	global $path;
	$about=import_conf($path."etc/about.conf");
	return $about["version"];
}
function url_get_contents($url) {
	return file_get_contents($url);
}
function getip() {
//	alert($_SERVER['REMOTE_ADDR']);
//	if($_SERVER['REMOTE_ADDR']=="127.0.0.1")return "216.113.204.128";
	if (!empty($_SERVER['HTTP_CLIENT_IP'])) return $_SERVER['HTTP_CLIENT_IP']; elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) return $_SERVER['HTTP_X_FORWARDED_FOR']; else return $_SERVER['REMOTE_ADDR'];
}
function make_alias($str) {
	
	$alias=null;
	$aliasid=null;
	
	global $db,$db_prefix;
	$alias=preg_replace("/[^a-zA-Z0-9\_\-]+/","",str_replace(" ","-",str_replace("uml;","e",str_replace("ö","oe",str_replace("ü","ue",str_replace("ä","ae",str_replace("ß","ss",strtolower($str))))))));
	
	$foundsth=true;
	
	while($foundsth==true) {
		$foundsth=true;
		while($foundsth==true) {
			$foundsth=false;
			$result=$db->query("select * from ".$db_prefix."pages where alias = '".$alias.$aliasid."'");
			while($row=$result->fetch_object()) {
				$aliasid++;
				$foundsth=true;
			}
		}
		
		
		$foundsth=true;
		while($foundsth==true) {
			$foundsth=false;
			$result=$db->query("select * from ".$db_prefix."articles where alias = '".$alias.$aliasid."'");
			while($row=$result->fetch_object()) {
				$aliasid++;
				$foundsth=true;
			}
		}
		
		$foundsth=false;
			
		$result=$db->query("select * from ".$db_prefix."articles where alias = '".$alias.$aliasid."'");
		while($row=$result->fetch_object()) {
			$aliasid++;
			$foundsth=true;
		}
		$result=$db->query("select * from ".$db_prefix."pages where alias = '".$alias.$aliasid."'");
		while($row=$result->fetch_object()) {
			$aliasid++;
			$foundsth=true;
		}
	}
	
	return $alias.$aliasid;
}
if(is_file($path."classes/dbobject.php"))include_once($path."classes/dbobject.php");
foreach(glob($path."classes/*.php") as $class) {
	include_once($class);
}
?>