$(function() {if (screen.width<=800) {
	$(".topnav-toggle").click(function () {
		$("html, body").animate({ scrollTop: 0 }, 200);
		$(".topnav").toggle();
		$(".uppernav").toggle();
	});
}
$(document.body).click(function(e) {
	  if($("#comismenu").css("display")=="block" && !$(e.target).is("#comismenu *") ) {
	    $("#comismenu").hide();
	  }
});
$(function(){
	$(".devinfo").draggable({
		stop: function() {
			$.ajax({data:'',type:'post',url:'?do=setcookie&name=userconf_devinfo_left&value='+$(this).css("left")});
			$.ajax({data:'',type:'post',url:'?do=setcookie&name=userconf_devinfo_top&value='+$(this).css("top")});
		}
	});
	$(".devinfo").resizable({
		stop: function() {
			$.ajax({data:'',type:'post',url:'?do=setcookie&name=userconf_devinfo_width&value='+$(this).css("width")});
			$.ajax({data:'',type:'post',url:'?do=setcookie&name=userconf_devinfo_height&value='+$(this).css("height")});
		}
	});
});
var dragTimer;
$(document).on('dragover', function(e) {
    var dt = e.originalEvent.dataTransfer;
    if(dt.types != null && (dt.types.indexOf ? dt.types.indexOf('Files') != -1 : dt.types.contains('application/x-moz-file'))) {
        $(".dropzone").fadeIn(500);
        $(".magic-file").addClass("expanded");
        window.clearTimeout(dragTimer);
    }
});
$(document).on('dragleave', function(e) {
    dragTimer = window.setTimeout(function() {
        $(".dropzone").fadeOut(400);
        $(".magic-file").removeClass("expanded");
    }, 100);
});
$(document).ready(function(){$(".removelink").attr("href","#");});
});
