<?php
class aside implements ArrayAccess {
	public function create() {
		echo "<h4>".text("latest_articles")."</h4>";
		$this->articlelist("timestamp","desc",5);
		
		echo "<h4>".text("popular_articles")."</h4>";
		$this->articlelist("views","desc",5);
		
		echo "<h4>".text("unread_articles")."</h4>";
		$this->unreadarticlelist(5);
		
		$this->links();
		$this->contact();
		
		echo "<br><br><br>";
   }
   private function articlelist($orderby="id",$order="",$limit="10") {
		global $db,$db_prefix;
   	$this->result = $db->query("select * from ".$db_prefix."articles where public = 'yes' and title != '' order by $orderby $order limit $limit");
		if($this->result) {
			while($this->row = $this->result->fetch_object()) {
				echo "<a href='";
				if(conf("modrewrite"))
					echo $this->row->alias.conf("rewrite_suffix");
				else
					echo "?article=".$this->row->id;
				echo "'><li>".$this->row->title."</li></a>";	
			}
		}
   }
   
   private function unreadarticlelist($limit) {
   	global $db,$db_prefix;$i=0;
		if(is_user()) {
			$result=$db->query("select value from ".$db_prefix."statistics where userid='".userid()."' and action='article' order by id desc limit 1000");
			while($row=$result->fetch_object()) {
				$this->user_read_articles[]=$row->value;
			}		
		}
		$this->result=$db->query("select * from ".$db_prefix."articles where public = 'yes' and title != '' order by timestamp desc limit 1000");
		if($this->result) {
			while($this->row = $this->result->fetch_object()) {
				if(empty($this->user_read_articles)||!is_array($this->user_read_articles)||!in_array($this->row->id,$this->user_read_articles)) {
					$i++;if($i>$limit)continue;
					echo "<a href='";
					if(conf("modrewrite"))
						echo $this->row->alias.conf("rewrite_suffix");
					else
						echo "?article=".$this->row->id;
					echo "'><li>".$this->row->title."</li></a>";	
					$this->unread=true;
				}
			}
		}
		if(!isset($this->unread))echo"<i style=margin-left:0px>".text("no_unread_articles")."</i><br>";
	}
   
   private function links() {
		if(file_exists("var/aside_links.json")&&count(file("var/aside_links.json"))>0) {
			echo "<h4>".text('links')."</h4>";
			$this->links=json_decode(file_get_contents("var/aside_links.json"),true);
			foreach($this->links as $this->link) {
				echo "<a href='".$this->link[0]."'><li>".$this->link[1]."</li></a>";
			}
		} elseif(is_sudo()) {
			echo "<h4>".text('links')."</h4>";
		}
		if(is_sudo())echo "<a href=admin/?action=9&dropdown=6&a=add><li>".text("add_link")."</li></a>";
	}
   private function contact() {
		global $db,$db_prefix;
		echo "<br><b>";
		$this->result = $db->query("select * from ".$db_prefix."user where id = '".pref("contact_person")."' limit 1");
		if($this->result) {
			while($this->row = $this->result->fetch_object()) {
				$this->user_name=$this->row->name;
				$this->email=$this->row->email;
				echo $this->user_name;

				echo"</b></b><br><a href=mailto:".$this->email.">".$this->email."</a>";
			}
		}
	}

  	public function offsetSet($offset, $value) { if (is_null($offset)) { $this->container[] = $value; } else { $this->container[$offset] = $value; } } public function offsetExists($offset) { return isset($this->container[$offset]); } public function offsetUnset($offset) { unset($this->container[$offset]); } public function offsetGet($offset) { return isset($this->container[$offset]) ? $this->container[$offset] : null; }
}