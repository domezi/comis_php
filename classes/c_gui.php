<?php
class c_gui {
	function createhomepage() {
		$mainpage=new mainpage();
		$mainpage->createhomepage();
	}
	function searchpage() {
		global $_GET;
		if(strlen($_GET["q"])==0)return;
		$search=new search();
		$search->by_keyword($_GET['q']);
	}
	public function page($pageid) {

		if($pageid==null) {global $_GET;$pageid=$_GET["page"];}

		if(strlen($pageid)==0)return;
		$page = new page();
		$page->createfromid($pageid);
	}
	public function nav_menu($article,$page,$inc) {
		$nav=new nav();
		$nav->create($article,$page,$inc);
	}
	public function article($id=null,$prefs=array("header"=>true,"attachments"=>true)) {

		$comments=$prefs["comments"];
		$footer=$prefs["footer"];
		$header=$prefs["header"];
		$attachments=$prefs["attachments"];
		$addview=$prefs["addview"];
		$show_editicons=$prefs["show_editicons"];
				
		if($id==null) {global $_GET;$id=$_GET["article"];}

		if(!is_numeric($id))return;
		global $db,$db_prefix;
		if($addview==true) {
			$db->query("update `".$db_prefix."articles` set views=views+1 where id = '".$id."'");
			addlog(0,"article",$id);
		}
		echo "<article class=article".$id.">";
		echo "<div class=load-statistics></div>";
		
		$this->found=false;
		$this->result = $db->query("select * from `".$db_prefix."articles` where id = '".$id."' order by timestamp desc limit 1");
		if($this->result->num_rows) {
			$this->arr_article = $this->result->fetch_object();
			if($this->arr_article) {
				$this->not_public=($this->arr_article->public!="yes" && is_sudo())?true:false;
				if($this->not_public)
					echo "<div class=\"alert alert-not-public alert-danger\">".text("article_not_public")."</div>";
				if($this->arr_article->public=="yes" || is_sudo()) {
				

					if($show_editicons)show_editicons($this->arr_article->id);


					if($header==true && $this->arr_article->header!="no")echo "<h2 class='article-title'><span>".$this->arr_article->title."</span></h2>";
					echo "<div class=article".$id."-code>".$this->arr_article->code;
					$php=str_replace('&gt;',">",str_replace('&lt;',"<",str_replace("&amp;","&",str_replace('&#039;',"'",str_replace('&#39;',"'",str_replace('&quot;','"',$this->arr_article->php))))));
					$php=str_replace('&#039;',"'",str_replace('&lt;',"<",$this->arr_article->php));
					echo eval($php);
					echo"</div><br class=clear>";
					if($attachments!=false&&!empty($this->arr_article->attachments)&&pref('attachments')!="false") {
						$attachments=json_decode($this->arr_article->attachments,true);
						if(count($attachments["attachments"])>9) {
							echo "<a href=# onmouseup=\"$(this).hide().next('.article-attachments-show').show();\">>> ".text("attachments_toggle")." <<</a>";
							$style="display:none";
						} else $style="";
						echo"<div style='".$style."' class=article-attachments-show><hr class=clear>";
						$attachments=json_decode($this->arr_article->attachments,true);
						foreach($attachments["attachments"] as $attachment) {
							echo "<a href='".$attachment["url"]."' class='btn btn-default'><span class='glyphicon glyphicon-paperclip'></span>&ensp;".$attachment["title"]."</a>";
						}
						echo "</div>";
					}
					if($comments!=false&&$this->arr_article->comments=="yes" && pref('pages_enabled')!="no" && pref('comments')!="no") {
						$comments=new comment();
						$comments->showall($id);
					}
					if($this->arr_article->footer!="no"&&$footer!=false&&pref('pages_enabled')!="no")
						echo "<br class=clear><div class=articlefooter data-article-id=".$id."><hr>".text("editor").": ".ucfirst($this->arr_article->editor)." -  ".text("date").": ".date("d.m.y H:i",$this->arr_article->timestamp)." -  ".text("views").": <span class=article-views>".$this->arr_article->views."</span> </div>";
					else echo "<br class=clear>";
					$this->found=true;
				}
			}
		}
		if(!$this->found) 
			echo "<h2>".text("no_article")."</h2>";
		echo "</article>";
		
	}
	
	public function aside() {
		$aside=new aside();
		if(pref("aside")!="no")
			$aside->create();
	}
	
	
	public function loginout() {
		if(is_user()) {
			echo "<a href=?do=logout>Logout</a>";
		} else {
			echo "<a href=admin/>Login</a>";
		}
	}

	public function adminlink() {
		if(is_part_admin()) {
			echo "<a href=admin/>Admin-Bereich</a>";
		}
	}	
	
	public function editlink() {
		if(is_part_admin()) {
			global $_GET;
			if($_GET["article"]>0)
				echo "<a href=admin/?action=2&a=edit&id=".$_GET["article"].">".text("edit_article")."</a>";
			if($_GET["page"]>0)
				echo "<a href=admin/?action=1&a=edit&id=".$_GET["page"].">".text("edit_page")."</a>";
		}
	}


	public function addcontentmenu() {
		global $db,$db_prefix;
		if(is_part_admin()) {
			echo "<li class=dropdown><a href='.?inc=account' role=button data-toggle=dropdown><span class=caret></span>&ensp;".ucfirst(text("administration"))."</a>
				<ul class=dropdown-menu role=dropdown>";
				if(is_file("var/favorites.json"))$favorites=file_get_contents("var/favorites.json");
				if(is_file("var/favorites.json")&&!empty($favorites))$favorites=json_decode(file_get_contents("var/favorites.json"),1);else $favorites=null;
				if(isset($favorites["favorites"][userid()])&&count($favorites["favorites"][userid()])>0) {
					foreach($favorites["favorites"][userid()] as $favorite) {
						echo "<li><a href='".$favorite["url"]."'><span class='glyphicon glyphicon-star'></span>&ensp;".ucfirst($favorite["title"])."</a></li>";
					}
					echo "<li class=divider></li>";
				}
				if(is_sudo())
					echo "<li><a href='admin/'><span class='glyphicon glyphicon-dashboard'></span>&ensp;".ucfirst(text("dashboard"))."</a></li>";
				if(pref('pages_enabled')=="yes" && is_allowed("pages"))
					echo "<li><a href='admin/?action=1&dropdown=0&view_next=true'><span class='glyphicon glyphicon-plus'></span>&ensp;".text("page")."</a></li>";
				if(is_allowed("articles"))
					echo "<li><a href='admin/?action=2&dropdown=0&view_next=true'><span class='glyphicon glyphicon-plus'></span>&ensp;".text("article")."</a></li>";
				if(is_allowed("user"))
					echo "<li><a href='admin/?action=3&dropdown=0&view_next=true'><span class='glyphicon glyphicon-plus'></span>&ensp;".text("user")."</a></li>";
				if(is_allowed("groups"))
					echo "<li><a href='admin/?action=4&dropdown=0&view_next=true'><span class='glyphicon glyphicon-plus'></span>&ensp;".text("group")."</a></li>";
				if(is_allowed("comments"))
					echo "<li><a href='admin/?action=5&dropdown=0&view_next=true'><span class='glyphicon glyphicon-comment'></span>&ensp;".text("comments")."</a></li>";
				if(is_allowed("newsletter"))
					echo "<li><a href='admin/?action=6&dropdown=0&view_next=true'><span class='glyphicon glyphicon-envelope'></span>&ensp;".text("newsletter")."</a></li>";
				if(is_allowed("articles"))
					echo "<li><a href='admin/?action=2&dropdown=1'><span class='glyphicon glyphicon-picture'></span>&ensp;".text("my_pictures")."</a></li>";
				if(is_sudo()) {
					echo "<li class=divider></li>
					<li><a href='admin/?action=9&dropdown=6'><span class='glyphicon glyphicon-link'></span>&ensp;".text("links")."</a></li>
					<li><a href='admin/?action=8&view_next=true'><span class='glyphicon glyphicon-globe'></span>&ensp;".text("addons")."</a></li>
					<li><a href='admin/?action=9&dropdown=5&view_next=true'><span class='glyphicon glyphicon-globe'></span>&ensp;".text("templatemanager")."</a></li>
					<li><a href='admin/?action=9&dropdown=2'><span class='glyphicon glyphicon-file'></span>&ensp;".text("comisbrowser")."</a></li>
					<li><a href='admin/?action=11&view_next=true'><span class='glyphicon glyphicon-hdd'></span>&ensp;".text("backup")."</a></li>
					";
				}
				if(is_allowed("prefs"))
					echo "<li><a href='admin/?action=9'><span class='glyphicon glyphicon-cog'></span>&ensp;".text("preferences")."</a></li>
					<li class=divider></li>
					<li><a href='admin/?action=10'><span class='glyphicon glyphicon-stats'></span>&ensp;".text("statistics")."</a></li>
					<li><a href='http://domiscms.de/support'><span class='glyphicon glyphicon-education'></span>&ensp;".text("support")."</a></li>";
				echo "
				</ul>
			</li>";	
		}
	}
	
	public function loginmenu() {
		global $db,$db_prefix;
		if(!is_user())
			echo "<li><a href='.?inc=login'><span class='glyphicon glyphicon-log-in'></span>&ensp;".ucfirst(text("login"))."</a></li>";	
		else {
			if(!userinfo("profilepic"))$profilepic="templates/main/images/profilepic.jpg";
			else$profilepic=userinfo("profilepic");
			echo "<li class=dropdown";if(is_sudo())echo" style=margin-right:-40px;";echo"><a title='".ucfirst(userinfo("username"))."' href='.?inc=account' role=button data-toggle=dropdown><span class=caret></span>&ensp;".ucfirst(text("me"))."&emsp;<div style=float:right>&emsp;</div><div class='hideonmobile profilepic' style='float:right;margin-top:-13px;margin-right:-20px !important;background-image:url(".$profilepic.");'></div></a>
				<ul class=dropdown-menu role=dropdown>";
				echo "<li><a href=# onmouseup=\"bootbox.dialog({size:'medium',message:'<center><img style=height:500px;width:500px;border-radius:50%;margin-top:20px;margin-bottom:20px; src=";
				echo(is_file(str_replace(".","hd.",$profilepic)))?str_replace(".","hd.",$profilepic):$profilepic;echo"></center>'});\"><span class='glyphicon glyphicon-picture'></span>&ensp;".ucfirst(text("show_profilepic"))."</a></li>";
				echo "
					<li><a href='.?inc=userstatistics'><span class='glyphicon glyphicon-stats'></span>&ensp;".text("statistics")."</a></li>
					<li><a href='.?inc=account'><span class='glyphicon glyphicon-user'></span>&ensp;Account Einstellungen</a></li>
					<li><a class='removelink' onmouseup=\"bootbox.confirm('Wollen Sie sich wirklich ausloggen?',function(confirm){if(confirm===true)window.location.href='.?do=logout';});\" href='.?inc=logout'><span class='glyphicon glyphicon-log-out'></span>&ensp;Logout</a></li>
				</ul>
			</li>";	
		}
	}

	public function contextmenu() {
		if(conf('contextmenu')) {
			$comismenu=new comismenu();
			$comismenu->create("mainpage");
		}
	}



}
?>
