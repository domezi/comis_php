<?php
class build implements ArrayAccess {
	public function buildcontainer() {
		if(isset($_GET['q']))
			include("includes/search.php");	
		elseif(!isset($_GET["article"])&&!isset($_GET["page"])&&!isset($_GET["inc"])&&!isset($_GET["do"])) {
			$mainpage=new mainpage();
			$mainpage->createhomepage(pref('homepage'));
		}
		elseif(isset($_GET['inc']) && file_exists("includes/".$_GET['inc'].".php")) {
			if(file_exists("includes/".$_GET['inc'].".php"))include_once("includes/".$_GET['inc'].".php");
			addlog(0,"inc",$_GET['inc']);	
		}
		elseif(isset($_GET['article'])) {
			$article=new article();
			$article->createfromid($_GET['article']);
		}
		elseif(isset($_GET['page'])) {
			$page = new page();
			$page->createfromid($_GET["page"]);
		}
	}

  	public function offsetSet($offset, $value) { if (is_null($offset)) { $this->container[] = $value; } else { $this->container[$offset] = $value; } } public function offsetExists($offset) { return isset($this->container[$offset]); } public function offsetUnset($offset) { unset($this->container[$offset]); } public function offsetGet($offset) { return isset($this->container[$offset]) ? $this->container[$offset] : null; }
}