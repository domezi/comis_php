<?php
class page implements ArrayAccess {
	public function createfromid($id) {
		if(!is_numeric($id))return;
		global $db,$db_prefix;


					addlog(0,"page",$id);
					$this->found=false;
					$this->result = $db->query("select id,title,code,public,alias from ".$db_prefix."articles where pageid = '".$id."' order by timestamp desc");
					if($this->result) {
						while($this->row = $this->result->fetch_object()) {
							if($this->found) echo "<hr class=clear>";
							$this->not_public=($this->row->public!="yes" && is_sudo())?true:false;
							if($this->not_public)
								echo "<div class=\"alert alert-danger\">".text("article_not_public")."</div>";
							if($this->row->public=="yes" || is_sudo()) {
								echo "<h2>".$this->row->title;
								show_editicons($this->row->id,$id);
								echo "</h2>".$this->row->code."<br style=clear:both;margin-top:10px;display:block>";
								if(pref('pages_enabled')!="no") {
									echo"<a href='";
									echo (conf("modrewrite"))?$this->row->alias.conf("rewrite_suffix"):".?article=".$this->row->id;
									echo "' class='btn btn-default'>
									>> ".text("view_article")." <<
									</a>";
								}
								echo"<br><br>";	
								$this->found=true;
							}
						}
					}
					if(!$this->found)
					echo "<h2> ".text("no_page")." </h2>";
					
					
					
	}
	
  	public function offsetSet($offset, $value) { if (is_null($offset)) { $this->container[] = $value; } else { $this->container[$offset] = $value; } } public function offsetExists($offset) { return isset($this->container[$offset]); } public function offsetUnset($offset) { unset($this->container[$offset]); } public function offsetGet($offset) { return isset($this->container[$offset]) ? $this->container[$offset] : null; }
}