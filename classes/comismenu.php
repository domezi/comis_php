<?php
class comismenu implements ArrayAccess {
	public function create($from="mainpage") {
		global $db,$db_prefix;
		if($from="mainpage" && is_sudo()) {
		echo '
		<ul id="comismenu" style="display:none;position:fixed;z-index:100;text-decoration:none;border-radius:4px;border:1px solid white;background:hsla(0,0%,100%,.93);box-shadow:0px 0px 20px -5px black">
			<a href="admin/?action=0"><li>'.text("current").'</li></a>
			<a href="admin/?action=1"><li>'.text("pages").'</a>
				<ul>
					<a href="admin/?action=1&dropdown=0"><li>'.text("new_page").'</a>
					<a href="admin/?action=1&create_empty"><li>'.text("add_empty").'</a>
				</ul>
			</li>
			<a href="admin/?action=2"><li>'.text("articles").'</a>
				<ul>
					<a href="admin/?action=2&dropdown=0"><li>'.text("new_article").'</a>
					<a href="admin/?action=2&create_empty"><li>'.text("add_empty").'</a>
					<a href=admin/?action=2&dropdown=1><li>'.text('my_pictures').'</li></a>
				</ul>
			</li>
			<a href="admin/?action=3"><li>'.text("user").'</a>
				<ul>
					<a href="admin/?action=3&dropdown=0"><li>'.text("new_user").'</a>
					<a href="admin/?action=3&create_empty"><li>'.text("add_empty").'</a>
				</ul>
			</li>
			<a href="admin/?action=4"><li>'.text("groups").'</a>
				<ul>
					<a href="admin/?action=4&dropdown=0"><li>'.text("new_group").'</a>
					<a href="admin/?action=4&create_empty"><li>'.text("add_empty").'</a>
				</ul>
			</li>
			<a href="admin/?action=5"><li>'.text("comments").'</li></a>
			<a href="admin/?action=6"><li>'.text("newsletter").'</a>
				<ul>
					<a href="admin/?action=6"><li>'.text("new").'</a>
					<a href="admin/?action=6&history"><li>'.text("history").'</a>
				</ul>
			</li>
			<a href="admin/?action=8"><li>'.text("addons").'</a>
				<ul>
					<a href=admin/?action=8&dropdown=0><li>'.text('my_addons').'</li></a>
					<a href=admin/?action=8&dropdown=1><li>'.text('install_addon_from file').'</li></a>
				</ul>
			</li>
			<a href="admin/?action=9"><li>'.text("preferences").'</a>
				<ul>
					<a href=admin/?action=9&dropdown=0><li>'.text('website').'</li></a>';
					echo "<a href=admin/?action=9&dropdown=6><li>".text('links')."</li></a>";
					if(conf("developer"))echo "<a href=admin/?action=9&dropdown=4><li>".text('configuration')."</li></a>";
					echo "<a href=admin/?action=9&dropdown=2><li>".text('comisbrowser')."</li></a>";			
					echo "<a href=admin/?action=9&dropdown=5><li>".text('templatemanager')."</li></a>";			
					echo '
				</ul>
			</li>
			<a href="admin/?action=11"><li>'.text("backup").'</a>
				<ul>
					';
					echo "<a href=admin/?action=11&show=all><li>".text("backup_all")."</li></a>
					<a href=admin/?action=11&show=daily><li>".text("backup_daily")."</li></a>
					<a href=admin/?action=11&show=download><li>".text("download_backups")."</li></a>
					<a href=admin/?action=11&show=my_backups><li>".text("my_backups")."</li></a>
					<a href=admin/?action=11&show=create_backup><li>".text("create_backup")."</li></a>
					<a href=../bin/backup.php?export><li>".ucfirst(text("export"))."</li></a>
					<a href=admin/?action=11&show=import><li>".ucfirst(text("import"))."</li></a>";
					echo '
				</ul>
			</li>';
			if(conf("developer")) {echo'<a href="admin/?action=11"><li>Developer</a>
				<ul>
					';
					echo "<a href=admin/?action=200&parent_action=9&file=../etc/main.conf&return=?action=9(and)dropdown=2(and)path=../etc><li>edit main.conf</li></a>";
					echo "<a href=admin/?action=9&dropdown=4><li>configuration</li></a>";
					echo "<a href=?do=logout><li>logout</li></a>";
					echo "<a href=installation/reinstall.php><li>reinstall</li></a>";
					echo "<a href=//www.domiscms.de><li>www.domiscms.de</li></a>";
					echo "<a href=?conf=developer&val=false><li>end developer</li></a>";
					echo "<li style=cursor:pointer; onmouseup=\"$.ajax({type:'post',data:'',url'index.php?do=setcookie&name=userconf_devinfo_top&value=0'});$.ajax({type:'post',data:'',url'index.php?do=setcookie&name=userconf_devinfo_left&value=0'});\">reset devinfo</li>";
					echo '
				</ul>
			</li>';}
			echo '
		</ul>
		<script type="text/javascript">
		$(function() {
			$("#comismenu").menu();
		});
		$(document).bind("contextmenu", function(e) {
		    $(\'#comismenu\').css({
		        top: (e.pageY)+"px",
		        left: e.pageX+"px"
		    }).show();
		    return false;
		});
		</script>';
		}
	}
	
  	public function offsetSet($offset, $value) { if (is_null($offset)) { $this->container[] = $value; } else { $this->container[$offset] = $value; } } public function offsetExists($offset) { return isset($this->container[$offset]); } public function offsetUnset($offset) { unset($this->container[$offset]); } public function offsetGet($offset) { return isset($this->container[$offset]) ? $this->container[$offset] : null; }
}