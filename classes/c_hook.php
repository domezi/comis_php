<?php
class c_hook {
		
	public function __construct() {

		$this->tocall=array();

	}

	public function add_function($when,$function) {	
		
		$this->tocall[$when][]=$function;

	}

	public function call($when) {

		foreach($this->tocall[$when] as $this->f) {
			($this->f)();
		}


	}

}
