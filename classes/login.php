<?php
class login {
	public function createSession($username,$password) {
		$user=new user();
		if($user->canlogin($username,$password)) {
			global $db,$db_prefix;
			$this->userid=userid($username);
			$this->hash=hash("sha512",rand(0,9e9));
			$db->query("delete from ".$db_prefix."logins where userid = '".$this->userid."' and ip='".getip()."'");
			$db->query("update ".$db_prefix."user set `last_login` = '".time()."',`failed_logins`='0' where id = '".$this->userid."'");
			$session_duration=(conf("session_duration"))?conf("session_duration"):3600;
			$db->query("insert into ".$db_prefix."logins (userid,hash,timestamp,expires,ip) values ('".$this->userid."','".$this->hash."','".time()."','".(time()+$session_duration)."','".getip()."')");
	      return $this->hash;
		} else return false;
   }
}
