<?php
class search {
	public function by_keyword($keyword) {
		global $db,$db_prefix;
		$this->found=0;
		$this->result = $db->query("SELECT * FROM `".$db_prefix."articles` WHERE (`code` LIKE '%".$keyword."%' OR `title` LIKE '%".$keyword."%') and public = 'yes' limit 100");
		if($this->result->num_rows) {
			echo "<div style=\"margin:15px;margin-left:3px;font-size:16px;\">".text("before_search")." ".$this->result->num_rows." ".text("between_search")."  \"".$keyword."\" ".text("after_search")."</div>";
			while($this->row = $this->result->fetch_object()) {
				echo "<a style=text-decoration:none; href='";
				if(conf("modrewrite"))
					echo $this->row->alias.conf("rewrite_suffix");
				else
					echo "?article=".$this->row->id;
				echo"'><div class='search-result'>".$this->row->title."<br>".substr(strip_tags($this->row->code),0,230)."<div class='search-result-fadeout'></div></div></a>";	
				$this->found++;
			}
			echo "<br>";
		}
		$this->result = $db->query("SELECT ".$db_prefix."comments.id,articleid,alias FROM `".$db_prefix."comments` join `".$db_prefix."articles` on articleid=".$db_prefix."articles.id WHERE (`msg` LIKE '%".$keyword."%') limit 100");
		if($this->result->num_rows) {
			echo "<div style=\"margin:5px;font-size:16px;\">".text("comments").": ".$this->result->num_rows."</div><hr>";
			$this->comment=new comment();
			while($this->row = $this->result->fetch_object()) {
				echo "<a style=text-decoration:none; href='";
				if(conf("modrewrite"))
					echo $this->row->alias.conf("rewrite_suffix");
				else
					echo "?article=".$this->row->id;
				echo "#comment".$this->row->id."'>";
				$this->comment->show($this->row->id);
				echo "</a>";	
				$this->found++;
			}
			echo "<br>";
		}
		if($this->found<1)
			echo "Zu Ihrer Suche \"".$_GET['q']."\" konnten leider kein Ergebnisse gefunden werden.";
		addlog(0,"search",$_GET['q']);
	}
//  	public function offsetSet($offset, $value) { if (is_null($offset)) { $this->container[] = $value; } else { $this->container[$offset] = $value; } } public function offsetExists($offset) { return isset($this->container[$offset]); } public function offsetUnset($offset) { unset($this->container[$offset]); } public function offsetGet($offset) { return isset($this->container[$offset]) ? $this->container[$offset] : null; }
}