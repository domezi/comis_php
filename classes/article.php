<?php
class article implements ArrayAccess {
	public function arrayfromids() {
		global $db,$db_prefix;
		return $db->query("select code from ".$db_prefix."articles where id = '".$id."'")->fetch_assoc()->code;
	}
	public function getbody($id) {
		global $db,$db_prefix;
		return $db->query("select code from ".$db_prefix."articles where id = '".$id."'")->fetch_object()->code;
	}
	public function getid($title) {
		global $db,$db_prefix;
		return $db->query("select id from ".$db_prefix."articles where title like '".$title."'")->fetch_object()->id;
	}
	public function createfromid($id,$comments=true,$footer=true,$header=true,$attachments=true,$addview=false) {
		if(!is_numeric($id))return;
		global $db,$db_prefix;
		if($addview==true) {
			$db->query("update `".$db_prefix."articles` set views=views+1 where id = '".$id."'");
			addlog(0,"article",$id);
		}
		echo "<article class=article".$id.">";
		echo "<div class=load-statistics></div>";
		
		$this->found=false;
		$this->result = $db->query("select * from `".$db_prefix."articles` where id = '".$id."' order by timestamp desc limit 1");
		if($this->result->num_rows) {
			$this->arr_article = $this->result->fetch_object();
			if($this->arr_article) {
				$this->not_public=($this->arr_article->public!="yes" && is_sudo())?true:false;
				if($this->not_public)
					echo "<div class=\"alert alert-not-public alert-danger\">".text("article_not_public")."</div>";
				if($this->arr_article->public=="yes" || is_sudo()) {
					show_editicons($this->arr_article->id);
					if($header==true && $this->arr_article->header!="no")echo "<h2 class='article-title'><span>".$this->arr_article->title."</span></h2>";
					echo "<div class=article".$id."-code>".$this->arr_article->code;
					$php=str_replace('&gt;',">",str_replace('&lt;',"<",str_replace("&amp;","&",str_replace('&#039;',"'",str_replace('&#39;',"'",str_replace('&quot;','"',$this->arr_article->php))))));
					$php=str_replace('&#039;',"'",str_replace('&lt;',"<",$this->arr_article->php));
					echo eval($php);
					echo"</div><br class=clear>";
					if($attachments!=false&&!empty($this->arr_article->attachments)&&pref('attachments')!="false") {
						$attachments=json_decode($this->arr_article->attachments,true);
						if(count($attachments["attachments"])>9) {
							echo "<a href=# onmouseup=\"$(this).hide().next('.article-attachments-show').show();\">>> ".text("attachments_toggle")." <<</a>";
							$style="display:none";
						} else $style="";
						echo"<div style='".$style."' class=article-attachments-show><hr class=clear>";
						$attachments=json_decode($this->arr_article->attachments,true);
						foreach($attachments["attachments"] as $attachment) {
							echo "<a href='".$attachment["url"]."' class='btn btn-default'><span class='glyphicon glyphicon-paperclip'></span>&ensp;".$attachment["title"]."</a>";
						}
						echo "</div>";
					}
					if($comments!=false&&$this->arr_article->comments=="yes" && pref('pages_enabled')!="no" && pref('comments')!="no") {
						$comments=new comment();
						$comments->showall($id);
					}
					if($this->arr_article->footer!="no"&&$footer!=false&&pref('pages_enabled')!="no")
						echo "<br class=clear><div class=articlefooter data-article-id=".$id."><hr>".text("editor").": ".ucfirst($this->arr_article->editor)." -  ".text("date").": ".date("d.m.y H:i",$this->arr_article->timestamp)." -  ".text("views").": <span class=article-views>".$this->arr_article->views."</span> </div>";
					else echo "<br class=clear>";
					$this->found=true;
				}
			}
		}
		if(!$this->found) 
			echo "<h2>".text("no_article")."</h2>";
		echo "</article>";
	}

  	public function offsetSet($offset, $value) { if (is_null($offset)) { $this->container[] = $value; } else { $this->container[$offset] = $value; } } public function offsetExists($offset) { return isset($this->container[$offset]); } public function offsetUnset($offset) { unset($this->container[$offset]); } public function offsetGet($offset) { return isset($this->container[$offset]) ? $this->container[$offset] : null; }
}