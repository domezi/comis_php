<?php
class cleaner {
	public function logins() {
		$out="<hr><b>Cleaning logins ...</b>";
		global $db,$db_prefix;
		$this->num_rows_before=$db->query("select id from ".$db_prefix."logins")->num_rows;
		$db->query("delete from ".$db_prefix."logins where expires < '".time()."'");
		$out.= "<br>found ".$this->num_rows_before." records";
		$out.= "<br>deleted ".($this->num_rows_before-$db->query("select id from ".$db_prefix."logins")->num_rows)." records";
   }
	public function delete_emptys() {
		echo "<hr><b>Deleting empty datarows ...</b>";
		global $db,$db_prefix;
		$db->query("delete from ".$db_prefix."pages where title = '' and alias = '' and description = ''");
		$db->query("delete from ".$db_prefix."articles where title = '' and alias = '' and code = ''");
   }
	public function optimize_tables() {
		echo "<hr><b>Optimizing tables ...</b>";
		global $db,$db_prefix;
		foreach(explode(",","articles,comments,groups,logins,pages,preferences,user") as $table) {
			echo "<br>optimize ".$db_prefix.$table;
			$db->query("optimize ".$db_prefix.$table);
		}
   }
	public function analyze_tables() {
		echo "<hr><b>Analyzing tables ...</b>";
		global $db,$db_prefix;
		foreach(explode(",","articles,comments,groups,logins,pages,preferences,user") as $table) {
			echo "<br>analyze ".$db_prefix.$table;
			$db->query("analyze ".$db_prefix.$table);
		}
   }
	public function repair_tables() {
		echo "<hr><b>Repairing tables ...</b>";
		global $db,$db_prefix;
		foreach(explode(",","articles,comments,groups,logins,pages,preferences,user") as $table) {
			echo "<br>repair ".$db_prefix.$table;
			$db->query("repair ".$db_prefix.$table);
		}
   }
	public function check_tables() {
		echo "<hr><b>Checking tables ...</b>";
		global $db,$db_prefix;
		foreach(explode(",","articles,comments,groups,logins,pages,preferences,user") as $table) {
			echo "<br>check ".$db_prefix.$table;
			$db->query("check ".$db_prefix.$table);
		}
   }
	public function alias() {
		echo "<hr><b>Creating alias ...</b>";
		global $db,$db_prefix;
		$result=$db->query("select id,title from ".$db_prefix."pages where alias = '' or alias is NULL");
		if($result) {
			$this->i=0;
			while($row=$result->fetch_object()) {
				$this->i++;
				$db->query("update ".$db_prefix."pages set alias = '".make_alias($row->title)."' where id = '".$row->id."'");
			}
		}
		echo "<br>".$this->i." pages affected.";
		$result=$db->query("select id,title from ".$db_prefix."articles where alias = '' or alias is NULL");
		if($result) {
			$this->i=0;
			while($row=$result->fetch_object()) {
				$this->i++;
				$db->query("update ".$db_prefix."articles set alias = '".make_alias($row->title)."' where id = '".$row->id."'");
			}
		}
		echo "<br>".$this->i." articles affected.";
   }
}
