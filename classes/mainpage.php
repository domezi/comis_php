<?php
class mainpage implements ArrayAccess {
	public function headpicture($what=null) {
		if($what=="heading") {
			echo "<h1 class='website_title'>".pref("website_title");if(is_sudo())echo "&emsp;<a class='changeopacity icon' href='admin/?action=9&view_next=true&active_field=website_title'><img src=templates/main/images/edit.png alt='' style=height:23px;margin-top:-7px;margin-bottom:-2px;></a>";echo "</h1>";
		}
	}
	public function mkheadpicture($pageid=null,$articleid=null) {
		global $db,$db_prefix;
		$this->headpicture=pref("headpicture");
		if(!is_numeric($pageid)&&is_numeric($articleid)&&$articleid>0) {
			$this->result=$db->query("select * from `".$db_prefix."articles` where id = '".$articleid."'");
			if($this->result)while($this->row = $this->result->fetch_object()){$pageid=$this->row->pageid;}
		}
		if(is_numeric($articleid)&&$articleid>0) {
			$this->result=$db->query("select * from `".$db_prefix."articles` where id = '".$articleid."'");
			if($this->result)while($this->row = $this->result->fetch_object()){if(is_file($this->row->headpicture))$this->headpicture=$this->row->headpicture;}
		} if($this->headpicture==pref("headpicture")&&is_numeric($pageid)&&$pageid>0) {
			$this->result=$db->query("select * from `".$db_prefix."pages` where id = '".$pageid."'");
			if($this->result)while($this->row = $this->result->fetch_object()){if(is_file($this->row->headpicture))$this->headpicture=$this->row->headpicture;}
		}
		return $this->headpicture;
	}
	public function footer() {
		echo ucfirst(text("made_with")).' <a href="http://www.domiscms.de/" style="text-decoration:underline">COMIS</a><a href=http://www.domisseite.de/ style=opacity:0.0;cursor:default;>zu Domis Seite</a>';
	}
	public function loginmenu() {
		echo "using depricated function, please use c_gui->loginmenu instead.";
	}
	public function addcontentmenu() {
		echo "using depricated function, please use c_gui->addcontentmenu instead.";
	}
	public function createhomepage() {
		global $db,$db_prefix,$path;
		if(pref("homepage")=="homefeed") {
			echo"<h2 class=welcomemsg style=display:inline-block>";
			show("welcome_msg");				
			echo"</h2>";
			if(is_sudo())echo"&ensp;<a onmouseup=\"bootbox.prompt('".text("welcomemsg_home")."',function(welcomemsg){if(!welcomemsg.length)return;\$.get('bin/ajax_hook.php?action=changepref&pref=welcome_msg&val='+welcomemsg,function(success){if(success=='1'){bootbox.alert('".text("success")."');\$('.welcomemsg').html(welcomemsg);}else{bootbox.alert('".text("error")."');}});});\" style='cursor:pointer;margin-top:-10px;' class='btn btn-default'><span class='glyphicon glyphicon-pencil'></span></a>";
			if(is_sudo())echo"<div class='alert alert-info hideonmobile' style='margin-top:10px;width:100%;padding: 3px 15px;padding-top:10px;color:#333;background:#fff;border-color:lightgray;' role=alert>".text("homearticles_amnt")." <input style=float:right;margin-top:-6px;margin-right:-10px;width:60px onchange=\"$.ajax({data:'',type:'post',url:'?conf=amnt_home_articles&val='+$(this).val()});\" type=number class=form-control min=0 max=99 value=".conf("amnt_home_articles")."><br class=clear></div>";
			if(pref("welcome_img_yn")!="no") {
				if(file_exists(pref("welcome_img")))
					echo '<img class=thumb src="'.pref("welcome_img").'" width="100%"><br><img src=templates/main/images/shadow_default.png style="margin-top:-4px;" width="100%">';
				else
					echo '<img class=thumb src="images/welcome_img.jpeg" width="100%"><br><img src=templates/main/images/shadow_mirror.png style="margin-top:-4px;" width="100%">';
				if(is_sudo())echo"<div class=hideonmobile style='background:red;height:36px;width:auto;margin:10px;margin-top:-100px;position:absolute;background-color:hsla(0,0%,100%,.8);background-image:url(templates/main/images/magic.gif);background-repeat:no-repeat;background-position:4px 2px;background-size:35px 35px;padding-top:7px;padding-left:50px;padding-right:20px;color:#297bb2;padding-bottom:0px;font-size:20px;box-shadow:0px 0px 10px black;border-radius:4px;border:1px solid white'>".text("welcomeimg")." ".text("upload")."<form action=admin/upload.php?img=welcome_img&action=9&dropdown=0&return2=mainpage method=post enctype='multipart/form-data'><input onchange=\"$('.magic-blackscreen').fadeIn(300);this.form.submit();\" name=welcome_img type=file style=cursor:pointer;opacity:0;padding:0;width:100%;height:100%;position:absolute;left:0;top:0;z-index:3></form></div>";
			}
			$this->result = $db->query("select id,alias from ".$db_prefix."articles where public = 'yes' order by timestamp desc limit ".conf("amnt_home_articles"));
			if($this->result->num_rows) {
				$this->homearticle=new article();
				while($this->row = $this->result->fetch_object()) {
					echo "<br><hr>";
					$this->homearticle->createfromid($this->row->id,false,false);
					echo "<br><a href='";
					echo (conf("modrewrite"))?$this->row->alias.conf("rewrite_suffix"):".?article=".$this->row->id;
					echo"' class='btn btn-default'>&gt;&gt; ".text("view_article")." &lt;&lt;</a><br>";
				}
			}
		}
		elseif(pref("homepage")=="article")echo $db->query("select * from ".$db_prefix."articles where title like 'home'")->fetch_object()->code;
	}
	public function suggests() {
		/*backward scompatible*/
	}
  	public function offsetSet($offset, $value) { if (is_null($offset)) { $this->container[] = $value; } else { $this->container[$offset] = $value; } } public function offsetExists($offset) { return isset($this->container[$offset]); } public function offsetUnset($offset) { unset($this->container[$offset]); } public function offsetGet($offset) { return isset($this->container[$offset]) ? $this->container[$offset] : null; }
}