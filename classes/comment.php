<?php
class comment implements ArrayAccess {
	public function showall($id) {
		if(!is_numeric($id))return;
		global $db,$db_prefix;
		$msgs=@$_SESSION["msgs"];
		if(isset($_POST['msg']))$_SESSION['msgs'][]=$_POST['msg'];
		$this->showform($id);
		if(isset($_POST['msg']) && (!is_array($msgs)||!in_array($_POST["msg"],$msgs))) {
			if(is_user()) {
				$msg=str_replace("<","&lt;",$_POST['msg']);
				$msg=str_replace("'","&#39;",$msg);
				if(conf("censor_comments"))foreach(file("etc/badwords.list") as $badwords) {
					foreach(array($badwords,strtoupper($badwords),strtolower($badwords),ucfirst($badwords)) as $badword){$badword=str_replace("\n","",$badword);$msg=str_replace($badword,@$badword[0].substr("**************",0,(strlen($badword))-2).@$badword[(strlen($badword)-1)],$msg);}
				}
				if($this->insert($id,$_POST["msg"])) $_SESSION['msg']="dsa";
			} else {
				echo $text["for_comments"]." <a href=?inc=login>".$text["login"]."</a> ".$text["or"]." <a href=?inc=register>".$text["register"]."</a>.";	
			}
		}
		$result = $db->query("select * from ".$db_prefix."comments where articleid = '".$id."' order by id desc");
		if($result) {
			while($row = $result->fetch_assoc()) {
				$this->show($row["id"]);
			}
		}
	}
	private function insert($articleid,$msg) {
		global $db,$db_prefix;
		return($db->query("insert into `".$db_prefix."comments`(`userid`,`timestamp`,`articleid`,`msg`) values('".userid()."','".time()."','".$articleid."','".$msg."')"))?true:false;
	}
	public function show($id,$notcensored=false,$pre=false) {
		global $db,$db_prefix,$path;
		if($pre==true)$pre="../";
		$result = $db->query("select msg,username,".$db_prefix."user.id,profilepic from ".$db_prefix."comments join ".$db_prefix."user on ".$db_prefix."comments.userid = ".$db_prefix."user.id where ".$db_prefix."comments.id='".$id."' limit 1");
		if($result) {
		$comment = $result->fetch_assoc();
			echo "<div class='comment'>";
			if($comment["id"]==userid() || is_sudo())
				echo "<div class='remove-comment' onmouseup=\"\$this=\$(this);bootbox.confirm('".text("sure")."',function(response){if(response===true){\$.get('".$pre."bin/ajax_hook.php?action=delete_comment&id=".$id."');\$this.parent().fadeOut();}});\">x</div>";
			if(pref("profilepics")!="no") {
				echo"<div class='profilepic'";
				 if(!empty($comment["profilepic"])) echo " style='background-image:url(\"".$pre.$comment["profilepic"]."\");'";
				echo "></div>";
			} if(is_file("etc/badwords.list")&&$notcensored==false && conf("censor_comments")) {
				foreach(file("etc/badwords.list") as $badwords) {
					foreach(array($badwords,strtoupper($badwords),strtolower($badwords),ucfirst($badwords)) as $badword){$badword=str_replace("\n","",$badword);$comment["msg"]=str_replace($badword,@$badword[0].substr("**************",0,(strlen($badword))-2).@$badword[(strlen($badword)-1)],$comment["msg"]);}
				}
			}
			echo"<b>".ucfirst($comment["username"])."</b>
			<div style=margin-left:48px>".$comment["msg"]."&nbsp;</div></div>";
		}
	}
	private function showform($redirect) {
		echo '<hr><h4 style="text-align:center;">'.text("comments").'</h4><form action="?article='.$redirect.'" method="post" style=margin-bottom:10px;display:block>';
		if(!is_user())echo "<a href=.?inc=login onmouseup=\"bootbox.confirm('".text("sure_login")."',function(confirm){if(confirm===true)window.location.href='.?inc=login';})\" class=removelink style='cursor:pointer !important'>";
		echo'<div class="input-group"><input placeholder="'.text("comments").'" name=msg class="form-control" type=text required';if(!is_user())echo" style='box-shadow:none;border:gray;cursor:pointer;border:1px solid lightgray'";echo'><span class="input-group-btn"><button class="btn btn-default" ';if(!is_user())echo" disabled style=background:white;cursor:pointer";echo'style="padding:6px 20px" type=submit onmouseup="$(this).val(\''.text("posting").'\');">'.text("post").'</button></span></div>';
		if(!is_user())echo "</a>";
		echo'<br class=clear></form>';
	}
	
  	public function offsetSet($offset, $value) { if (is_null($offset)) { $this->container[] = $value; } else { $this->container[$offset] = $value; } } public function offsetExists($offset) { return isset($this->container[$offset]); } public function offsetUnset($offset) { unset($this->container[$offset]); } public function offsetGet($offset) { return isset($this->container[$offset]) ? $this->container[$offset] : null; }
}