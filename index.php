<?php

	/**
	 * @file	index.php 
	 * @author	Dominik Ziegenhagel <domi@domisseite.de>
	 * @date	28.09.2016
	 */

	if(is_file("INSTALL"))header("location:installation");

	session_start();

	include("lib/index_functions.php");

	$index=new index();

	include("includes/do.php");

	if(is_file("etc/dbcon.php")) {
	  include('db.php');
	} else {
		$index->no_db();
		return;
	}

	$c_hook=new c_hook();

	include("includes/website.php");

	$c_hook->call("db_close");

	$db->close();

?>
