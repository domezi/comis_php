<?php

//make sure your path is set correctly
if(is_file("db.php"))$path="";
elseif(is_file("../db.php"))$path="../";
elseif(is_file("../../db.php"))$path="../../";

require_once($path."lib/import_function.php");

// prevent sql injection
function prevsqlinj (&$value, $key) {
	$value = trim(htmlspecialchars($value, ENT_QUOTES));
}
array_walk ($_GET, 'prevsqlinj');
array_walk ($_POST, 'prevsqlinj');

include("etc/dbcon.php");

$db=mysqli_connect($dbcon["host"],$dbcon["username"],$dbcon["password"],$dbcon["name"]);
if(!$db) die(__FILE__.":L".__LINE__.", FATAL ERROR, couldn't connect to database!");

require_once($path."lib/global_functions.php");

if(file_exists($path.'language/'.pref("language").'.php'))
	include($path.'language/'.pref("language").'.php');
elseif(file_exists('language/en.php'))
	include($path.'language/en.php');
?>
