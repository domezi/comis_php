<?php
require("../db.php");
if(!is_sudo())die("must be sudo");
$clean=new cleaner();
$clean->optimize_tables();
$clean->repair_tables();
$clean->check_tables();
$clean->analyze_tables();
$clean->delete_emptys();
$clean->alias();
$clean->logins();
unset($clean);
addlog("aclogfile","Msg","Cleaner wurde ausgef&uuml;hrt.");
?>