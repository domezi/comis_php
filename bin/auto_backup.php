<?php
require_once("../db.php");
if(!is_part_admin())return;
require_once("../etc/backup.conf.php");
if(isset($auto_backup_name))$_GET["name"]=$auto_backup_name;
if(@$_GET["backup_selection"]!="true") {
	foreach($backup_files as $file) {
		$tmp=explode("/",$file);
		$tmp=explode(".",$tmp[1]);
		$_GET["backup_".$tmp[0]]="true";
	}
	$_GET["backup_tables"]="true";
	$_GET["backup_addons"]="true";
	$_GET["backup_language"]="true";
}
$filename="backup_".uniqid().".json";
$backup["bkup_v"]="1";
$backup["timestamp"]=time();
foreach($backup_files as $file) {
	$tmp=explode("/",$file);
	$tmp=explode(".",$tmp[1]);
	if(isset($_GET["backup_".$tmp[0]]) && $_GET["backup_".$tmp[0]]=="true")$backup["files"][$tmp[0]]=@file_get_contents("../".$file);
}
if(isset($_GET["backup_tables"]) && $_GET["backup_tables"]=="true"){foreach(array("articles","pages","user","groups","preferences","comments") as $table) {
	$result = $db->query("select * from `".$db_prefix.$table."` order by id desc");
	while($row = $result->fetch_assoc()) {
		$backup["tables"][$table][]=$row;
	}
}
}
if(isset($_GET["backup_addons"]) && $_GET["backup_addons"]=="true") {$backup["addons"]=array_merge(glob("../addons/*"),glob("../addons/*/*"));}
if(isset($_GET["backup_templates"]) && $_GET["backup_templates"]=="true") {$backup["templates"]=glob("../templates/*");}
if(isset($_GET["backup_language"]) && $_GET["backup_language"]=="true") {
	foreach(glob("../language/*") as $lang) {
		$backup["languages"][]=str_replace("../language/","",str_replace(".php","",$lang));
	}
}
$data=json_encode($backup);
$name=(isset($_GET["name"]))?$_GET["name"]:"AUTO ".date("d.m.y h:i:s");
touch("../backup/".$filename,$data);
if(file_put_contents("../backup/".$filename,$data)) {
	addlog("aclogfile","Msg","Autobackupfile $filename mit ID ".($backupsjson["insert_id"]+1)." wurde angelegt.");
	$backupsjson=@json_decode(@file_get_contents("../var/backups.json"),1);
	$backupsjson["insert_id"]=$backupsjson["insert_id"]+1;
	$backupsjson["last_backup"]=time();
	$backupsjson["backups"][]=array("id"=>$backupsjson["insert_id"],"name"=>$name,"filename"=>$filename,"timestamp"=>time());
	file_put_contents("../var/backups.json",json_encode($backupsjson));
	echo "1";
} else error(201504012004);
?>