<?php
session_start();
require_once("../db.php");
if(!is_sudo()) {
	if(pref("language")=="") {$l="en";}
	else {$l=pref("language");}
	require_once("../language/".$l.".php");
	echo "<body style=background:url(../templates/main/images/bg.jpeg);background-size:cover;color:white;font-family:helvetica><br><br><h1 style=text-align:center;>".$text["admin_require"].".</h1><h2 style=text-align:center;><a href=../?inc=login&username=admin style=color:lightblue;text-decoration:none;>&gt; &gt; ".$text["login"]."  &lt; &lt;</a></h2>";return;
}
require("../etc/backup.conf.php");
$backup=json_decode(@file_get_contents("../backup/".$_GET["filename"]),1);
if(!empty($backup["tables"]))$restore["tables"]=true;
foreach($backup_files as $file) {
	$tmp=explode("/",$file);
	$tmp=explode(".",$tmp[1]);
	if(!empty($backup["files"][$tmp[0]]))$restore[$tmp[0]]=true;
}
if(!empty($backup["addons"]))$restore["addons"]=true;
if($backup["bkup_v"]=="1") {
	if($restore["tables"]) {
		foreach($backup["tables"] as $key=>$table) {
			$db->query("truncate `".$db_prefix.$key."`");
			$tablekey=$key;
			foreach($table as $row) {
				$query="insert into `".$db_prefix.$tablekey."`(";$i=0;$values="";
				foreach($row as $key=>$value) {
					if($i==0)$i=1;else{$query.=",";$values.=",";}
					$query.="`".$key."`";
					$values.="'".$value."'";
				}
				$query.=") values(".$values.")";
				$db->query($query);
			}
		}
	}
	foreach($backup_files as $file) {
		$tmp=explode("/",$file);
		$tmp=explode(".",$tmp[1]);
		if($restore[$tmp[0]]==true)file_put_contents("../".$file,$backup["files"][$tmp[0]]);
	}
} else {$error++;}
if($_GET["fallback"]=="backup") echo "<meta http-equiv=refresh content=1.9,../admin/?action=11&show=".$_GET["show"].">";
echo '<link href="../templates/main/style.css" rel="stylesheet" type="text/css"><link href="../templates/'.pref('admin_template').'/style.css" rel="stylesheet" type="text/css"><body style="/*background:#eee !important*/"><style type="text/css">.bend {	animation:rotateY 2.5s ease-out;	-webkit-animation:rotateY 2.5s ease-out;}@keyframes rotateY {	0%{transform:rotateY(90deg);opacity:0;}	10%{transform:rotateY(90deg);}	35%{opacity:1;}	50%{transform:rotateY(0deg);}	60%{opacity:1;}	80%{opacity:0;}}@-webkit-keyframes rotateY {	0%{-webkit-transform:rotateY(90deg);opacity:0;}	10%{-webkit-transform:rotateY(90deg);}	35%{opacity:1;}	50%{-webkit-transform:rotateY(0deg);}	60%{opacity:1;}	80%{opacity:0;}}</style>';
$icon=(isset($error))?"failed":"success";
$nicht=(isset($error))?" nicht":"";
addlog("aclogfile","Msg","Wiederherstellung des Backups ".$_GET["filename"]." bei ".ucfirst(userinfo("username")).$nicht." erfolgreich verlaufen.");
echo '<img style="position:fixed;top:43%;left:50%;margin-left:-75px;margin-top:-75px;width:150px;height:150px;" src="../templates/main/images/'.$icon.'.png" alt="" class="bend">';
?>