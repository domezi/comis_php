<?php
session_start();
require("../db.php");
if(isset($_GET["action"])) {
	if(is_allowed("preferences") && $_GET["action"]=="changepref" && in_array($_GET["pref"],array("welcome_msg"))) {
		if($db->query("update ".$db_prefix."preferences set value='".$_GET["val"]."' where name='".$_GET["pref"]."'"))echo"1";
	} elseif(is_allowed("articles")&&$_GET["action"]=="update_article_code") {
		file_put_contents("../tmp",$_POST["code"]);
		if(@$db->query("update ".$db_prefix."articles set code='".str_replace("&gt;",">",str_replace("&lt;","<",str_replace("'","&#39;",$_POST["code"])))."' where id='".$_GET["id"]."'"))die("1");
	} elseif(is_allowed("articles")&&$_GET["action"]=="get_article_code") {
		echo @$db->query("select code from ".$db_prefix."articles where id='".$_GET["id"]."' limit 1")->fetch_object()->code;
	} elseif($_GET["action"]=="delete_comment" && is_numeric($_GET["id"])) {
		$result = $db->query("select msg,username,".$db_prefix."user.id,profilepic from ".$db_prefix."comments join ".$db_prefix."user on ".$db_prefix."comments.userid = ".$db_prefix."user.id where ".$db_prefix."comments.id='".$_GET["id"]."' limit 1");
		if(is_object($result)) {
			if($result->num_rows) {
				while($row=$result->fetch_object()) {
					if($row->id == userid() || is_sudo())
						$db->query("delete from ".$db_prefix."comments where ".$db_prefix."comments.id='".$_GET["id"]."'");
				}
			}
		}
	} elseif($_GET["action"]=="loaduserstats" && is_user()) {
		if($_GET["stats"]=="logins") {
			$result = $db->query("select timestamp from ".$db_prefix."statistics where action='login' and userid='".userid()."'");
			if(is_object($result)) {
				if($result->num_rows) {
					echo "<table style='background:white;border-radius:5px;' class='table'>";
					echo "<tr><th class=active>".text("date")." / ".text("time")."</th></tr>";
					while($row=$result->fetch_object()) {
						echo "<tr><td>";
						if(date("d.m.Y")==date("d.m.Y",$row->timestamp))echo date("d.m.Y ",$row->timestamp);
						echo date("H:i",$row->timestamp);
						echo "</td></tr>";
					}
					echo "</table>";
				}
			}
		} elseif($_GET["stats"]=="last_login") {
			$result = $db->query("select * from ".$db_prefix."statistics where action='login' and userid='".userid()."'");
			if(is_object($result)) {
				if($result->num_rows) {
					while($row=$result->fetch_object()) {
						echo "<table style='background:white;border-radius:5px;";if(isset($hideon))echo"display:none;";else $hideon=true;echo"' class='table table-hidden'>";
						echo "<tr><th class=active>".text("date")." / ".text("time")."</th><td>";
						if(date("d.m.Y")==date("d.m.Y",$row->timestamp))echo date("d.m.Y ",$row->timestamp);
						echo date("H:i",$row->timestamp);
						echo "</td></tr>
						<tr><th class=active>".text("ipaddr")."</th><td>".$row->ip."</td></tr>
						<tr><th class=active style=width:200px>".ucfirst(text("user_agent"))."</th><td>".$row->user_agent."</td></tr>
						";
						echo "</table>";
					}
				} if($result->num_rows>1) echo "<a class='btn btn-default' style='float:right;margin-top:-10px' onmouseup=\"$('.table-hidden').fadeIn();$(this).hide();\">".text("show_more")."</a>";
			}
		} elseif($_GET["stats"]=="articles") {
			$result = $db->query("select value,title,".$db_prefix."statistics.timestamp from ".$db_prefix."statistics join ".$db_prefix."articles on ".$db_prefix."statistics.value = ".$db_prefix."articles.id where action='article' and userid='".userid()."'");
			if(is_object($result)) {
				if($result->num_rows) {
					echo "<table style='background:white;border-radius:5px;' class='table'>";
					echo "<tr><th class=active>".text("title")."</th><th class=active>".text("date")." / ".text("time")."</th></tr>";
					while($row=$result->fetch_object()) {
						echo "<tr><td><a href='?article=".$row->value."'><span style='margin:-4px;margin-right:5px;' class='btn btn-default btn-sm glyphicon glyphicon-search'></span></a> ".$row->title."</td><td>";
						if(date("d.m.Y")==date("d.m.Y",$row->timestamp))echo date("d.m.Y ",$row->timestamp);
						echo date("H:i",$row->timestamp);
						echo "</td></tr>";
					}
					echo "</table>";
				}
			}
		} elseif($_GET["stats"]=="searches") {
			$result = $db->query("select value,timestamp from ".$db_prefix."statistics where action='search' and userid='".userid()."'");
			if(is_object($result)) {
				if($result->num_rows) {
					echo "<table style='background:white;border-radius:5px;' class='table'>";
					echo "<tr><th class=active>".text("search")."</th><th class=active style=width:120px>".text("results")."</th><th class=active style=width:200px>".text("date")." / ".text("time")."</th></tr>";
					while($row=$result->fetch_object()) {
						echo "<tr><td><a href='?q=".$row->value."'><span style='margin:-4px;margin-right:5px;' class='btn btn-default btn-sm glyphicon glyphicon-search'></span></a> ".$row->value."</td><td>";
						$found=($db->query("SELECT * FROM `".$db_prefix."articles` WHERE (`code` LIKE '%".$row->value."%' OR `title` LIKE '%".$row->value."%') and public = 'yes' limit 100")->num_rows + $db->query("SELECT ".$db_prefix."comments.id,articleid,alias FROM `".$db_prefix."comments` join `".$db_prefix."articles` on articleid=".$db_prefix."articles.id WHERE (`msg` LIKE '%".$row->value."%') limit 100")->num_rows);
						echo $found;						
						echo "</td><td>";
						if(date("d.m.Y")==date("d.m.Y",$row->timestamp))echo date("d.m.Y ",$row->timestamp);
						echo date("H:i",$row->timestamp);
						echo "</td></tr>";
					}
					echo "</table>";
				}
			}
		} elseif($_GET["stats"]=="comments") {
			$result = $db->query("select articleid,title,msg,".$db_prefix."comments.timestamp from ".$db_prefix."comments join ".$db_prefix."articles on ".$db_prefix."comments.articleid = ".$db_prefix."articles.id where userid='".userid()."'");
			if(is_object($result)) {
				if($result->num_rows) {
					echo "<table style='background:white;border-radius:5px;' class='table'>";
					echo "<tr><th class=active>".text("article")."</th><th class=active>".text("length")."</th><th class=active>".text("date")." / ".text("time")."</th></tr>";
					while($row=$result->fetch_object()) {
						echo "<tr><td><a href='?article=".$row->articleid."'><span style='margin:-4px;margin-right:5px;' class='btn btn-default btn-sm glyphicon glyphicon-search'></span></a> ".$row->title."</td>";
						echo "<td>".strlen($row->msg)." ".text("chars")."</td><td>";
						if(date("d.m.Y")==date("d.m.Y",$row->timestamp))echo date("d.m.Y ",$row->timestamp);
						echo date("H:i",$row->timestamp);
						echo "</td></tr>";
					}
					echo "</table>";
				}
			}
		} elseif($_GET["stats"]=="created_articles") {
			$result = $db->query("select id,title,timestamp from ".$db_prefix."articles where editor='".userinfo("username")."'");
			if(is_object($result)) {
				if($result->num_rows) {
					echo "<table style='background:white;border-radius:5px;' class='table'>";
					echo "<tr><th class=active>".text("title")."</th><th class=active>".text("date")." / ".text("time")."</th></tr>";
					while($row=$result->fetch_object()) {
						echo "<tr><td><a href='?article=".$row->id."'><span style='margin:-4px;margin-right:5px;' class='btn btn-default btn-sm glyphicon glyphicon-search'></span></a> ".$row->title."</td><td>";
						if(date("d.m.Y")==date("d.m.Y",$row->timestamp))echo date("d.m.Y ",$row->timestamp);
						echo date("H:i",$row->timestamp);
						echo "</td></tr>";
					}
					echo "</table>";
				}
			}
		} elseif($_GET["stats"]=="pages") {
			$result = $db->query("select value,title,".$db_prefix."statistics.timestamp from ".$db_prefix."statistics join ".$db_prefix."pages on ".$db_prefix."statistics.value = ".$db_prefix."pages.id where action='page' and userid='".userid()."'");
			if(is_object($result)) {
				if($result->num_rows) {
					echo "<table style='background:white;border-radius:5px;' class='table'>";
					echo "<tr><th class=active>".text("title")."</th><th class=active>".text("date")." / ".text("time")."</th></tr>";
					while($row=$result->fetch_object()) {
						echo "<tr><td><a href='?article=".$row->value."'><span style='margin:-4px;margin-right:5px;' class='btn btn-default btn-sm glyphicon glyphicon-search'></span></a> ".$row->title."</td><td>";
						if(date("d.m.Y")==date("d.m.Y",$row->timestamp))echo date("d.m.Y ",$row->timestamp);
						echo date("H:i",$row->timestamp);
						echo "</td></tr>";
					}
					echo "</table>";
				}
			}
		}
	} elseif($_GET["action"]=="show_statistics" && is_part_admin()) {

		if(isset($_GET["feature"]))$_SESSION["feature"]=$_GET["feature"];
		if(!isset($_SESSION["feature"]))$_SESSION["feature"]="views";
		if(isset($_GET["scale"]))$_SESSION["scale"]=$_GET["scale"];
		if(!isset($_SESSION["scale"]))$_SESSION["scale"]="daily";
		if(isset($_GET["amnt"]))$_SESSION["amnt"]=$_GET["amnt"];
		if(!isset($_SESSION["amnt"]))$_SESSION["amnt"]="5";
		if(isset($_GET["order"])&&$_GET["order"]=="toggle")$_SESSION["order"]=($_SESSION["order"]=="asc")?"desc":"asc";
		if(!isset($_SESSION["order"]))$_SESSION["order"]="desc";
				
		$feature["col"]=$_SESSION["feature"];
		
		/*switch($_SESSION["feature"]) {
			case "views":
				$feature["col"]="views";
				break;
			case "browser":
				$feature["col"]="browser";
				break;
			case "user":
				$feature["col"]="user";
				break;
		}
		*/
		
		$result=$db->query("select * from ".$db_prefix."statistics where action='article' and value='".$_GET["id"]."' order by timestamp desc");
		if(is_object($result)) {
			if($result->num_rows) {
				$scale["active"][$_SESSION["scale"]]=" class=active";
				$scale["amnt"][$_SESSION["amnt"]]=" class=active";
				$scale["feature"][$_SESSION["feature"]]=" class=active";
				$jqueryselectorpre="$('.article".$_GET["id"]."').find('.load-statistics').";
				$jqueryselectorcd=null;
				$jqueryselectorapp=null;
				$onclose=null;
				$header=text("statistics");
				if(@$_GET["jqueryselectorpre"]=="false") {
					$jqueryselectorpre="$('.article".$_GET["id"]."').";
					$jqueryselectorcd="../";
					$onclose="$('.article".$_GET["id"]."-stat').show();";
					$jqueryselectorapp="&jqueryselectorpre=false";
					$header=text("article").": ".$db->query("select title from ".$db_prefix."articles where id='".$_GET["id"]."' limit 1")->fetch_object()->title;
				}
				echo "<div class='well' style='background:white'>
				<h2 style=margin:0;display:inline-block;>".$header."</h2>
				<div class='btn-group' style='float:right;margin-top:-5px'>
				  	<button type='button' class='btn btn-default dropdown-toggle' data-toggle='dropdown' aria-expanded='false'>
				    	<span class='glyphicon glyphicon-cog'></span> ".text("preferences")." <span class='caret'></span>
				  	</button>
					<ul class='dropdown-menu' role='menu'>
						<li><a style=cursor:pointer onmouseup=\"".$jqueryselectorpre."load('".$jqueryselectorcd."bin/ajax_hook.php?action=show_statistics&id=".$_GET["id"]."".$jqueryselectorapp."');\">".ucfirst(text("refresh"))."</a></li>
						";
						echo "<li><a style=cursor:pointer onmouseup=\"".$jqueryselectorpre."load('".$jqueryselectorcd."bin/ajax_hook.php?action=show_statistics&id=".$_GET["id"]."&order=toggle".$jqueryselectorapp."');\">".ucfirst(text("order_".$_SESSION["order"]))."</a></li>";
						echo "<li class='divider'></li>
				   	<li".@$scale["feature"]["views"]."><a style=cursor:pointer onmouseup=\"".$jqueryselectorpre."load('".$jqueryselectorcd."bin/ajax_hook.php?action=show_statistics&id=".$_GET["id"]."&feature=views".$jqueryselectorapp."');\">".ucfirst(text("views"))."</a></li>
				   	<li".@$scale["feature"]["browser"]."><a style=cursor:pointer onmouseup=\"".$jqueryselectorpre."load('".$jqueryselectorcd."bin/ajax_hook.php?action=show_statistics&id=".$_GET["id"]."&feature=browser".$jqueryselectorapp."');\">".ucfirst(text("browser"))."</a></li>
				   	<li".@$scale["feature"]["user"]."><a style=cursor:pointer onmouseup=\"".$jqueryselectorpre."load('".$jqueryselectorcd."bin/ajax_hook.php?action=show_statistics&id=".$_GET["id"]."&feature=user".$jqueryselectorapp."');\">".ucfirst(text("user"))."</a></li>
				   	<li".@$scale["feature"]["detailed"]."><a style=cursor:pointer onmouseup=\"".$jqueryselectorpre."load('".$jqueryselectorcd."bin/ajax_hook.php?action=show_statistics&id=".$_GET["id"]."&feature=detailed".$jqueryselectorapp."');\">".ucfirst(text("detailed"))."</a></li>
						";if($_SESSION["feature"]=="views") {
							echo "<li class='divider'></li>
					   	<li".@$scale["active"]["monthly"]."><a style=cursor:pointer onmouseup=\"".$jqueryselectorpre."load('".$jqueryselectorcd."bin/ajax_hook.php?action=show_statistics&id=".$_GET["id"]."&scale=monthly".$jqueryselectorapp."');\">".ucfirst(text("monthly"))."</a></li>
					   	<li".@$scale["active"]["daily"]."><a style=cursor:pointer onmouseup=\"".$jqueryselectorpre."load('".$jqueryselectorcd."bin/ajax_hook.php?action=show_statistics&id=".$_GET["id"]."&scale=daily".$jqueryselectorapp."');\">".ucfirst(text("daily"))."</a></li>
					   	<li".@$scale["active"]["hourly"]."><a style=cursor:pointer onmouseup=\"".$jqueryselectorpre."load('".$jqueryselectorcd."bin/ajax_hook.php?action=show_statistics&id=".$_GET["id"]."&scale=hourly".$jqueryselectorapp."');\">".ucfirst(text("hourly"))."</a></li>
					   	<li".@$scale["active"]["minutely"]."><a style=cursor:pointer onmouseup=\"".$jqueryselectorpre."load('".$jqueryselectorcd."bin/ajax_hook.php?action=show_statistics&id=".$_GET["id"]."&scale=minutely".$jqueryselectorapp."');\">".ucfirst(text("minutely"))."</a></li>
					   	";
					   }
					   if($_SESSION["feature"]!="browser") {
							echo"<li class='divider'></li>";
						   foreach(explode(",","5,10,20,50,100,200,500") as $amnt) {
								echo "<li".@$scale["amnt"][$amnt]."><a style=cursor:pointer onmouseup=\"".$jqueryselectorpre."load('".$jqueryselectorcd."bin/ajax_hook.php?action=show_statistics&id=".$_GET["id"]."&amnt=$amnt".$jqueryselectorapp."');\">".ucfirst(text("show"))." max. $amnt</a></li>";
						   }
					   }
					   echo "
						<li class='divider'></li>
						<li><a style=cursor:pointer onmouseup=\"".$onclose.$jqueryselectorpre."hide({effect:'blind'});\">".ucfirst(text("close"))."</a></li>
				  		</ul>
				</div>
				";
				
				$days=null;
				
				switch($_SESSION["scale"]) {
					case "monthly":
						$scale["th"]=text("month");
						$scale["date_format"][0]="Ym";
						$scale["date_format"][1]="M";
						break;
					case "daily":
						$scale["th"]=text("day");
						$scale["date_format"][0]="Ymd";
						$scale["date_format"][1]="D,d";
						break;
					case "hourly":
						$scale["th"]=text("a_clock");
						$scale["date_format"][0]="YmdH";
						$scale["date_format"][1]="H";
						break;
					case "minutely":
						$scale["th"]=text("a_clock");
						$scale["date_format"][0]="YmdHi";
						$scale["date_format"][1]="H:i";
						break;
				}
				
				if($_SESSION["feature"]=="detailed") {
					while($row=$result->fetch_assoc()) {
						if(count(@$detailed)<$_SESSION["amnt"]) {
							$detailed[]=$row;
						}
					}
				} elseif($_SESSION["feature"]=="views") {
					while($row=$result->fetch_object()) {
						if(count(@$days)<$_SESSION["amnt"]) {
							@$days[date($scale["date_format"][0],$row->timestamp)]["views"]++;
							$days[date($scale["date_format"][0],$row->timestamp)]["date"]=$row->timestamp;
						}
					}
				} elseif($_SESSION["feature"]=="browser") {
					while($row=$result->fetch_object()) {
						@$browsers[parse_user_agent($row->user_agent)]["times_used"]++;
						$browsers[parse_user_agent($row->user_agent)]["date"]=$row->timestamp;
						$browsers[parse_user_agent($row->user_agent)]["browser"]=parse_user_agent($row->user_agent);
					}
				} elseif($_SESSION["feature"]=="user") {
					while($row=$result->fetch_object()) {
						if(count(@$users)<$_SESSION["amnt"]) {
							@$users[$row->userid]["views"]++;
							$users[$row->userid]["userid"]=$row->userid;
						}
					}
				}/* elseif($_SESSION["feature"]=="locations") {
					while($row=$result->fetch_object()) {
						if(count(@$locations)<$_SESSION["amnt"]) {
							$geo = unserialize(@file_get_contents("http://www.geoplugin.net/php.gp?ip=".$row->ip));
							@$locations[$geo["geoplugin_city"]]["views"]++;
						}
					}
				}*/
				
//				alert(var_dump($users));

				$meta=null;

				if($_SESSION["feature"]=="views") {
					if($_SESSION["order"]=="asc")$days=array_reverse($days);
					foreach($days as $day) {
						if($day["views"]>$meta["most"]) {
							$meta["most"]=$day["views"];
						}
					}
				} elseif($_SESSION["feature"]=="browser") {
					
					foreach($browsers as $key=>$browser) {
//						$key=$tmps[$browser["times_used"]];
//						@$tmps[$browser["times_used"]]["date"]=$browser["browser"];
						
						$tmps[$key]=$browser["times_used"];
						@$tmps[$key]["date"]=$browser["date"];
						
//						echo "<hr>".$browser;
					}

					asort($tmps);
					
					foreach($tmps as $key=>$tmp) {
						$addon="#".uniqid();
						$new_browsers[$tmp.$addon]["times_used"]=$tmp;				
						$new_browsers[$tmp.$addon][$tmp["date"]]=$tmp["date"];				
						$new_browsers[$tmp.$addon]["browser"]=$key;				
					}
					
					$browsers=$new_browsers;
					
					if($_SESSION["order"]=="desc")$browsers=array_reverse($browsers);
					
					foreach($browsers as $browser) {
						if($browser["times_used"]>$meta["most"]) {
							$meta["most"]=$browser["times_used"];
						}
					}
				} elseif($_SESSION["feature"]=="user") {
								
									foreach($users as $key=>$user) {
//										$key.="#".uniqid();
										$userviews[$key]=$user["views"];
										@$userviews[$key]["userid"]=$user["userid"];
									}

									asort($userviews);
									foreach($userviews as $userid=>$views) {
										$addon="#".uniqid();
										$new_users[$userid.$addon]["views"]=$views;				
										$new_users[$userid.$addon]["userid"]=$userid;				
									}
									$users=$new_users;
					
					if($_SESSION["order"]=="asc")$users=array_reverse($users);
					foreach($users as $user) {
						if($user["views"]>$meta["most"]) {
							$meta["most"]=$user["views"];
						}
					}
				}/* elseif($_SESSION["feature"]=="locations") {
					if($_SESSION["order"]=="asc")$locations=array_reverse($locations);
					foreach($locations as $location) {
						if($location["views"]>$meta["most"]) {
							$meta["most"]=$location["views"];
						}
					}
				}*/
						
				if($_SESSION["feature"]=="views") {
					echo "<table class='table' style=margin-top:15px><tr>
						<th style=border-top:none;>".$scale["th"]."</th>
						<th style=border-top:none>".ucfirst(text($feature["col"]))."</th>
					</tr>";
					foreach($days as $day) {
						echo "<tr>
							<td>".date($scale["date_format"][1],$day["date"])."</span>
							</td><td>".$day["views"]."</td><td width=100%>
								<div class=progress style=margin:0;>
								  <div class='progress-bar progress-bar-info' role=progressbar aria-valuenow=".($day["views"]/$meta["most"]*90)." aria-valuemin=0 aria-valuemax=".$meta["most"]." style=width:".($day["views"]/$meta["most"]*90)."%;></div>
								</div>
							</td>
						</tr>";
					}
				} elseif($_SESSION["feature"]=="browser") {
					echo "<table class='table' style=margin-top:15px><tr>
						<th style=border-top:none;>".ucfirst(text("browser"))."</th>
						<th style=border-top:none>".ucfirst(text("amount"))."</th>
					</tr>";
					foreach($browsers as $browser) {
						echo "<tr>
							<td width=25%><span class='browser-icon browser-icon-".$browser["browser"]."'></span>&emsp;&emsp;".ucfirst($browser["browser"])."</span>
							</td><td>".$browser["times_used"]."</td><td width=70%>
								<div class=progress style=margin:0;>
								  <div class='progress-bar progress-bar-info' role=progressbar aria-valuenow=".($browser["times_used"]/$meta["most"]*90)." aria-valuemin=0 aria-valuemax=".$meta["most"]." style=width:".($browser["times_used"]/$meta["most"]*90)."%;></div>
								</div>
							</td>
						</tr>";
					}
				} elseif($_SESSION["feature"]=="user") {
					echo "<table class='table' style=margin-top:15px><tr>
						<th style=border-top:none;>".ucfirst(text("username"))."</th>
						<th style=border-top:none>".ucfirst(text("views"))."</th>
					</tr>";
					foreach($users as $user) {
	//						alert(userinfo("profilepic",@$user["userid"]));
							$userinfo=userinfo("profilepic",@$user["userid"]);
							if(!empty($userinfo))$profilepic="background-image:url('".userinfo("profilepic",@$user["userid"])."')";else $profilepic=null;
							if(!empty($user["userid"])) {$username=ucfirst(userinfo("username",@$user["userid"]));} else {$profilepic="background-image:url(templates/main/images/profilepic_unknown.jpg)";$username=text("other_users");}
							echo "<tr>
								<td width=25%><div class=profilepic style=\"height:30px;width:30px;margin:-5px;$profilepic\"></div>&ensp;".$username."</span>
								</td><td>".$user["views"]."</td><td width=70%>
									<div class=progress style=margin:0;>
									  <div class='progress-bar progress-bar-info' role=progressbar aria-valuenow=".($user["views"]/$meta["most"]*90)." aria-valuemin=0 aria-valuemax=".$meta["most"]." style=width:".($user["views"]/$meta["most"]*90)."%;></div>
									</div>
								</td>
							</tr>";
					}
				} elseif($_SESSION["feature"]=="detailed") {
					echo "<table class='table' style=margin-top:15px><tr>
						<th style=border-top:none;>".ucfirst(text("username"))."</th>
						<th style=border-top:none;>IP</th>
						<th style=border-top:none;>".ucfirst(text("date"))."</th>
						<th style=border-top:none;>".ucfirst(text("browser"))."</th>
						<th style=border-top:none;>".ucfirst(text("os"))."</th>
					</tr>";
					foreach($detailed as $detail) {
						$detail_username=($detail["userid"])?ucfirst(userinfo("username",$detail["userid"])):text("unknown");
						echo "<tr>
							<td>".$detail_username."</td>
							<td>".$detail["ip"]."</td>
							<td>".date("d.m H:i:s",$detail["timestamp"])."</td>
							<td><span class='browser-icon browser-icon-".parse_user_agent($detail["user_agent"])."'></span>&emsp;&emsp;".ucfirst(parse_user_agent($detail["user_agent"]))."</td>
							<td><span class='os-icon os-icon-".get_os($detail["user_agent"])."'></span>&emsp;&emsp;".ucfirst(get_os($detail["user_agent"]))."</td>
							</td>
						</tr>";
					}
				}

				echo "</table>
				</div>";
			}
		}
	} elseif($_GET["action"]=="check_avaiability_username") {
		if(strlen($_GET["username"])<3) {
			echo "0";return;
		}
		$result=$db->query("select * from ".$db_prefix."user where username like '".$_GET["username"]."'");
		if(is_object($result)) {
			if($result->num_rows<1)echo"1";else echo"0";
		} else echo "0";
	}
}
?>