<?php
if(isset($_GET["backup_file"])) {
	if(strstr($_GET["backup_file"],"/"))return;
	header("Content-Type: application/octet-stream");
	header("Content-Disposition: attachment; filename=\"../backup/".$_GET["backup_file"]."\"");
	header('Content-disposition: filename="comis_backup_'.date("Ymd_Hi").'.json"');
	readfile("../backup/".$_GET["backup_file"]);
	addlog("aclogfile","Msg","Das Backupfile ".$_GET["backup_file"]." wurde heruntergeladen.");
}
else {
$file = "../files/tmp/comis_export_".$_GET["uniqid"].".zip";
if(!file_exists($file)) {echo "<script>alert('This file has been downloaded already!');</script>";
echo "<meta http-equiv=refresh content=0,../admin/?action=11&Wiederherstellung><script>window.location.href='../admin/?action=11&Wiederherstellung';</script>";return;}
header("Content-Type: application/force-download");
header("Content-Disposition: attachment; filename=\"".$file."\"");
header('Content-disposition: filename="comis_exported.zip"');
readfile($file);
unlink($file);
	addlog("aclogfile","Msg","Das Comis-Exportfile wurde heruntergeladen.");
}
?>