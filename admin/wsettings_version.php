<?php include_once("../db.php");if(!is_sudo())return;
$tmp=import_conf("../var/_dash_widgets_prefs.conf");
$checked=($tmp["auto_update"]=="true")?true:false;
?>
<div style="text-align:center;width:96%;display:block" class="widget-version-settings-toggle-parent"><br>
 Comis soll selbständig das Update einleiten, falls eine neue Version vorhanden ist. <br>
 	<br>
 	<div class="<?php if($checked==true)echo'active';?>" onmouseup="$('.active').removeClass('active');$(this).addClass('active');$.get('reorder.php?name=auto_update&value=true');" style='border-radius:5px 0px 0px 5px;'>
		Ja
	</div>
	<div class="<?php if($checked==false)echo'active';?>" onmouseup="$('.active').removeClass('active');$(this).addClass('active');$.get('reorder.php?name=auto_update&value=false');" style='border-radius:0px 5px 5px 0px;'>
		Nein
	</div><br>
<br><i>(Sie werden vor dem updaten auf jeden Fall noch einmal um eine Bestätigung gebeten.)</i><br><br>
</div>
<style type="text/css">
.widget-version-settings-toggle-parent div {
	width: 30%;
	display: inline-block;
	background:hsla(0,0%,100%,.3);
	border:1px solid hsla(0,0%,100%,.5);
	margin-left: -6px;
	cursor: pointer;
}
.widget-version-settings-toggle-parent div.active,.widget-version-settings-toggle-parent div:hover {
	background:hsla(0,0%,100%,.8);
}
</style>