<?php include_once("../db.php");if(!is_sudo())return;?><h1 class='widget-headline'>Kommandozeile</h1>
<form><input autofocus="" name="command" style="padding:0px 1%;width:97%;background:hsla(0,0%,100%,.15);font-family:true-type;font-size:16px;border:1px solid hsla(0,0%,100%,.4);margin-bottom:5px"  class="command-line" onchange="this.form.submit();"></form><style type="text/css">
.ui-menu-item {width: auto !important;}
</style>
<?php
//value="<imper_edit_article_1>"
echo '<script>$(function() {
		var commandline = [
		';
		echo '"Hilfe #HELP"';
		$result=$db->query("select * from ".$db_prefix."articles");
		while($row=$result->fetch_object()) {
			echo ',"Artikel '.$row->title.' bearbeiten #ea'.$row->id.'"';
			echo ',"Artikel '.$row->title.' loschen #da'.$row->id.'"';
		}
		$result=$db->query("select * from ".$db_prefix."pages");
		while($row=$result->fetch_object()) {
			echo ',"Menupunkt '.$row->title.' bearbeiten #ep'.$row->id.'"';
			echo ',"Menupunkt '.$row->title.' loschen #dp'.$row->id.'"';
		}
		$result=$db->query("select * from ".$db_prefix."user");
		while($row=$result->fetch_object()) {
			echo ',"Benutzer '.$row->username.' bearbeiten #eu'.$row->id.'"';
			echo ',"Benutzer '.$row->username.' loschen #du'.$row->id.'"';
		}
		$result=$db->query("select * from ".$db_prefix."groups");
		while($row=$result->fetch_object()) {
			echo ',"Gruppe '.$row->name.' loschen #dg'.$row->id.'"';
		}
		echo '
		];
		 function split( val ) {
		return val.split( /,\s*/ );
		}
		function extractLast( term ) {
		return split( term ).pop();
		}
		$(".command-line").bind( "keydown", function( event ) {
		if ( event.keyCode === $.ui.keyCode.TAB &&
		$( this ).autocomplete( "instance" ).menu.active ) {
		}
		})
		.autocomplete({
		minLength: 0,
		source: function( request, response ) {
		response( $.ui.autocomplete.filter(
		commandline, extractLast( request.term ) ) );
		},
		focus: function() {
		return false;
		}
		});
		});</script>';