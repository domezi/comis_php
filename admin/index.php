<?php session_start();?>
<!DOCTYPE html>
<html>
<head>
<?php

	if(isset($_GET["command"]))include("commandconv.php");
	$adminaccess = @$_COOKIE['adminaccess'];
	$action = @$_GET["action"];
	require_once('../db.php');
	require_once("../lib/admin_functions.php");

	if(pref("language")=="") {$l="en";} else {$l=pref("language");}
	require_once("../language/".$l.".php");
	

	if(!is_part_admin()) {
			if(is_user()){refresh("../");die();}
			if(isset($_POST['login'])) {
				$user=new user();
				if($user->canlogin($_POST["username"],$_POST["password"])) {
					$login=new login();
					$_SESSION["hash"]=$login->createSession($_POST["username"],$_POST["password"]);
					echo '<meta http-equiv=refresh content=0,../?do=login&admin><script type="text/javascript">';
				} else echo text("error_by_login")." <a href=.>".ucfirst(text("login"))." ></a>";
			} else {
			echo '
			<link rel=stylesheet href=../templates/'.pref('admin_template').'/style.css>
			<link rel=stylesheet href=../lib/bootstrap.css>
			<script type="text/javascript" src="../lib/jquery.js"></script>
			<script src="../lib/jquery-ui.js"></script>
			<link rel="stylesheet" href="../lib/jquery-ui-smoothness.css">
			<link rel="stylesheet" href="../lib/jquery-ui.theme.css">
			<body id=admin style=color:#333;font-family:helvetica><br><br>
			<div class="container fade-in" style="left:50%;position:fixed;margin-left:-300px !important;top:50%;margin-top:-300px !important;width:600px !important;background:hsla(0,0%,90%,.9);padding:30px 50px 50px;border-radius:10px">
				<h1 style=text-align:center;>'.$text["log-in"].'</h1>
				<p style=text-align:center;>'.$text["plz_login"].'</p><br><br>
				<form action="" method="post">
				<input style=margin-bottom:10px; class=form-control name=username value="'.@$_GET["username"].'" placeholder="'.ucfirst($text["username"]).'" required autofocus>
				<input class=form-control name=password type="password" placeholder="'.ucfirst($text["password"]).'" required>
				<input class=form-control type=submit style="margin-top:10px;" value="'.ucfirst($text["login"]).'" name="login"></form>
				<br style=clear:both>
			</div>
			<script>$(function(){$(".fade-in").hide().delay(100).fadeIn(700);});</script>
			';
			}
		
		return;
		die("<body style=background:url(../templates/main/images/bg.jpeg);background-size:cover;color:white;font-family:helvetica><br><br><h1 style=text-align:center;>".$text["admin_require"].".</h1><h2 style=text-align:center;><a href=../?inc=login&username=admin style=color:lightblue;text-decoration:none;>&gt; &gt; ".$text["login"]." &lt; &lt;</a></h2>");
	}



	foreach(glob("../classes/*.php") as $class) {
		include_once($class);	
	}

	$c_hook=new c_hook();
	$c_gui=new c_gui();
	$c_system=new c_system();
	$c_manage=new c_manage();
	$c_get=new c_get();
	
	$result=$db->query("select * from ".$db_prefix."addons");
	if($result)while($row=$result->fetch_object()) {
		include_once('../addons/'.$row->alias.'/adminpage.php');
	}

	$c_hook->call("admin_header");


	$tablebyaction=array(null,"pages","articles","user","groups");
	$preview_target=false;
	$style=false;
	$private=import_conf("../etc/private.conf");
	$i=0;
	if(conf("hash_pass_salt_activate"))$salt=conf("hash_pass_salt");
	
	if(conf("auto_backup") && (!isset($_SESSION["next_backup"])||$_SESSION["next_backup"]<=time())) {
		echo "<a href=?action=11>
		<div class='changeopacity notification'>
		<img src=../templates/main/images/icon_backup.gif style=margin-bottom:-7px;margin-top:-2px;margin-right:10px;>
		".$text["backup_was_made"]."
		</div>		
		</a>";
		if(isset($_SESSION["next_backup"]))include_once('../bin/auto_backup.php');
		$backupsjson=json_decode(@file_get_contents("../var/backups.json"),1);
		$_SESSION["next_backup"]=$backupsjson["last_backup"]+conf("backup_interval");
	}
	if(isset($_GET["create_empty"])) {
		if(is_allowed($tablebyaction[$_GET["action"]]))
		$db->query("insert into `".$db_prefix.$tablebyaction[$_GET["action"]]."`() values()");
	}

	foreach(array("pages","articles","user","groups","comments","newsletter","prefs") as $str) {
		$access[$str]=(is_allowed($str))?"yes":"no";
	}
	if(!isset($_SESSION["domiscms_latest_version"]))$_SESSION["domiscms_latest_version"]=@file_get_contents("http://domiscms.de/bin/print_v.php");
	echo "<title>".pref("website_title")." - ".$text['a_dmin']."</title>";?>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8">
	<meta http-equiv="content-style-type" content="text/css">
	<script src="../lib/jquery.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
	<link rel="shortcut icon" href="../<?php show('shortcut'); ?>" type="image/x-icon"/>
	<?php echo '<link href="../templates/'.pref('admin_template').'/style.css" rel="stylesheet" type="text/css">';?>
	<link href="../templates/main/style.admin.mobile.css" rel="stylesheet" type="text/css">
	<link href="../templates/main/style.css" rel="stylesheet" type="text/css">
	<script src="//cdn.ckeditor.com/4.5.6/full/ckeditor.js"></script>
	<script type="text/javascript" src="../lib/global_functions.js"></script>
	<script src="../lib/jquery-ui.js"></script>
	<link rel="stylesheet" href="../lib/jquery-ui-smoothness.css">
	<link rel="stylesheet" href="../lib/jquery-ui.theme.css">
	<link href="../lib/bootstrap.css" rel="stylesheet" type="text/css">
	<script src="../lib/bootstrap.js"></script>
	<script src="../lib/bootbox.js"></script>
	<script>
	$(function() {
		$.datepicker.formatDate( "yy.mm-dd", new Date( 2007, 1-1, 26 ));
		$(".datepicker").datepicker();
		$(".leftnav-toggle").on("click",function () {
			$(".leftnav-bscreen").toggle();
		});
	});
	</script>
	<script type="text/javascript">
	function addfavorite(url) {
		id=$(".favorite-icon").parent().attr("data-id");
		if ($(".favorite-icon").hasClass("glyphicon-star")) {
			bootbox.confirm("Wollen Sie dieses Lesezeichen wirklich entfernen?",function (confirm) {
				if (confirm===true) {
					$.get("ajax_hook.php?action=removefavorite&id="+id,function () {
						bootbox.alert("Diese Seite wurde erfolgreich von Ihren Favouriten entfernt!");
						$(".favorite-icon").addClass("glyphicon-star-empty").removeClass("glyphicon-star");
					});
				}
			});
		} else {
			bootbox.prompt("Beschreibung des Lesezeichens",function (title) {
				if (title!==null) {
					$.get("ajax_hook.php?action=addfavorite&title="+title.replace(/&/g,"%26")+"&url="+url.replace(/&/g,"%26"),function (idreturn) {
						bootbox.alert("Diese Seite wurde erfolgreich zu Ihren Favouriten hinzugef&uuml;gt!");
						$(".favorite-icon").parent().attr("data-id",idreturn);
						$(".favorite-icon").addClass("glyphicon-star").removeClass("glyphicon-star-empty");
					});
				}
			});
		}
	}
	</script>
	</head>
	<?php


	if(!isset($_GET["min"])) {
		?>	
		<body id="admin" style="background-attachment:fixed;">
		$c_hook->call("admin_body");
		<div id=adminheader><img src="../images/logo2.png" style="position:absolute;top:15px;opacity:.7;left:10px;height:40px;" onmouseup="goathail()"><h1 style="text-align:center"><?php echo $text['adminheader']; ?></h1></div>
		<div class="leftnav-bscreen"></div>
		<div class="leftnav-toggle"></div>
		<nav class="leftnav sprite">
		$c_hook->call("admin_leftnav");
		<?php require("leftnav.php"); ?>
		</nav>
		<div id="beforecontainer"></div>
		<div id="admincontainer" style="padding:50px;overflow:auto;">
		<div style="display:block;position:absolute;left:10px;top:10px;">
			<?php
			$empty="-empty";$id=null;
			if(is_file("../var/favorites.json"))$favorites=file_get_contents("../var/favorites.json");
			if(is_file("../var/favorites.json")&&!empty($favorites))$favorites=json_decode(file_get_contents("../var/favorites.json"),1);else $favorites=null;
			if(isset($favorites["favorites"][userid()])&&count($favorites["favorites"][userid()])>0) {
				foreach($favorites["favorites"][userid()] as $index=>$favorite) {
					if(str_replace("&amp;","&",$favorite["url"])==$_SERVER["REQUEST_URI"]) {
						$empty=null;$id=$index;
					}
				}
			}
			echo '<a class="addfavorite" style=cursor:pointer onmouseup="addfavorite(\''.$_SERVER["REQUEST_URI"].'\');" data-id=\''.$id.'\'><span style="color:#333;font-size:25px" class="favorite-icon glyphicon glyphicon-star'.$empty.'"></span></a>';
			?>
		</div>
		<?php
	}

	$c_hook->call("admin_actioncall_append");

	if(@$action==0) {
		require("admin_dashboard.php");
	}
	if(@$action==1 && $access["pages"]=="yes") {
		require("admin_pages.php");
	}
	if(@$action==2 && $access["articles"]=="yes") {
		require("admin_articles.php");
	}
	if(@$action==3 && $access["user"]=="yes") {
		require("admin_user.php");
	}
	if(@$action==4 && $access["groups"]=="yes") {
		require("admin_groups.php");
	}
	if(@$action==5 && $access["comments"]=="yes") {
		require("admin_comments.php");
	}
	if(@$action==6 && $access["newsletter"]=="yes") {
		require("admin_newsletter.php");
	}
	if(@$action==8 && is_sudo()) {
		require("admin_addons.php");
	}
	if($action==9 && $access["prefs"]=="yes") {
		require("admin_preferences.php");
	}
	if($action==10) {
		require("admin_statistics.php");
	}
	if(@$action==11 && is_sudo()) {
		require("admin_backup.php");
	}
	if(@$action==100 && is_sudo() && conf("comisinfo")) {
		require("comisinfo.php");
	}
	if(@$action==102 && is_sudo() && conf("phpinfo")) {
		phpinfo();
	}
	if(@$action==104 && is_sudo() && conf("phpinfo")) {
		if(is_file("../etc/private.conf")) {
			alert("System already registered!",1);
		}
		elseif(is_sudo()){
			if(file_put_contents("../etc/private.conf",@file_get_contents("http://www.domiscms.de/register_system.php?owner=".userinfo("name")."&version=".v()."&website_adress=".@$_SERVER["HTTP_REFERER"]."&email=".userinfo("email"))))
				alert(text("system_registered_successfully").".",1);
		}
		else alert("Please ask Your administrator to perform this action!",1);
	}
	if(@$action==200 && is_sudo() && conf("comiseditor")) {
		require("comiseditor.php");
	}

	echo "</div>";
	if(isset($_GET['return2'])&&$_GET['return2']=="mainpage")refresh("../");
	if(isset($_GET["update"]) && is_sudo()) require("admin_update.php");
	echo "</div>";
	if(conf('contextmenu'))require("contextmenu.php");
	if(!empty($style))echo "<style>".$style."</style>";
	$c_hook->call("admin_footer");
	?>
	<script type="text/javascript" src="../lib/admin.js"></script>
	<script type="text/javascript">$(function(){$(".removelink").attr("href","#");});
	w=1;function goathail(){if(w==10){window.open("http://domiscms.de/images/goathail.jpg", '_blank');w=0;}w=w+1;};</script><?php return; ?></body></html>
