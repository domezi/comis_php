<div id="loading" style="position:absolute;left:50%;top:50%;margin-left:-50px;margin-top:-50px;"><img style="width:100px;height:100px" src="../templates/main/images/load.png" class="rotatefast"></div>
<script type="text/javascript">
$(function () {
	setTimeout(function () {
		$("#admincontainer").append("<div class=dashboard-wrapper></div>");
		$(".dashboard-wrapper").hide().load("admin_dashboard_load.php",function () {
			$("#loading").fadeOut(400);
			$(".dashboard-wrapper").delay(400).fadeIn(500);
		});
	},400);
});
</script>