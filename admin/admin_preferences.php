<?php
foreach(explode(",","website_template,admin_template,welcome_img,welcome_img_yn,shortcut,profilepics,comments,homepage,website_title,aside,search,home_article,contact_person,website_description,website_title,welcome_msg,bg_img,background,headpicture,headpicture_yn,background_yn,color_theme,maintenance") as $pref) {
	if(isset($_POST[$pref])||isset($_GET[$pref])) {
		$valpref=(isset($_POST[$pref]))?$_POST[$pref]:$_GET[$pref];
		if($db->query("update ".$db_prefix."preferences set value = '".$valpref."' where name = '".$pref."'")){$success=true;addlog("aclogfile","Msg","ADMIN: Eine Einstellung wurde von ".ucfirst(userinfo("username"))." ge&auml;ndert. (".$valpref."/".$pref.")");}
		if(in_array($pref,explode(",","website_template,admin_template"))) $reload_site=true;
	}
}
if(isset($_POST["pages_enabled"])) {
	$db->query("update ".$db_prefix."preferences set value = '".$_POST['pages_enabled']."' where name = 'pages_enabled'");
	$db->query("update ".$db_prefix."preferences set value = '".$_POST['pages_enabled']."' where name = 'comments'");
	addlog("aclogfile","Msg","ADMIN: Paging von ".ucfirst(userinfo("username"))." ge&auml;ndert.");	
$success=true;}
if(isset($_POST["language"])) {
	if($_POST["language"]!="de" && $_POST["language"]!="en")
		alert($text["third_translation_warn"]);
	$db->query("update ".$db_prefix."preferences set value = '".$_POST['language']."' where name = 'language'");
	addlog("aclogfile","Msg","ADMIN: Sprache von ".ucfirst(userinfo("username"))." gewechselt.");
$reload_site=true;$success=true;}
if(isset($reload_site))
	echo "<script>window.location.href=window.location.href;</script>";	
if(isset($success))
	echo "<div class='alert alert-success' role='alert'>".$text['changes_saved']."</div>";
if(@$_GET['dropdown']==0) {
?>
<table width=100% id="adminprefs">
<tr>
<td width=33% style="vertical-align:top;">
<form action="?action=9&dropdown=0" method="post">
<?php echo $text['welcomemsg_home']; ?>
<br>
<input class="form-control" style="width:300px;" onchange="this.form.submit()" class="welcome_msg" name="welcome_msg" style="width:300px;" value="<?php show('welcome_msg'); ?>">
<?php echo $text["3"]; ?>
<input class="form-control" style="width:300px;" onchange="this.form.submit()" name="website_description" style="width:300px;" value="<?php show('website_description'); ?>">
<?php echo $text['website_title']; ?><br>
<input class="form-control" style="width:300px;" onchange="this.form.submit()" name="website_title" class="website_title" style="width:300px;" value="<?php show('website_title'); ?>">
<?php echo $text['pref_l']; ?>
<select class="form-control" style="width:200px;" onchange="this.form.submit()" name="language">
	<?php
	$avaiable_languages=import_conf("../etc/avaiable_languages.conf");
	if ($lfilehandle = opendir('../language/')) {
		 while (false !== ($lfile = readdir($lfilehandle))) {
			 if ($lfile != "." && $lfile != ".." && !strstr($lfile,"~")) {
				 	$tmp=explode(".",$lfile);
				 	if($avaiable_languages[$tmp[0]]!="") {
						echo '<option value="'.$tmp[0].'"';if(pref("language")==$tmp[0])echo " selected";echo '>'.$avaiable_languages[$tmp[0]].'</option>';
					}
			 }
		 }
		 closedir($lfilehandle);
	}
	?>
</select>
<?php echo $text['website_template']; ?>
<select class="form-control" style="width:200px;" onchange="this.form.submit()" name="website_template">
	<?php
	$templates = scandir('../templates');
	foreach ($templates as $template) {
		$selected="";
		if($template != ".." && $template != "." && $template != "random" && $template != "main" && $template != "/") {
			if(pref('website_template')==$template) {
				$selected="selected";
			}
			$template_info=file_get_contents("../templates/".$template."/template.json");
			$template_info=json_decode($template_info,true);
			$template_name=(empty($template_info["name"]))?ucfirst($template):$template_info["name"];
			echo "<option value='".$template."' $selected>".$template_name."</option>";
		}
	}
	?>
</select>
<?php echo $text['admin_template']; ?>
<select class="form-control" style="width:200px;" onchange="this.form.submit()" name="admin_template">
	<?php
	$templates = scandir('../templates');
	foreach ($templates as $template) {
		$selected="";
		if($template != ".." && $template != "." && $template != "random" && $template != "main" && $template != "/") {
			if(pref('admin_template')==$template) {
				$selected="selected";
			}
			$template_info=file_get_contents("../templates/".$template."/template.json");
			$template_info=json_decode($template_info,true);
			$template_name=(empty($template_info["name"]))?ucfirst($template):$template_info["name"];
			echo "<option value='".$template."' $selected>".$template_name."</option>";
		}
	}
	?>
</select>
<?php if(pref("pages_enabled")=="yes") {$checked="checked";} echo ucfirst($text['modul']); echo '<br>
<select class="form-control" style="width:200px;" onchange="this.form.submit()" name="pages_enabled">
	<option value="yes"';if(pref("pages_enabled")=="yes")echo " selected";echo '>'.$text['blogging'].'</option>
	<option value="no"';if(pref("pages_enabled")!="yes")echo " selected";echo '>'.$text['website'].'</option>
</select>
'.$text["homepage"].'
<select class="form-control" style="width:200px;" onchange="this.form.submit()" name="homepage">
	<option value="homefeed"';if(pref("homepage")=="homefeed")echo " selected";echo '>'.$text['homefeed'].' </option>
	<option value="article"';if(pref("homepage")=="article")echo " selected";echo '>'.$text['article_home'].' </option>
	<option value="empty"';if(pref("homepage")=="empty")echo " selected";echo '>'.$text['empty'].' </option>
</select>
'; echo $text["contact_person"]; ?>
<select class="form-control" style="width:200px;" onchange="this.form.submit()" name="contact_person">
	<?php
		$result = $db->query("select * from ".$db_prefix."user");
		if($result) {
			while($row = $result->fetch_object()) {
				echo "<option value='".$row->id."'";
				if($row->id==pref("contact_person"))echo " selected";
				echo ">".$row->name."</option>";
			}
		}
	?>
</select>
</form>
</td><td width=33% style="vertical-align:top;">
<!--<form method="post">-->
<?php
foreach(array(array("article_suggestions","aside"),"search","comments","profilepics",array("background","background_yn"),array("headpicture","headpicture_yn"),array("welcomeimg","welcome_img_yn"),"maintenance") as $pref) {
	if(strlen($pref[0])==1) {$tmp=$pref;$pref=false;$pref[0]=$tmp;$pref[1]=$tmp;}
	echo ucfirst(text($pref[0]));
	echo "<br>
	<div class='btn-group' role='group'>
		<div class='btn-group'>
			<button class='";if(pref($pref[1])=="yes")echo "active ";echo "btn btn-default' onmouseup=\"";if($pref[0]=="background")echo"$('.upload_background').show({effect:'blind'});";echo"";if($pref[0]=="headpicture")echo"$('.upload_headpicture').show({effect:'blind'});";echo"";if($pref[0]=="welcomeimg")echo"$('.upload_welcome_img').show({effect:'blind'});";echo"$(this).parent().parent().find('.active').removeClass('active');$(this).addClass('active');\$.ajax({data:'".$pref[1]."=yes',method:'post',url:'?action=9'});\">ON</button>
		</div>
		<div class='btn-group'>
			<button class='";if(pref($pref[1])!="yes")echo "active ";echo "btn btn-default' onmouseup=\"";if($pref[0]=="background")echo"$('.upload_background').hide({effect:'blind'});";echo"";if($pref[0]=="headpicture")echo"$('.upload_headpicture').hide({effect:'blind'});";echo"";if($pref[0]=="welcomeimg")echo"$('.upload_welcome_img').hide({effect:'blind'});";echo"$(this).parent().parent().find('.active').removeClass('active');$(this).addClass('active');\$.ajax({data:'".$pref[1]."=no',method:'post',url:'?action=9'});\">OFF</button>
		</div>";
		echo"
	</div>";
	if($pref[0]=="maintenance") {
		echo "&nbsp;<button class='btn btn-default' onmouseup=\"bootbox.dialog({title:'".text("edit_maintenance_msg")."',message:'<input value=\'".str_replace("'","",str_replace('"',"",pref("maintenance_header")))."\' placeholder=".text("header")." class=form-control id=header><textarea placeholder=".text("message")." class=form-control style=white-space:pre-wrap;margin-top:10px id=body>".str_replace("'","",str_replace('"',"",pref("maintenance_body")))."</textarea>',buttons:{ok:{label:'OK',callback:function(){var header=$('#header').val();var body=$('#body').val();\$.get('ajax_hook.php?action=update_maintenance_info&header='+header+'&body='+body,function(data){if(data=='1'){bootbox.alert('".text("success")."');}else{bootbox.alert('".text("error")."');}});}}}});\"><span class='glyphicon glyphicon-cog'></span></button>";
	}
	echo "<br>";
}
?>
<!--</form>-->
</td><td width=33% style="vertical-align:top;">
<?php echo"<div class=upload_welcome_img";if(pref("welcome_img_yn")!="yes") {echo " style=display:none";} echo ">".$text['welcomeimg'].$text["upload"]; ?> <br>
<form action="upload.php?img=welcome_img" method=post enctype="multipart/form-data">
<input class="form-control" style="width:300px;" onchange="this.form.submit()" name="welcome_img" type=file>
</form></div>
<?php echo"<div class=upload_headpicture";if(pref("headpicture_yn")!="yes") {echo " style=display:none";}echo ">".$text['headpicture'].$text["upload"]; ?> <br>
<form action="upload.php?img=headpicture" method="post" enctype="multipart/form-data">
<input class="form-control" style="width:300px;" onchange="this.form.submit()" name="headpicture" type=file>
</form></div>
<?php echo"<div class=upload_background";if(pref("background_yn")!="yes") {echo " style=display:none";}echo ">".$text['background'].$text["upload"];?><br>
<form action="upload.php?img=background" method=post enctype="multipart/form-data">
<input class="form-control" style="width:300px;" onchange="this.form.submit()" name="background" type=file>
</form></div>
<?php echo $text['shortcut']; ?> <br>
<form action="upload.php?img=shortcut" method=post enctype="multipart/form-data">
<input class="form-control" style="width:300px;" onchange="this.form.submit()" name="shortcut" type=file>
</form>
</td></tr></table>
<?php
}
if(@$_GET['dropdown']==2 && is_sudo() && conf("comisbrowser")) {
	require("comisbrowser.php");
}
elseif(@$_GET['dropdown']==5 && is_sudo()) {


/*
	echo "<div class='alert alert-info'>Sie sind ein Entwickler? Wissen Sie schon, wie einfach es ist Addons / Themes zu erstellen? Registrieren Sie sich auf www.domiscms.de und besuchen Sie das Entwicklerzentrum unter www.domiscms.de/developers/ um eigene Themes und Addons zu programmieren.</div>";

	if(isset($_GET["remove_template"])) {
		$template_info=file_get_contents("../templates/".$_GET["remove_template"]."/template.json");
		$template_info=json_decode($template_info,true);
		$template_dir="../templates/".$_GET["remove_template"]."/";
		foreach($template_info["files"] as $file)unlink($template_dir.$file);
		foreach($template_info["dirs"] as $dir)rmdir($template_dir.$dir);
		unlink($template_dir."template.json");rmdir($template_dir);
		
		refresh("?action=9&dropdown=5&success");
		return;
	}
	elseif(isset($_GET["install_template"])) {
		$template_info=file_get_contents("http://www.domiscms.de/templates/".$_GET["install_template"]."/template.json");
		$template_info=json_decode($template_info,true);
		$template_dir="../templates/".$_GET["install_template"]."/";
		$remote_dir="http://www.domiscms.de/templates/".$_GET["install_template"]."/";
		
		mkdir("../templates/".$_GET["install_template"]);
		
		file_put_contents("../templates/".$_GET["install_template"]."/template.json",file_get_contents("http://www.domiscms.de/templates/".$_GET["install_template"]."/template.json"));
		
		foreach($template_info["dirs"] as $dir)mkdir($template_dir.$dir);
		foreach($template_info["files"] as $file) {
			if(substr($file, (strlen($file)-4), 4)==".php") {
				file_put_contents($template_dir.$file,file_get_contents($remote_dir.$file."txt"));
			} else {
				file_put_contents($template_dir.$file,file_get_contents($remote_dir.$file));
			}
		}
//		str_replace($search, $replace, $subject, &$count = null);
		refresh("?action=9&dropdown=5&success");
		return;
	}








	if(isset($_GET["set"]) && file_exists("../templates/".$_GET["set"]."/style.css")) {
		$db->query("update ".$db_prefix."preferences set value = '".$_GET['set']."' where name = 'website_template'");
		$db->query("update ".$db_prefix."preferences set value = '".$_GET['set']."' where name = 'admin_template'");
		refresh("?action=9&dropdown=5&success");return;
	}
	$_SESSION["website_template_url"]=false;
	$_SESSION["website_template_rawname"]=false;
	$templates=@file("http://www.domiscms.de/templates/templates.list");
	if(empty($templates)) {
		echo "<h1 style='margin-top:100px;font-weight:normal;text-align:center'>".text("check_inet_connection")."</h1><br><br><img src=../templates/main/images/connection.gif style=opacity:.75;width:300px;position:relative;left:50%;margin-left:-150px>";
	}
	else {
		if(isset($_GET["success"])) echo "<div class='alert alert-success'>".$text["success"]."</div>";
		foreach($templates as $template) {
			$template=str_replace("\n",null,str_replace("\r",null,$template));
			$template_info=file_get_contents("http://www.domiscms.de/templates/".$template."/template.json");
			$template_info=json_decode($template_info,true);
			echo "<div class='template-preview'>
				<div class='template-preview-screeny' style='background-image:url(\"http://www.domiscms.de/templates/".$template."/".$template_info["screenshot"]."\");'>";
				if(file_exists("../templates/".$template."/template.json"))
					echo "<div class='template-preview-btns'>
						<a href='?action=9&dropdown=5&set=".$template."' class='btn-comis'>".ucfirst($text["take"])."</a>&emsp;
						<a href='?action=9&dropdown=5&remove_template=".$template."' class='btn-comis'>".ucfirst($text["remove"])."</a>
					</div>";
				else
					echo "<div class='template-preview-btns'>
						<a href='?action=9&dropdown=5&install_template=".$template."' class='btn-comis'>".ucfirst($text["install"])."</a>
					</div>";
				echo "</div>
				<div class='template-preview-name'>"."&nbsp;".$template_info["name"];if(file_exists("../templates/".$template."/template.json"))echo "<img src='../templates/main/images/installed.gif'>";echo "</div>
			</div>";
		}
	}




*/
	if(isset($_GET["set"]) && is_file("../templates/".str_replace("/",null,$_GET["set"])."/about.php")) {
		$db->query("update ".$db_prefix."preferences set value = '".$_GET['set']."' where name = 'website_template'");
		refresh("?action=9&dropdown=5&success");return;
	}

	foreach(glob("../templates/*/about.php") as $template_file) {
		include($template_file);

		$template_folder=str_replace("../templates/",null,str_replace("/about.php",null,$template_file));

		if(is_file(str_replace("about.php","screenshot.jpeg",$template_file)))$screenshot=str_replace("about.php","screenshot.jpeg",$template_file);
		else $screenshot=str_replace("about.php","screenshot.png",$template_file);

		echo "<div class='template-preview'>
			<div class='template-preview-screeny' style='background-image:url(\"".$screenshot."\");'>";
				echo "<div class='template-preview-btns'>
					<a href='?action=9&dropdown=5&set=".$template_folder."' class='btn-comis'>".ucfirst($text["take"])."</a>&emsp;
				</div>";
			echo "</div>
			<div class='template-preview-name'>"."&nbsp;".$template_info["name"];echo "</div>
		</div>";

		unset($template_info);
	}
}

elseif(@$_GET['dropdown']==6 && is_sudo()) {
	if(@$_GET["a"]=="save") {
		$links=@json_decode(file_get_contents("../var/aside_links.json"), true);
		$links[]=array($_POST["line1"],$_POST["line2"]);
		file_put_contents("../var/aside_links.json",json_encode($links));
		echo "<div class='alert alert-success'>".$text["success"]."</div>";
	}
	if(@$_GET["a"]=="delete") {
		$links=@json_decode(file_get_contents("../var/aside_links.json"),true);
		unset($links[($_GET["id"]-1)]);
		file_put_contents("../var/aside_links.json",json_encode($links));
	}
	if(@$_GET["a"]=="edit" || @$_GET["a"]=="add") {
		echo "<h2>".$text["link"]."</h2><form style='width:300px' action='?action=9&a=save&dropdown=6&id=".@$_GET["id"]."' method='post'>
		URL<input class=form-control name='line1' placeholder=URL type=url value='' required>".$text["description"]."
		<input class=form-control name='line2' placeholder='".$text["description"]."' value='' required>
		<input class='btn btn-default form-block' type=submit value='".$text["save"]."'>
		</form>";
	}
	else {
		echo "<table width=100% class='table'><tr>
		<th style=border:0;width:3%;>ID</th>
		<th class=important style=border:0; width:23%>URL</th>
		<th style=border:0; width:20%>".$text['text']."</th>
		<th style=border:0; width=200px>".$text['actions']."</th>";$i=1;
		if(is_array(@json_decode(@file_get_contents("../var/aside_links.json"), true)))foreach(@json_decode(@file_get_contents("../var/aside_links.json"), true) as $link) {
			$link=str_replace("\n",null,str_replace("\r",null,$link));
			if($link!="") {
				echo "</tr></td>
				<td>".$i."</td>
				<td class=important>".$link[0]."</td>
				<td>".$link[1]."</td>
				<td> 
				<a href='#' onmouseup=\"if(confirm('".$text["sure"]."')){window.location.href='?action=".@$action."&a=delete&dropdown=6&id=".$i."';$(this).preventDefault();}\"><img src=../templates/main/images/delete.png alt='' style=height:27px;margin:-7px;margin-left:10px;></a>
				</td></tr>";$i++;
			}
		}
		echo "</table>";
		echo "<a href='?action=9&dropdown=6&a=add' class='btn btn-default'>".$text["add_link"]."</a>";
	}
}
if(isset($_GET['active_field'])){
	echo "<script>$(function() {\$('input').not('.".$_GET['active_field']."').css('opacity','.3');\$('select').not('.".$_GET['active_field']."').css('opacity','.3');});</script>";
//	\$('body').append('<div class=blackscreen style=z-index:50></div>');
}
?>
