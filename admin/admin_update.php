<?php
smkdir("../files/private");
$update_zip=@file_get_contents("http://domiscms.de/files/downloads/comis_".$_SESSION["domiscms_latest_version"].".zip");
if(@$update_zip==""){alert($text["no_updates"]);return;}
if(file_put_contents("../files/private/update.zip",$update_zip)) {
	smkdir("../files/backup/");
	copy("../.htaccess","../backup/_htaccess-".time());
	rename("../etc/db.conf","../etc/db.conf.tmp");
	rename("../etc/inst.conf","../etc/inst.conf.tmp");
	rename("../etc/main.conf","../etc/main.conf.tmp");
	if(file_exists("../etc/private.conf"))rename("../etc/private.conf","../etc/private.conf.tmp");
	if(strstr(substr(@file_get_contents("../includes/mainpage.php"),0,150),"$dont_change_on_auto_update=true;"))
		rename("../includes/mainpage.php","../includes/mainpage.php.tmp");
	$zip = new zipArchive();
	$result = $zip->open("../files/private/update.zip");
	if ($result === TRUE) {
		 if(file_exists("../installation/update.php"))unlink("../installation/update.php");
		 $zip ->extractTo("../");
		 $zip ->close();
		 rename("../installation/index.php","../installation/reinstall.php");
		 unlink("../etc/db.conf");
		 unlink("../etc/inst.conf");
		 if(file_exists("../etc/private.conf"))unlink("../etc/private.conf");
			if(strstr(substr(@file_get_contents("../includes/mainpage.php.tmp"),0,150),"$dont_change_on_auto_update=true"))
				unlink("../includes/mainpage.php");
		 rename("../etc/db.conf.tmp","../etc/db.conf");
		 rename("../etc/inst.conf.tmp","../etc/inst.conf");
		 rename("../etc/main.conf.tmp","../etc/main.conf");
		 if(file_exists("../etc/private.conf.tmp"))rename("../etc/private.conf.tmp","../etc/private.conf");
			if(strstr(substr(@file_get_contents("../includes/mainpage.php.tmp"),0,150),"$dont_change_on_auto_update=true"))
				rename("../includes/mainpage.php.tmp","../includes/mainpage.php");
	 	 if(!is_file("../installation/update.php"))
			 echo "<meta http-equiv=refresh content='0,../admin'>";
		 else
			 echo "<meta http-equiv=refresh content='0,../installation/update.php'>";
		 unlink("../files/private/update.zip");
		 rmdir("../files/private");
		 $about=import_conf("../etc/about.conf");
		addlog("aclogfile","Msg","ADMIN: Comis wurde von ".ucfirst(userinfo("username"))." mit Version ".$v." auf die aktuelle Version ".$about["version"]." geupdated.");
	} else alert("ERROR BY UPDATING!");
}