<?php
if(!is_part_admin())return;
if(pref("pages_enabled")=="no" && $l=="de") {
	$text["articles"]="Seiten";
	$text["article"]="Seite";
	$text["new_article"]="Neue Seite";
}
$dont_show_menu = array(10);
$menu = array($text['current'],
	$text['pages'],
	$text['articles'],
	$text['user'],
	$text['groups'],
	$text['comments'],
	$text['newsletter'],
	"",
	$text['addons'],
	$text['preferences'],
	"",
	$text['backup']);
$c_hook->call("admin_leftnav_setup");
if(is_sudo()) {
	for($x=0;$x < count($menu);$x++) {
		if($menu[$x]!="") {
			$marginright=(in_array($x,explode(",","1,2,3,4,6")))?"margin-right:45px":null;
			if(@$action==$x || (@$_GET['parent_action']==$x && @$_GET["parent_action"]!="")) {
				$createactive=(isset($_GET["action"])&&$_GET["action"]==$x&&isset($_GET["dropdown"])&&$_GET["dropdown"]=="0")?"active":null;
				echo "<a href=?action=".$x."><li onmouseup=\"window.location.href='?action=".$x."'\" class=active style='background-position:0px -".($x*44)."px;$marginright'>".ucfirst($menu[$x])."</a></li>";if(in_array($x,explode(",","1,2,3,4,6")))echo "<a href='?action=".$x."&dropdown=0' onmouseup='return false'><div style='margin-top:-45px;margin-right:0px' class='create-empty $createactive'></div></a>";echo "";
					if(in_array(@$action,range(1,4))) {
						echo "<a href=?action=".$x."&create_empty><li onmouseup=\"window.location.href='?action=".$x."&create_empty'\" class=dropdown>".$text['add_empty']."</li></a>";			
						if(@$action==2) {
							echo "<a href=?action=".$x."&dropdown=1><li onmouseup=\"window.location.href='?action=".$x."&dropdown=1'\" class=dropdown>".$text['my_pictures']."</li></a>";
						}
					}
					if(@$action==9 || @$_GET['parent_action']==9) {
						echo "<a href=?action=".$x."&dropdown=0><li onmouseup=\"window.location.href='?action=".$x."&dropdown=0'\" class=dropdown>".$text['website']."</li></a>";
						echo "<a href=?action=".$x."&dropdown=6><li onmouseup=\"window.location.href='?action=".$x."&dropdown=6'\" class=dropdown>".$text['links']."</li></a>";
						if(conf("developer"))echo "<a href=?action=".$x."&dropdown=4><li onmouseup=\"window.location.href='?action=".$x."&dropdown=4'\" class=dropdown>".$text['configuration']."</li></a>";			
						if(conf("comisbrowser"))echo "<a href=?action=".$x."&dropdown=2><li onmouseup=\"window.location.href='?action=".$x."&dropdown=2'\" class=dropdown>".$text['comisbrowser']."</li></a>";			
						echo "<a href=?action=".$x."&dropdown=5><li onmouseup=\"window.location.href='?action=".$x."&dropdown=5'\" class=dropdown>".$text['templatemanager']."</li></a>";				
						if(file_exists("../INSTALL")) {	
							echo "<a href=../installation/index.php style=color:#a00;><li onmouseup=\"window.location.href='../installation/index.php'\" class=dropdown>".$text['reinst']."</li></a>";			
						}
					}
					if(@$action==11) {
						echo "<a href=?action=11&show=all><li onmouseup=\"window.location.href='?action=11&show=all'\" class=dropdown>".$text["backup_all"]."</li></a>
						<a href=?action=11&show=create_backup><li onmouseup=\"window.location.href=''?action=11&show=create_backup\" class=dropdown>".$text["create_backup"]."</li></a>
						<a href=?action=11&show=import><li onmouseup=\"window.location.href='?action=11&show=import'\" class=dropdown>".ucfirst($text["import"])."</li></a>";
					}
			}
			elseif(!in_array($x,$dont_show_menu)) {
				echo "<a href=?action=".$x."><li onmouseup=\"window.location.href='?action=".$x."'\" style='background-position:0px -".($x*44)."px;$marginright'>";
				echo ucfirst($menu[$x])."</a></li>";
				if(in_array($x,explode(",","1,2,3,4,6")))echo "<a href='?action=".$x."&dropdown=0' onmouseup='return false'><div style='margin-top:-45px;margin-right:0px' class='create-empty'></div></a>";
			}
		}
		if($x==7)echo '<li style="background:none !important;height:0px">';
	}
}
else {
	$x=$action;
	if($access["pages"]=="yes") {
		if(@$action==1) {
			echo "<a href=?action=1><li onmouseup=\"window.location.href='?action=1'\" class=active style='background-position:0px -".(1*44)."px;'>".ucfirst($menu[1])."<a href='?action=".$x."&dropdown=0'><div class=create-empty></div></a></li></a>";
		}
		else {
			echo "<a href=?action=1><li onmouseup=\"window.location.href='?action=1'\" style='background-position:0px -".(1*44)."px;'>".ucfirst($menu[1])."<a href='?action=".$x."&dropdown=0'><div class=create-empty></div></a></li></a>";
		}
	}
	if($access["articles"]=="yes") {	
		if(@$action==2) {
			echo "<a href=?action=2><li onmouseup=\"window.location.href='?action=2'\" class=active style='background-position:0px -".(2*44)."px;'>".ucfirst($menu[2])."<a href='?action=".$x."&dropdown=0'><div class=create-empty></div></a></li></a>";
		}
		else {
			echo "<a href=?action=2><li onmouseup=\"window.location.href='?action=2'\" style='background-position:0px -".(2*44)."px;'>".ucfirst($menu[2])."<a href='?action=".$x."&dropdown=0'><div class=create-empty></div></a></li></a>";
		}
	}
	if($access["user"]=="yes") {	
		if(@$action==3) {
			echo "<a href=?action=3><li onmouseup=\"window.location.href='?action=3'\" class=active style='background-position:0px -".(3*44)."px;'>".ucfirst($menu[3])."<a href='?action=".$x."&dropdown=0'><div class=create-empty></div></a></li></a>";
		}
		else
			echo "<a href=?action=3><li onmouseup=\"window.location.href='?action=3'\" style='background-position:0px -".(3*44)."px;'>".ucfirst($menu[3])."<a href='?action=".$x."&dropdown=0'><div class=create-empty></div></a></li></a>";
	}
	if($access["groups"]=="yes") {	
		if(@$action==4) {
			echo "<a href=?action=4><li onmouseup=\"window.location.href='?action=4'\" class=active style='background-position:0px -".(4*44)."px;'>".ucfirst($menu[4])."<a href='?action=".$x."&dropdown=0'><div class=create-empty></div></a></li></a>";
		}
		else
			echo "<a href=?action=4><li onmouseup=\"window.location.href='?action=4'\" style='background-position:0px -".(4*44)."px;'>".ucfirst($menu[4])."<a href='?action=".$x."&dropdown=0'><div class=create-empty></div></a></li></a>";
	}
	if($access["comments"]=="yes") {	
		if(@$action==5)
			echo "<a href=?action=5><li onmouseup=\"window.location.href='?action=5'\" class=active style='background-position:0px -".(5*44)."px;'>".ucfirst($menu[5])."</li></a>";
		else
			echo "<a href=?action=5><li onmouseup=\"window.location.href='?action=5'\" style='background-position:0px -".(5*44)."px;'>".ucfirst($menu[5])."</li></a>";
	}
	if($access["newsletter"]=="yes") {	
		if(@$action==6)
			echo "<a href=?action=6><li onmouseup=\"window.location.href='?action=6'\" class=active style='background-position:0px -".(6*44)."px;'>".ucfirst($menu[6])."</li></a>";
		else
			echo "<a href=?action=6><li onmouseup=\"window.location.href='?action=6'\" style='background-position:0px -".(6*44)."px;'>".ucfirst($menu[6])."</li></a>";
	}
	echo "<br>";
	if($access["prefs"]=="yes") {
		if(@$action==9) {
			echo "<a href=?action=9><li onmouseup=\"window.location.href='?action=9'\" class=active style='background-position:0px -".(9*44)."px;'>".ucfirst($menu[9])."</li></a>";
		}
		else {
			echo "<a href=?action=9><li onmouseup=\"window.location.href='?action=9'\" style='background-position:0px -".(9*44)."px;'>".ucfirst($menu[9])."</li></a>";
		}
	}
}
if($l=="de" && conf("disp_help_link")) {echo "<a href='http://www.domiscms.de/support'><li onmouseup=\"window.location.href='http://www.domiscms.de/support'\" style='background-position:0px -528px;' ";if(@$action==103)echo " class=active";echo ">".$text["support"]."</li></a>";}
$c_hook->call("admin_leftnav_append");
?>

<li onmouseup="window.location.href='../'" style="position:fixed;left:0;bottom:0;width:230px !Important;background-position:0px -572px;" class="website-preview">
	<a href="../" target="<?php echo $preview_target; ?>">
	<?php echo ucfirst($text['website_preview']); ?>
	</a>
</li><br>
<?php
echo "<div class='headernav'><a href='../?do=logout'><li onmouseup=\"window.location.href='../?do=logout'\">".ucfirst(text('logout'));?></li></a>
<?php
if(is_sudo()) {
?>
<a href=?action=100><li onmouseup=\"window.location.href='?action=100'\" <?php if(@$action==100) echo "class=active";?>><?php echo ucfirst(text("information"));?></li></a>
<?php
}
?>
<a href=?action=10><li onmouseup=\"window.location.href='?action=10'\" <?php if(@$action==10) echo "class=active";?>><?php echo ucfirst(text("statistics"));?></li></a>
</div>
