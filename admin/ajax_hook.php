<?php
require("../db.php");
$out="Start";
$na="<div style=margin:10px;>".text("stat_na")."</div>";
if(isset($_GET["action"])) {
	if($_GET["action"]=="import_article") {
		$json=base64_decode($_GET["json"]);
		$article=json_decode($json,1);
		$query="insert into ".$db_prefix."articles (`id`, `orderid`, `pageid`, `title`, `alias`, `editor`, `timestamp`, `public`, `publish_ts`, `page`, `footer`, `comments`, `views`) VALUES (NULL, '".$article["orderid"]."', '".$article["pageid"]."', '".$article["title"]."', '".make_alias($article["title"])."', '".$article["editor"]."', '".$article["timestamp"]."', '".$article["public"]."', '".$article["publish_ts"]."', '".$article["page"]."', '".$article["footer"]."', '".$article["comments"]."', '".$article["views"]."')";
		if($db->query($query))echo "../?article=".$db->insert_id;else echo"0";
		return;
	}
	if($_GET["action"]=="update_maintenance_info") {
		$query="update ".$db_prefix."preferences set value='".$_GET["header"]."' where name='maintenance_header'";
		$query2="update ".$db_prefix."preferences set value='".$_GET["body"]."' where name='maintenance_body'";
		if($db->query($query)&&$db->query($query2))echo"1";
		return;
	}
	if($_GET["action"]=="removefavorite") {
		if(is_file("../var/favorites.json")&&!empty(file_get_contents("../var/favorites.json")))$favorites=json_decode(file_get_contents("../var/favorites.json"),1);else $favorites["insert_id"]=0;
		unset($favorites["favorites"][userid()][$_GET["id"]]);
		file_put_contents("../var/favorites.json",json_encode($favorites));
		return;
	}
	if($_GET["action"]=="addfavorite") {
		if(is_file("../var/favorites.json")&&!empty(file_get_contents("../var/favorites.json")))$favorites=json_decode(file_get_contents("../var/favorites.json"),1);else $favorites["insert_id"]=0;
		$favorites["insert_id"]++;
		$favorites["favorites"][userid()][$favorites["insert_id"]]["url"]=$_GET["url"];
		$favorites["favorites"][userid()][$favorites["insert_id"]]["title"]=$_GET["title"];
		file_put_contents("../var/favorites.json",json_encode($favorites));
		echo $favorites["insert_id"];
		return;
	}
	$out.=">action";
	if($_GET["action"]=="remove_attachment") {
		$out.=">remove";
		if(!empty($_GET["id"]) && !empty($_GET["article_id"])) {
			$out.=">numeric";
			$query2="select attachments from ".$db_prefix."articles where id = '".@$_GET['article_id']."'";
			$result = $db->query($query2);
			while($row = $result->fetch_assoc()) {
				$out.=">while";
				$attachments=json_decode($row["attachments"],true);
				unset($attachments["attachments"][$_GET["id"]]);
				$out.="<pre>";
				$query="update ".$db_prefix."articles set attachments='".json_encode($attachments)."' where id = '".@$_GET['article_id']."'";
				$out.=">".$query;
				if($db->query($query)) {
					$out.=">query";
					die("1");
				} else die("0");
			}
		}
	}
	elseif($_GET["action"]=="edit_attachment") {
		$out.=">edit";
		if(is_numeric($_GET["id"]) && is_numeric($_GET["article_id"])) {
			$result = $db->query("select attachments from ".$db_prefix."articles where id = '".@$_GET['article_id']."'");
			while($row = $result->fetch_assoc()) {
				$attachments=json_decode($row["attachments"],true);
				$attachments["attachments"][$_GET["id"]]["title"]=$_GET["new_title"];

				if($db->query("update ".$db_prefix."articles set attachments='".json_encode($attachments)."' where id = '".@$_GET['article_id']."'")) {
					die("1");
				} else die("0");
			}
		}
	}
	elseif($_GET["action"]=="add_attachment") {
		$out.=">edit";
		if(is_numeric($_GET["id"]) && is_numeric($_GET["article_id"])) {
			$result = $db->query("select attachments from ".$db_prefix."articles where id = '".@$_GET['article_id']."'");
			while($row = $result->fetch_assoc()) {
				$attachments=json_decode($row["attachments"],true);
				$attachments["insert_id"]=$attachments["insert_id"]+1;
				$attachments["attachments"][$attachments["insert_id"]]["title"]=$_GET["title"];
				$attachments["attachments"][$attachments["insert_id"]]["url"]=$_GET["url"];
				if($db->query("update ".$db_prefix."articles set attachments='".json_encode($attachments)."' where id = '".@$_GET['article_id']."'")) {
					die("1");
				} else die("0");
			}
		}
	}
	elseif($_GET["action"]=="show_statistics") {
		if($_GET["statload"]=="logins") {
			$result=$db->query("select * from ".$db_prefix."statistics where action='login' and value='success' order by timestamp desc");
			if(is_object($result)) {
				if($result->num_rows) {
					echo "<table class='table table-soft table-hover' style=background:white;><tr>
						<th>".text("username")."</th>
						<th>".text("date")."</th>
						<th>".text("ipaddr")."</th>
					</tr>";
					while($row=$result->fetch_object()) {
						echo "<tr>
							<td>".ucfirst(userinfo("username",$row->userid))."</td>
							<td>".date("d.m.Y H:i:s",$row->timestamp)."</td>
							<td>".$row->ip."</td>
						</tr>";
					}
					echo "</table>";
				} else {
					echo $na;
				}
			}
		} elseif($_GET["statload"]=="logouts") {
			$result=$db->query("select * from ".$db_prefix."statistics where action='logout' and value='success' order by timestamp desc");
			if(is_object($result)) {
				if($result->num_rows) {
					echo "<table class='table table-soft table-hover' style=background:white;><tr>
						<th>".text("username")."</th>
						<th>".text("date")."</th>
						<th>".text("ipaddr")."</th>
					</tr>";
					while($row=$result->fetch_object()) {
						echo "<tr>
							<td>".ucfirst(userinfo("username",$row->userid))."</td>
							<td>".date("d.m.Y H:i:s",$row->timestamp)."</td>
							<td>".$row->ip."</td>
						</tr>";
					}
					echo "</table>";
				} else {
					echo $na;
				}
			}
		} elseif($_GET["statload"]=="msgs") {
			$result=$db->query("select * from ".$db_prefix."statistics where action like 'msg' order by timestamp desc");
			if(is_object($result)) {
				if($result->num_rows) {
					echo "<table class='table table-soft table-hover' style=background:white;><tr>
						<th>".text("message")."</th>
						<th>".text("date")."</th>
						<th>".text("username")."</th>
					</tr>";
					while($row=$result->fetch_object()) {
						echo "<tr>
							<td>".$row->value."</td>
							<td>".date("d.m.Y H:i:s",$row->timestamp)."</td>
							<td>".ucfirst(userinfo("username",$row->userid))."</td>
						</tr>";
					}
					echo "</table>";
				} else {
					echo $na;
				}
			}
		} elseif($_GET["statload"]=="searches") {
			$result=$db->query("select * from ".$db_prefix."statistics where action='search' order by timestamp desc");
			if(is_object($result)) {
				if($result->num_rows) {
					echo "<table class='table table-soft table-hover' style=background:white;><tr>
						<th>".text("searchquery")."</th>
						<th>".text("date")."</th>
						<th>".text("username")."</th>
					</tr>";
					while($row=$result->fetch_object()) {
						echo "<tr>
							<td>
								<a target=_blank style=margin-left:-5px;position:absolute;margin-top:-5px class='btn btn-default btn-sm' href='../?q=". str_replace(" ","+",$row->value) ."'><span class='glyphicon glyphicon-search'></span></a>
								<div style=margin-left:35px>".$row->value."</div>
							</td>
							<td>".date("d.m.Y H:i:s",$row->timestamp)."</td>
							<td>";if($row->userid)echo ucfirst(userinfo("username",$row->userid));echo"</td>
						</tr>";
					}
					echo "</table>";
				} else {
					echo $na;
				}
			}
		} elseif($_GET["statload"]=="errors") {
			$result=$db->query("select * from ".$db_prefix."statistics where action='error' order by timestamp desc");
			if(is_object($result)) {
				if($result->num_rows) {
					echo "<table class='table table-soft table-hover' style=background:white;><tr>
						<th>".text("error")."</th>
						<th>".text("date")."</th>
						<th>".text("ipaddr")."</th>
						<th>".text("username")."</th>
					</tr>";
					while($row=$result->fetch_object()) {
						echo "<tr>
							<td>".$row->value."</td>
							<td>".date("d.m.Y H:i:s",$row->timestamp)."</td>
							<td>".$row->ip."</td>
							<td>";if($row->userid)echo ucfirst(userinfo("username",$row->userid));echo"</td>
						</tr>";
					}
					echo "</table>";
				} else {
					echo $na;
				}
			}
		} elseif($_GET["statload"]=="includes") {
			$result=$db->query("select * from ".$db_prefix."statistics where action='inc' order by timestamp desc");
			if(is_object($result)) {
				if($result->num_rows) {
					echo "<table class='table table-soft table-hover' style=background:white;><tr>
						<th>".text("path")."</th>
						<th>".text("username")."</th>
						<th>".text("date")."</th>
						<th>".text("ipaddr")."</th>
					</tr>";
					while($row=$result->fetch_object()) {
						echo "<tr>
							<td>".ucfirst(text("file").": /includes/".$row->value.".php")."</td>
							<td>";if($row->userid)echo ucfirst(userinfo("username",$row->userid));echo"</td>
							<td>".date("d.m.Y H:i:s",$row->timestamp)."</td>
							<td>".$row->ip."</td>
						</tr>";
					}
					echo "</table>";
				} else {
					echo $na;
				}
			}
		} elseif($_GET["statload"]=="pages") {
			$result=$db->query("select * from ".$db_prefix."statistics where action='page' order by timestamp desc");
			if(is_object($result)) {
				if($result->num_rows) {
					echo "<table class='table table-soft table-hover' style=background:white;><tr>
						<th>".text("title")."</th>
						<th>".text("username")."</th>
						<th>".text("date")."</th>
						<th>".text("ipaddr")."</th>
					</tr>";
					while($row=$result->fetch_object()) {
						echo "<tr>
							<td>".$db->query("select title from ".$db_prefix."pages where id='".$row->value."' limit 1")->fetch_object()->title."</td>
							<td>".ucfirst(userinfo("username",$row->userid))."</td>
							<td>".date("d.m.Y H:i:s",$row->timestamp)."</td>
							<td>".$row->ip."</td>
						</tr>";
					}
					echo "</table>";
				} else {
					echo $na;
				}
			}
		} elseif($_GET["statload"]=="articles") {
			echo "<br>
			<div class='btn-group' style='float:left;margin-top:-5px'>
			  	<button type='button' class='btn btn-default dropdown-toggle' data-toggle='dropdown' aria-expanded='false'>
			    	<span class='glyphicon glyphicon-stats'></span> ".text("add_article_view")." <span class='caret'></span>
			  	</button>
				<ul class='dropdown-menu' role='menu'>
					";
					$found=0;
					$result=$db->query("select * from ".$db_prefix."articles");
					if(is_object($result)) {
						if($result->num_rows) {
							while($row=$result->fetch_object()) {
								if($db->query("select id from ".$db_prefix."statistics where action='article' and value='".$row->id."'")->num_rows) {
									$found++;
									echo "<li class='article".$row->id."-stat' onmouseup=\"$(this).delay(100).hide();$('.load-statistics').find('.article-statistics').prepend('<div class=article".$row->id."></div>').find('.article".$row->id."').load('../bin/ajax_hook.php?action=show_statistics&id=".$row->id."&jqueryselectorpre=false');\"><a href=#><span class='glyphicon glyphicon-plus'></span> ".$row->title."</a></li>";
								}
							}
						}
					}
					echo"
				</ul>
			</div>
			<br><br>
			<div class=article-statistics style=margin-bottom:-15px;></div>
			";
			if(!$found)echo$na;
		} elseif($_GET["statload"]=="resetstats") {
			echo "<div class='resetstats-dialog' style='width:100%;padding:50px;text-align:center'><br>".text("sure")."<br>
			<a style='margin:10px auto;text-align:right;' class='btn btn-danger' onmouseup=\"$('.resetstats-dialog').html('".text("success")."!');$.get('ajax_hook.php?action=sudorequest&execute=resetstats');\">".ucfirst(text("reset_stats"))."</a></div>";
		} elseif($_GET["statload"]=="systemlogs") {
			echo(is_file("../log/system.log"))?"<div class=well style=background:white;white-space:pre-wrap>".str_replace("\n \n","\n",str_replace("\n\n","\n",str_replace("\r","\n",file_get_contents("../log/system.log"))))."</div>":$na;
		}
	}
}
if(is_sudo()&&isset($_GET["action"])&&$_GET["action"]=="sudorequest") {
	if($_GET["execute"]=="resetstats") {
		$db->query("truncate `".$db_prefix."statistics`");
	}
}
?>
<style type="text/css">
.table-soft {
	border-radius:4px;
	border:1px solid #e3e3e3;
	box-shadow: inset 0 1px 1px rgba(0, 0, 0, .05);
	border-top:none;
}
</style>