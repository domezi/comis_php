<script type="text/javascript">
	$(function () {
		$(".nav-tab-statistics").find("li").on("mouseup",function (e) {
			if (!$(this).hasClass("donothing")) {
				$(this).parent().find(".active").removeClass("active");
				$(this).addClass("active");
				$("").addClass("active");
				$(".load-statistics").load("ajax_hook.php?action=show_statistics&statload="+$(this).find("a").attr("data-statload"));
			}
		});
		$(".load-statistics").load("ajax_hook.php?action=show_statistics&statload=logins");
	});
</script>
<div class="well">
	<ul class="nav nav-tabs nav-tab-statistics">
		<li><a href="#" data-statload="articles"><?php echo ucfirst(text("articles"))?></a></li>
		<li><a href="#" data-statload="pages"><?php echo ucfirst(text("pages"))?></a></li>
		<li><a href="#" data-statload="searches"><?php echo ucfirst(text("searches"))?></a></li>
		<li><a href="#" data-statload="includes">Includes</a></li>
		<li class="active"><a href="#" data-statload="logins">Logins</a></li>
		<li><a href="#" data-statload="logouts">Logouts</a></li>
		<li><a href="#" data-statload="msgs">Meldungen</a></li>
		<li><a href="#" data-statload="errors">Fehler</a></li>
		<li><a href="#" data-statload="systemlogs">System</a></li>
		<li style="float:right"><a href="#" data-statload="resetstats"><?php echo ucfirst(text("reset_stats"))?></a></li>
	</ul>
	<div class="load-statistics" style="margin-top:-1px;margin-bottom:-10px;"></div>
</div>