<?php
require_once("../db.php");
if(!is_sudo())return;
echo "<div class='blackscreen fullscreen loadingscreen' style=display:none><img style='position:absolute;top:50%;margin-top:-80px;' class='rotatefast' src=../templates/main/images/load.png></div>";
$SERVER=$_SERVER["SERVER_NAME"].$_SERVER["PHP_SELF"];
//		echo "<style>#admincontainer{padding:0 !important}</style><iframe width=100% frameborder=0 style=height:99.6%; src='//domiscms.de/addons.php?v=$v&l=".$l."&feature=incomis&referer=".$SERVER."&bl'><a href=//domiscms.de/addons.php?v=$v&l=".$l."&feature=incomis&referer=".$SERVER."&bl>hier</a></iframe>";

//		$addons["insert_id"]="1";
//		$addons["templates"][]=array("id"=>"1","name"=>"News","trusted"=>"1","author"=>"Dominik","timestamp"=>"","img"=>"","description"=>"Shows recent News from Google","downloads"=>"0","root"=>"news","files"=>array("news.mainpage.php"),"version"=>"1.0","comis_version"=>"2.0.030");
//		
//		die(json_encode($addons));

//die(file_get_contents("http://www.domiscms.de/addons/addons.list"));

$addons=@file("http://www.domiscms.de/addons/addons.list");
foreach($addons as $addon) {
	$addon2[]=str_replace("\r","",str_replace("\n","",$addon));
}

$addons=$addon2;

foreach(glob("../addons/*/addon.json") as $addon) {
	$addon_dirname=str_replace("../addons/","",str_replace("/addon.json","",$addon));
	if(!in_array($addon_dirname,$addons)) {
		$addons[]=$addon_dirname;
	}
}

echo "<div class='alert alert-info'>Sie sind ein Entwickler? Wissen Sie schon, wie einfach es ist Addons / Themes zu erstellen? Registrieren Sie sich auf www.domiscms.de und besuchen Sie das Entwicklerzentrum unter www.domiscms.de/developers/ um eigene Themes und Addons zu programmieren.</div>";

if(!is_array($addons)) {
	echo "<h1 style='margin-top:100px;font-weight:normal;text-align:center'>".text("check_inet_connection")."</h1><br><br><img src=../templates/main/images/connection.gif style=opacity:.75;width:300px;position:relative;left:50%;margin-left:-150px>";
} else {
	if(isset($_GET["success"])) echo "<div class='alert alert-success'>".$text["success"]."</div>";
	$tmpstyle="";
	foreach($addons as $addon) {
		$addon_name=str_replace("\r","",str_replace("\n","",$addon));
		if(!empty(file_get_contents("http://www.domiscms.de/addons/".$addon_name."/addon.json"))) {
			$addon_info=file_get_contents("http://www.domiscms.de/addons/".$addon_name."/addon.json");
			$addon_info=json_decode($addon_info,true);
			$screenshot="http://www.domiscms.de/addons/".$addon_name."/".$addon_info["screenshot"];
		} else {
			$addon_info=file_get_contents("../addons/".$addon_name."/addon.json");
			$addon_info=json_decode($addon_info,true);
			$screenshot="../addons/".$addon_name."/".$addon_info["screenshot"];
		}
		if($addon_info["screenshot"]=="")$addon_info["screenshot"]="../templates/main/images/icon-img.gif";
		echo "<div class='template-preview'>
			<div class='template-preview-screeny' style=\"background-image:url('".$screenshot."');background-position:center center;\">";
			$tmpstyle.=".addon".$addon_info["uniqid"]."-btn-";$tmpstyle.=(!file_exists("../addons/".$addon_name."/addon.json"))?"remove":"install";
			if(!file_exists("../addons/".$addon_name."/addon.json"))$tmpstyle.=",.addon".$addon_info["uniqid"]."-btn-installed";
			$tmpstyle.="{display:none}";
				echo "<div class='template-preview-btns'>
					<a class='btn-comis addon".$addon_info["uniqid"]."-btn-remove' href=# onmouseup=\"$('.loadingscreen').fadeIn(400);$.get('?action=8&remove_addon=".$addon_info["dirname"]."',function(){\$('.loadingscreen').fadeOut(400);$('.addon".$addon_info["uniqid"]."-btn-installed').toggle();$('.addon".$addon_info["uniqid"]."-btn-remove').toggle();$('.addon".$addon_info["uniqid"]."-btn-install').toggle();});\">".ucfirst($text["remove"])."</a>
					<a class='btn-comis addon".$addon_info["uniqid"]."-btn-install' href=# onmouseup=\"$('.loadingscreen').fadeIn(400);$.get('?action=8&install_addon=".$addon_info["dirname"]."',function(){\$('.loadingscreen').fadeOut(400);$('.addon".$addon_info["uniqid"]."-btn-installed').toggle();$('.addon".$addon_info["uniqid"]."-btn-remove').toggle();$('.addon".$addon_info["uniqid"]."-btn-install').toggle();});\">".ucfirst($text["install"])."</a>
				</div>";
			echo "</div>
			<div class='template-preview-name'>".ucfirst($addon_info["name"])."&nbsp;";echo "<img class='addon".$addon_info["uniqid"]."-btn-installed' src='../templates/main/images/installed.gif'>";echo"</div>
		</div>";
	}
	echo "<style>".$tmpstyle."</style>";
}