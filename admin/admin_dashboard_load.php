<?php
require("../db.php");
if(!file_exists("../var/dash_widget_notices.txt")) touch("../var/dash_widget_notices.txt");
if(!file_exists("../var/_dash_widgets.conf")) {
	file_put_contents("../var/_dash_widgets_min.conf",'weather#####statistics#####');
	file_put_contents("../var/_dash_widgets_prefs.conf","#preflist
recent_logins_amnt=3
recent_articles_amnt=4
recent_searches_amnt=3
recent_comments_amnt=4
recent_pages_amnt=4
news_amnt=1
addons_amnt=2
logs_amnt=30
notices_size=3
weather_city=Vancouver
weather_country=CA
auto_update=false
info_mode_complex=false
");
	file_put_contents("../var/_dash_widgets1.conf","1#version
1#info
1#news");
	file_put_contents("../var/dash_messages.txt","Comis Product Video#<iframe width='400' height='225' src='https://www.youtube.com/embed/1F20Tu3fCzE?rel=0&amp;controls=0&amp;showinfo=0' frameborder='0' allowfullscreen></iframe>");
	file_put_contents("../var/_dash_widgets2.conf","2#date
2#command
2#notices");
	file_put_contents("../var/_dash_widgets3.conf","3#comisnews
3#addons
3#recent_comments");
	file_put_contents("../var/_dash_widgets.conf",file_get_contents("../var/_dash_widgets1.conf")."\n".file_get_contents("../var/_dash_widgets2.conf")."\n".file_get_contents("../var/_dash_widgets3.conf")."\n");
}
echo '
<a onmouseup="if(confirm(\''.$text["sure"].'\')){$.get(\'?action=200&parent_action=9&delete=../var/_dash_widgets.conf&return=?action=9(and)dropdown=2(and)path=../etc\');window.location.href=window.location.href;}" style="opacity:.5;display:block;position:absolute;height:30px;width:30px;left:40px;top:10px;background:url(../templates/main/images/reset.png);background-size:21px;background-repeat:no-repeat;background-position:1px 3px;"></a>';
	if(is_sudo()) {
	?>
		<style type="text/css">
		.widget-placeholder {
		padding:13px
		}
		</style>
		<script>
		function widgetsettings(widget,settings) {
				$(".widget#"+widget+" .widget-load").html("<h1 class='widget-headline'>"+$(".widget#"+widget+" .widget-load h1").html()+"</h1>"+"<div class='wsettings-"+widget+"'></div>"+"<a onmouseup=\"$(this).css('cursor','default').css('opacity','.5');$('.widget#"+widget+" .widget-load').load('widget_"+widget+".php?reload');\" class=btn-comis style='background:hsla(0,0%,100%,.25);padding:1px;margin-bottom:10px;text-align:center;display:block;border:1px solid hsla(0,0%,100%,.4);cursor:pointer'>OK</a>");max="";
			if(widget=="addons")max="&max=2";else if(widget=="logs")max="&max=100";
			if (settings==false) $(".wsettings-"+widget).load('wsettings_default.php?widget='+widget+max);
			else $(".wsettings-"+widget).load('wsettings_'+widget+'.php?reload'+max);
		}
		$(function(){
			$( ".widget-column" ).sortable({
				connectWith: ".widget-column",
				handle: "h1",
				cancel: ".widget-toggle",
				placeholder: "widget-placeholder widget",
			   update: function( event, ui ) {
	            var order = $(this).sortable('toArray').toString();
	            id=$(this).attr("id").replace("widget-column-","");
	            get="reorder.php?order="+order+"&col="+id;
	            $.get(get);
			   }
			});
			$(".widget").addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" ).find( "h1" ).addClass( "ui-widget-header ui-corner-all" );
			$(".ui-widget-header").css("font-size","19px").css("font-weight","200").css("margin","10px -11px").css("padding","3px 13px").css("margin-top","0").css("cursor","grab").css("background","none").css("border","none").css("border-radius","0px").css("border-bottom","1px solid hsla(0,0%,100%,.3)");
			$(".ui-widget").css("font-size","19px").css("font-weight","200").css("padding","3px 13px").css("margin-top","0").css("background","hsla(0,0%,100%,.2)").css("border","1px solid hsla(0,0%,100%,.3)");
		});</script><?php $g=0;$curr_column=1;$widget_load_script=null;
		echo "<div id=widgets><div class=widget-column id='widget-column-".$curr_column."'>";
		
		$widgetsmin=explode("#####",file_get_contents("../var/_dash_widgets_min.conf"));
		foreach(file("../var/_dash_widgets.conf") as $widget) {
			$g++;
				$widget=str_replace("\n",null,str_replace("\r",null,$widget));
				$last_column=$curr_column;
				$tmp=explode("#",$widget);
				$curr_column=$tmp[0];
				$widget=$tmp[1];
				if($last_column!=$curr_column) echo "</div><div class=widget-column id='widget-column-".$curr_column."'>";
			if(strlen($widget)>1) {
				if(file_exists("widget_".$widget.".php")) {
					$visible=(in_array($widget,$widgetsmin))?"show":"hide";
					echo "<div class='widget ";if(isset($all_widgets_hidden) || in_array($widget,$widgetsmin))echo" widget-hidden";echo"' id='".$widget."'>
					<div class='widget-icon-toggle widget-icon-toggle-".$visible."' onmouseup=\"if(\$(this).hasClass('widget-icon-toggle-show')){\$('#".$widget." .widget-icon-settings').show();\$('.widget#".$widget."').removeClass('widget-hidden');\$(this).removeClass('widget-icon-toggle-show').addClass('widget-icon-toggle-hide');\$.get('reorder.php?show=".$widget."');}else{\$('#".$widget." .widget-icon-settings').hide();\$('.widget#".$widget."').addClass('widget-hidden');\$(this).removeClass('widget-icon-toggle-hide').addClass('widget-icon-toggle-show');\$.get('reorder.php?hide=".$widget."');}\"></div>";
					if(strstr(file_get_contents("widget_".$widget.".php"),'/*widgetsettings*/')){echo"<div class='widget-icon-settings'";if($visible=="show")echo" style=display:none;";echo" onmouseup=\"widgetsettings('".$widget."',false);\"></div>";}
					if(strstr(file_get_contents("widget_".$widget.".php"),'/*widgetsetownfunction*/')){echo"<div class='widget-icon-settings'";if($visible=="show")echo" style=display:none;";echo" onmouseup=\"widgetsettings('".$widget."','load');\"></div>";}
					echo"<div class='widget-load'>";
					echo "<img src='../templates/main/images/load.png' style='margin-left:160px;margin-top:20px;margin-bottom:20px;width:70px;' class='rotatefast'>";
					$widget_load_script.="$('.widget#".$widget." .widget-load').load('widget_".$widget.".php');";
					echo "</div></div>";
				}
				else {
					echo "<div class='widget'>Widget \"".$widget."\" konnte nicht geladen werden.</div>";
				}
			} else $g--;
		}
		echo "<script>".$widget_load_script."</script>";
		if($g < 1) {
			unlink("../var/_dash_widgets.conf");refresh('?widgets_restored');
		}
		echo "</div>";
	}else{if(date("H") >= 1 && date("H") <= "4" ) {		$msg = $text["nice_2_cu"][7].date(" H ").$text["nice_2_cu"][8];	}	elseif(date("H") > 18) {		$msg = $text["nice_2_cu"][5];	}	elseif(date("H") > 5 && date("H") < 8) {		$msg = $text["nice_2_cu"][6];	}	else {		$msg = $text["nice_2_cu"][rand(0,4)];	}echo"<h1 style=text-align:center;margin-top:100px;font-weight:normal><div style=transform:rotate(90deg);-webkit-transform:rotate(90deg);font-size:200px>:-)</div>".$msg."</h1>";}