<?php
require("../db.php");
include("lib/index_functions.php");
$index=new index();

$clean=new cleaner();	
$clean->logins();
	
if(isset($_POST['register'])) {
	include("includes/register.php");
	return;
}
elseif(isset($_POST['login'])) {
	$user=new user();
	if($user->canlogin($_POST["username"],$_POST["password"])) {
		$login=new login();
		$_SESSION["hash"]=$login->createSession($_POST["username"],$_POST["password"]);
		$index->do_what("login");
		$value="1";
	} else {
		$nologin=true;$value="0";
	}
	addlog(0,"login","success",userid($_POST["username"]));
}
if(isset($_GET["success"]) && is_user()) {
	if(is_part_admin())$msg = $text["do_you_want"]." <a href=admin>".$text["adminarea"]."</a>?";
	else{
		if(date("H") > 18) {
			$msg = $text["nice_2_cu"][5];
		}
		elseif(date("H") > 5 && date("H") < 8) {
			$msg = $text["nice_2_cu"][6];
		}
		else {
			$msg = $text["nice_2_cu"][rand(0,4)];
		}
	}
	echo "<iframe style=display:none; src=login.php>".$text["please"]." <a href=login.php>".$text["here"]."</a> ".$text["click"]." .</iframe>";
	echo "<h1 style='margin-bottom:0;'>".text("hello")." ".ucfirst(userinfo("username")).",</h1><br><span style='font-size:18px;'>".$text["successfully_logged_in"]."</span><br><br>$msg<br><br>";
	echo "<a href='?inc=account'>>> ".$text["change_account"]."</a><br>";
	
	$result=$db->query("select value from ".$db_prefix."statistics where userid='".userid()."' and action='article' order by id desc limit 1");
	while($row=$result->fetch_object()) {$articleid=$row->value;}
	if(isset($articleid))echo "<a href='?article=".$articleid."'>>> Zuletzt angesehener Beitrag</a><br>";
}
elseif(is_user())
	echo $text["already_logged_in"];	
else {
	if(isset($nologin)&&$nologin==true)$animate="animate:false,";else $animate=null;
	echo '<script>bootbox.dialog({title:"'.ucfirst(text("login")).'",size:"small",'.$animate.'message:\'<center><form action="?inc=login" method="post" class="loginform" style="display:inline"><input class="form-control" name=username value="'.@$_GET["username"].'" placeholder="'.ucfirst($text["username"]).'" required autofocus><input class="form-control" name=password type="password" placeholder="'.ucfirst($text["password"]).'" required><div style=height:5px;></div><input class="btn btn-primary" type=submit style="width:105px;" value="'.ucfirst($text["login"]).'" name="login"></form>&ensp;<form method=post style="display:inline"><input class="btn btn-default" type=submit style="width:130px;margin-top:3px" value="'.ucfirst($text["register"]).'" name="register"></form>\'});</script>';
}
if(isset($nologin)&&$nologin==true)alert(text("error_by_login"),1);
?>
