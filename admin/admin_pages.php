<?php

if(isset($_FILES["headpicture"])){
	smkdir("../files/headpictures/");
	$type=(strstr(@$_FILES["headpicture"]["type"],"/"))?$_FILES["headpicture"]["type"]:"jpeg";
	$tmp=@explode("/",$type);
	$fileend=uniqid().".".$tmp[1];
	$abs_filename="files/headpictures/".$fileend;
	$filename="../"."files/headpictures/".$fileend;
	if(move_uploaded_file($_FILES["headpicture"]["tmp_name"],$filename))imgscale($filename,"1000","auto",$filename);else alert(text("error"),1);
	$db->query("update ".$db_prefix."pages set headpicture='".$abs_filename."' where id = '".@$_GET['id']."'");
}


if(@$_GET["a"]=="colchange" && in_array($_GET["col"],explode(",","orderid,title,description,alias"))) $db->query("update ".$db_prefix."pages set ".$_GET["col"]."='".$_POST["val"]."' where id = '".$_GET['id']."'");
if(@$_GET['dropdown']=="0") {
	$hide=true;
	if(isset($_POST['submit'])) {
		$query="insert into ".$db_prefix."pages(`orderid`,`title`,`alias`,`description`,`parent`,`headpicture`) values('".(-1+$_POST['orderid']+1)."','".$_POST['title']."','".make_alias($_POST['title'])."','".$_POST['description']."','".(-1+$_POST['parent']+1)."','')";
		echo $query;
		if($db->query($query)) {
			echo "<div class='alert alert-success'>".$text["page_created"]."</div>";
			addlog("aclogfile","Msg","Men&uuml;punkt ".$_POST['title']." von ".ucfirst(userinfo("username"))." erstellt erstellt.");
		}
		else {
			echo "<div class='alert alert-danger'>".$text["error"].".</div>";
			addlog("aclogfile","Msg","FEHLER: Seite ".$_POST['title']." von ".ucfirst(userinfo("username"))." nicht erstellt.");
		}
		$hide=false;
	}
	else {
		echo '<form action="?action='.@$action.'&dropdown='.@$_GET['dropdown'].'" method=post>'.$text['new_page']."</h2>".$text['title'].'<br>
		<input name="title" placeholder="'. $text['title_of_page'] .'" autofocus>
		<br><br>'.$text['description'].'<br>
		<input name="description" placeholder="'. $text['optional'] .'">
		<br><br>';
		if(!conf("articles_as_submenu")) {
			echo $text['parent_page']."<br>";
			echo "<select name=parent>";
			echo "<option value='0'>".$text['no']."</option>";
			$result2 = $db->query("select * from ".$db_prefix."pages order by id desc");
			while($row2 = $result2->fetch_assoc()) {
				if($row2["id"] != $row[pageid])
					echo "<option value=".$row2["id"].">".$row2["title"]."</option>";
			}
			echo "</select><br><br>";
		}
		echo '<input name="submit" value="'. $text['save'] .'" type="submit"><br></form>';			
	}
}
if(@$_GET['a']=="edit") {
	if(isset($_POST['editsubmit'])) {
		if($db->query("update ".$db_prefix."pages set `title` = '".$_POST['title']."', `description` = '".$_POST['description']."', `parent` = '".$_POST['parent']."' where id = '".@$_GET['id']."'")) {
			echo "<div class='alert alert-success'>".$text["changes_saved"]."</div>";
			addlog("aclogfile","Msg","Seite ".$_POST['title']." von ".ucfirst(userinfo("username"))." bearbeitet.");
		}
		else {
			echo "<div class='alert alert-danger'>".$text["error"]."</div>";
			addlog("aclogfile","Msg","FEHLER: Seite ".$_POST['title']." von ".ucfirst(userinfo("username"))." nicht bearbeitet.");
		}
	}
	else {
		echo '<form action="?action='.@$action.'&id='.@$_GET['id'].'&a=edit" method=post>';
		echo "<h2>".$text["edit_page"]."</h2>".$text["title"]."<br>";
		$result = $db->query("select * from ".$db_prefix."pages where id = '".@$_GET['id']."'");
		while($row = $result->fetch_assoc()) {
			echo "
		<input name='title' value='".$row["title"]."' placeholder='".$text['title_of_page']."' autofocus>
		<br><br>
		". $text['description'] ."<br>
		<input name='description' value='".$row["description"]."' placeholder='".$text['optional']."'>
		<br><br>";
		if(!conf("articles_as_submenu")) {
			echo $text['parent_page']."<br>";
			echo "<select name=parent>";
			echo "<option value='0'>".$text['no']."</option>";
			$result2 = $db->query("select * from ".$db_prefix."pages order by id desc");
			while($row2 = $result2->fetch_assoc()) {
				if($row2["id"] != $row["id"])
					echo "<option value=".$row2["id"];if($row[parent]==$row2["id"])echo " selected";echo ">".$row2["title"]."</option>";
			}
			echo "</select><br><br>";
		}
		
		echo "<input name='editsubmit' value='".$text["save"]."' type='submit'><br>";	
		}
		echo "</form>";
		echo "<br><a class=btn-comis style=cursor:pointer onmouseup=\"bootbox.alert('<form action=?action=1&a=edit&id=".$_GET["id"]."&btw_headpicture method=post enctype=multipart/form-data><input onchange=this.form.submit(); name=headpicture type=file></form>');\" style='list-style:none;float:left;border:none;'>
		<span class=' glyphicon glyphicon-plus'></span> ".text("headpicture")."
		</a>";

	}
}
if(@$hide==false) {
	?>
	<script type="text/javascript">
	$(function () {
		$('.dialog-order-pages').sortable({
		   update: function(event,ui) {
            var order=$(this).sortable('toArray').toString();
            get="reorder.php?reorder=pages&order="+order;
            $.get(get);
		   }
	   });
	});
	</script>
	<?php
	echo "<div class='dialog-order-pages dialog-order-list' style='display:none' title='".text("please_order_pages")."'><span onmouseup=\"\$(this).html('".text("order_pages_instructions")."').css('cursor','default');\" style=cursor:pointer> >> ".text("show_instructions")."</span><br>";
	$result = $db->query("select * from ".$db_prefix."pages order by orderid asc");
	while($row = $result->fetch_assoc()) {
		echo "<li id='".$row["id"]."'>".$row["title"]."</li>";
	}
	echo "</div>";
	echo "<div style='left:50px;top:15px;position:absolute'>
		<div onmouseup=\"$('.dialog-order-pages').dialog().fadeIn(0);\" class='btn-comis' style='cursor:pointer;float:left;margin-right:5px;'>".text("order_pages")."</div>
		<div onmouseup=\"$('.alias').toggle({effect:'slide'});\" class='btn-comis' style='cursor:pointer;float:left'>".text("alias")."</div>
	</div>";
//	echo "<div onmouseup=\"$('.dialog-order-pages').dialog().fadeIn(0);\" class='btn-comis' style='cursor:pointer;left:50px;top:15px;position:absolute'>".text("order_pages")."</div>";
	$result = $db->query("select * from ".$db_prefix."pages order by id desc");
	if($result) {
		if(@$_GET['a']=="delete") {
			if($db->query("delete from ".$db_prefix."pages where id = '".@$_GET['id']."'")) {
//					echo "<div class='alert alert-success'>".$text["page_deleted"]."</div>";
				addlog("aclogfile","Msg","Seite '".@$_GET['id']."' von ".ucfirst(userinfo("username"))." entfernt.");
			}
			else {
//					echo "<div class='alert alert-danger'>".$text["page_deleted_err"]."</div>";
				addlog("aclogfile","Msg","FEHLER: Seite '".$_POST['id']."' von ".ucfirst(userinfo("username"))." nicht entfernt.");
			}
		}
		if(@$_GET['a']=="edit" && @$_POST['editsubmit']!=$text["save"]) {}
		else {
			echo "<table width=100% class='table'>";
			echo "<tr>
			<th style=border:0;width:40px>ID</th>
			<th style=border:0;width:50px>OID</th>
			<th class=important style=border:0;width:200px>". $text['name'] ."</th>
			<th class=alias style=display:none;border:0;width:200px>". $text['alias'] ."</th>
			<th style=border:0;width:auto>". $text['description'] ."</th>
			<th style=border:0;text-align:right;padding-right:20px;width:200px>". $text['actions'] ."</th>
			</tr>";
			$result = $db->query("select * from ".$db_prefix."pages order by id desc");
			while($row = $result->fetch_assoc()) {
				echo "<tr>
				<td>".$row["id"]."&nbsp;</td>
				<td>".quickedit("orderid","width:44px;padding-left:0;padding-right:0;text-align:center;","number")."</td>
				<td class=important>".quickedit("title")."</td>
				<td class=alias style=display:none>".quickedit("alias","width:200px")."</td>
				<td>".quickedit("description","width:400px")."</td>
				<td>";
				$i++;
				echo "
				<div style='white-space:no-wrap;width:110px;float:right'>
					<a href='../?page=".$row["id"]."' style=outline:none;float:right><img src=../templates/main/images/preview.png title='Preview' alt='' style=height:27px;margin:-7px;margin-left:10px;margin-right:-2px;></a>
					<a href=# onmouseup=\"if(confirm('".$text['sure']."')){\$(this).parent().parent().fadeOut(400);\$.get('?action=".@$action."&a=delete&id=".$row["id"]."');}\" style=outline:none;float:right><img title='Delete' src=../templates/main/images/delete.png alt='' style=height:27px;margin:-7px;margin-left:14px;margin-right:-2px;></a>
					<a href='?action=".@$action."&a=edit&id=".$row["id"]."' style=outline:none;float:right><img title='Edit' src=../templates/main/images/edit.png alt='' style=height:27px;margin:-7px;margin-left:12px;></a>
				</div>
				</td></tr>";
			}
			echo "</table>";
		}
	}
}
