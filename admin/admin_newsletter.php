<?php
	if(is_file("../var/newsletter.json"))
		$newsletters=json_decode(file_get_contents("../var/newsletter.json"),true);
	else
		$newsletters["insert_id"]="0";
	if(isset($_GET["a"]) && $_GET["a"]=="delete") {
		foreach($newsletters["newsletters"] as $key=>$newsletter) {
			if($newsletter["id"]==$_GET["id"]) {
				unset($newsletters["newsletters"][$key]);
			}
		}
		file_put_contents("../var/newsletter.json",json_encode($newsletters));
	}
	if(isset($_POST["msg"]) && !empty($_POST["msg"])) {
		if(isset($_POST["relativeid"]) && !empty($_POST["relativeid"])) {
			$newsletters["newsletters"][$_POST["relativeid"]]=array("id"=>$newsletters["newsletters"][$_POST["relativeid"]]["id"],"subject"=>$_POST["subject"],"msg"=>$_POST["msg"],"receiver"=>$_POST["receiver"],"timestamp"=>time());
		}
		else {
			$newsletters["insert_id"]=($newsletters["insert_id"]+1);
			$newsletters["newsletters"][]=array("id"=>$newsletters["insert_id"],"subject"=>$_POST["subject"],"msg"=>$_POST["msg"],"receiver"=>$_POST["receiver"],"timestamp"=>time());
		}
	}
	if(@$_GET["dropdown"]=="0" || @$_GET["a"]=="edit" || isset($_POST['send'])) {
		if(isset($_POST['send'])) {
			$post_tos=explode("&lt;",$_POST["receiver"]);
			$post_tos=array_unique($post_tos);
			array_shift($post_tos);
			$i=0;
			foreach($post_tos as $post_to) {
				$tmp=explode("&gt;",$post_to);
				$post_to=$tmp[0];
				if(strstr($post_to,"smartlist")) {
					$result = $db->query("select * from ".$db_prefix."user");
					while($row = $result->fetch_object()) {
						if(($row->newsletter!="no") || strstr($post_to,"smartlist2")) {
							$tos[$i]["name"]=$row->name;
							$tos[$i]["email"]=$row->email;
							$tos[$i]["hash"]=$row->hash;
							$i++;
						}
					}
				}
				if(strstr($post_to,"group")) {
					$result = $db->query("select * from ".$db_prefix."user where groupid='".str_replace("group",null,$post_to)."'");
					while($row = $result->fetch_object()) {
						$tos[$i]["name"]=$row->name;
						$tos[$i]["email"]=$row->email;
						$tos[$i]["hash"]=$row->hash;
						$i++;
					}
				}
				if(strstr($post_to,"user")) {
					$result = $db->query("select * from ".$db_prefix."user where id='".str_replace("user",null,$post_to)."'");
					while($row = $result->fetch_object()) {
						$tos[$i]["name"]=$row->name;
						$tos[$i]["email"]=$row->email;
						$tos[$i]["hash"]=$row->hash;
						$i++;
					}
				}
			}
			@file_put_contents("../var/append_unsubscribe_option.var",@$_POST["append_unsubscribe_option"]);
			if($tos) {
				addlog("aclogfile","Msg","ADMIN: Newsletter (".$_POST["subject"].") wurde von ".ucfirst(userinfo("username"))." an ".count($tos)." Personen versendet.");
				file_put_contents("../log/system.log",@file_get_contents("../log/system.log")."\n\n ".@file_get_contents("../log/newsletter.log"));
				if(is_file("../log/newsletter.log"))unlink("../log/newsletter.log");
				foreach($tos as $to) {
						$title="From: Newsletter@".pref("website_title");
						$unsubscribe=(@$_POST["append_unsubscribe_option"]=="true")?"\n\n\n------------------------ \nWenn Sie diesen Newsletter nicht mehr erhalten wollen, klicken Sie auf diesen Link:\nhttp://".$_SERVER["SERVER_NAME"]."/index.php?do=unsubscribe&hash=".$to['hash']."&".$to["name"]:"";
						if(mail($to['email'], $_POST['subject'], $_POST['msg'].$unsubscribe,$title)) file_put_contents("../log/newsletter.log",@file_get_contents("../log/newsletter.log")."\n".$text['newsletter1']." (".$to['name'].")"); else file_put_contents("../log/newsletter.log",@file_get_contents("../log/newsletter.log")."\n".$text['newsletter2']." (".$to['name'].")");
				} echo "<br><h2>".$text["success"]."</h2><div id=newsletterlog style='margin-top:-20px;display:none;white-space:pre-wrap'>".@file_get_contents("../log/newsletter.log")."</div><a href=# onmousedown=\"$('#newsletterlog').fadeIn(500);$(this).hide(0);\">&gt;&gt; ".$text["newsletter_log"]."</a>";
			} else error(201503071656);
		} else {
		if(isset($_GET["id"])) {
			foreach($newsletters["newsletters"] as $key=>$object) {
				if($object["id"]==$_GET["id"]) {
					$newsletter=$object;
					$relativeid=$key;
				}
			}
		} else {
			$newsletter=null;$relativeid=null;
		}
		if(isset($_GET["to"]))$to=userinfo("username",$_GET["to"])." <user".$_GET["to"].">, ";else$to=null;
		echo "<h2>".text('send_email')."</h2>
		<div style='width:97.5%'>
			<form action=?action=6&".$text['newsletter']." method=post>
			<div class=ui-widget><input name='receiver' style='width:100%;' placeholder='".$text["receiver"]."' class='useremails' value='".$to.$newsletter["receiver"]."'></div>
			<input name=subject value='".$newsletter["subject"]."' placeholder='".$text['subject']."' style=margin-top:5px;width:100%;><br><textarea name=msg placeholder='".$text['newsletter']." ".$text['text']."' style='white-space:pre-wrap;background:hsla(0,0%,100%,.5);border:1px solid white;margin:5px 0px;width:100%;height:200px;'>".$newsletter["msg"]."</textarea><br>
			<input name='relativeid' value='".$relativeid."' type='hidden'>
			<input style='float:right;margin-right:-25px' type=submit name=send value='".$text['send_nl']."'>
			<input name='history' value='".ucfirst($text["history"])."' type='submit'>
			<input name='quicksave' value='".$text["save"]."' type='submit'>
			<input ";
			if(@file_get_contents("../var/append_unsubscribe_option.var")=="true"&&@$_GET["newsletter"]!="0")echo"checked";
			echo" type=checkbox name=append_unsubscribe_option value=true>".text("append_unsubscribe_option")."
			</form>
		</div>";
		echo '<script>$(function() {
		var useremails = [
		';
		echo '"'.$text["all"].' <smartlist1>"';
		echo ',"'.$text["smartlist2"].' <smartlist2>"';
		$result=$db->query("select * from ".$db_prefix."user");
		while($row=$result->fetch_object()) {
			echo ',"'.$row->name.' <user'.$row->id.'>"';
		}
		$result=$db->query("select * from ".$db_prefix."groups");
		while($row=$result->fetch_object()) {
			echo ',"'.$row->name.' <group'.$row->id.'>"';
		}
		echo '
		];
		 function split( val ) {
		return val.split( /,\s*/ );
		}
		function extractLast( term ) {
		return split( term ).pop();
		}
		$(".useremails").bind( "keydown", function( event ) {
		if ( event.keyCode === $.ui.keyCode.TAB &&
		$( this ).autocomplete( "instance" ).menu.active ) {
		event.preventDefault();
		}
		})
		.autocomplete({
		minLength: 0,
		source: function( request, response ) {
		response( $.ui.autocomplete.filter(
		useremails, extractLast( request.term ) ) );
		},
		focus: function() {
		return false;
		},
		select: function( event, ui ) {
		var terms = split( this.value );
		terms.pop();
		terms.push( ui.item.value );
		terms.push( "" );
		this.value = terms.join( ", " );
		return false;
		}
		});
		});</script>';
		}
	}
	else {
		echo "<table width=100% class='table'><tr>
		<th style=border:0;width:3%;>ID</th>
		<th style=border:0; width:20%>".$text['date']."</th>
		<th class=important style=border:0; width:23%>".$text['subject']."</th>
		<th style=border:0; width:20%>".$text['receiver']."</th>
		<th style=border:0;width:70px>".$text['actions']."</th>";
		if(isset($newsletters["newsletters"])&&count($newsletters["newsletters"])) {
			foreach($newsletters["newsletters"] as $newsletter) {
				echo "<tr>
				<td>".$newsletter["id"]."</td>
				<td>".date("d.m.Y H:i",$newsletter["timestamp"])."</td>
				<td class=important>".$newsletter["subject"]."</td>
				<td>".$newsletter["receiver"]."</td>
				<td>
				<div style='white-space:no-wrap;width:110px;float:right'>
					<a href='?action=".$action."&a=edit&id=".$newsletter["id"]."' ><img src=../templates/main/images/edit.png alt='' style=height:27px;margin:-7px;></a>
					<a href='#' onmouseup=\"\$this=\$(this);\$.get('?action=".$action."&a=delete&id=".$newsletter["id"]."&history',function(response){\$this.parent().parent().hide({effect:'fade'},400);});\"><img src=../templates/main/images/delete.png alt='' style=height:27px;margin:-7px;margin-left:10px;></a>
				</div>
				</td>
				</tr>";
			}
		}
		echo "</table>";
	}
	file_put_contents("../var/newsletter.json",json_encode($newsletters));