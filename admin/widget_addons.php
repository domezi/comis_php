<?php @session_start(); ?>
<?php /*widgetsettings*/ include_once("../db.php");if(!is_sudo())return;?><h1 class='widget-headline'>Neueste Addons</h1>
<div id="addonswidgetcontent">
<?php
echo "<div class='blackscreen fullscreen addonloadingscreen' style=display:none><img style='position:absolute;top:50%;margin-top:-80px;' class='rotatefast' src=../templates/main/images/load.png></div>";
$tmp=import_conf("../var/_dash_widgets_prefs.conf");
if(!isset($_SESSION["addonswidgetcontent"]) || isset($_GET["reload"]))$_SESSION["addonswidgetcontent"]=@file_get_contents("http://www.domiscms.de/addons/addons.json");
if(!empty($_SESSION["addonswidgetcontent"])) {
	$addons=json_decode($_SESSION["addonswidgetcontent"],1);$a=0;
	shuffle($addons["addons"]);$out=null;
	foreach($addons["addons"] as $addon) {
		if(is_file("../addons/".$addon["files"][0].".php"))$out.="<a onmouseup=\"$('.addonloadingscreen').fadeIn(400);$.get('?action=8&remove_addon=".$addon["id"]."',function(){\$('div#addons #addonswidgetcontent').html('".text("addon_was_deleted")."');$('.addonloadingscreen').fadeOut(400);$('.addon".$addon["id"]."-btn-installed').toggle();$('.addon".$addon["id"]."-btn-remove').toggle();$('.addon".$addon["id"]."-btn-install').toggle();});\" class='btn btn-warning' style='cursor:pointer;padding:8px;padding-top:5px;padding-bottom:0px;margin-bottom:5px;'><img src=../templates/main/images/delete.png style=height:15px></a>";else$out.="<a onmouseup=\"$('.addonloadingscreen').fadeIn(400);$.get('?action=8&install_addon=".$addon["id"]."',function(){\$('div#addons #addonswidgetcontent').html('".text("addon_was_installed")."');$('.addonloadingscreen').fadeOut(400);$('.addon".$addon["id"]."-btn-installed').toggle();$('.addon".$addon["id"]."-btn-remove').toggle();$('.addon".$addon["id"]."-btn-install').toggle();});\" class='btn btn-success' style='padding-left:8px;padding-right:8px;padding-top:5px;margin-bottom:5px;'><img src=../templates/main/images/arr.png></a>";
		$out.=" ".$addon["name"]."<br>";
		if($a==$tmp["addons_amnt"])return;$a++;
	}
}
echo(empty($_SESSION["addonswidgetcontent"]))?"Fehler beim holen der Addons.":$out;
?>
</div>
<style type="text/css">
#addonswidgetcontent .btn-success {
	padding: 0px 8px;
	margin-top: 3px;
	opacity:.9;
	background:#5CB85C;
}
#addonswidgetcontent .btn-success:hover {
	opacity:1;
	background:#56cf56;
}
</style>