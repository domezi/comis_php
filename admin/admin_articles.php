<?php

if(isset($_FILES["headpicture"])){
	smkdir("../files/headpictures/");
	$type=(strstr(@$_FILES["headpicture"]["type"],"/"))?$_FILES["headpicture"]["type"]:"jpeg";
	$tmp=@explode("/",$type);
	$fileend=uniqid().".".$tmp[1];
	$abs_filename="files/headpictures/".$fileend;
	$filename="../"."files/headpictures/".$fileend;
	if(move_uploaded_file($_FILES["headpicture"]["tmp_name"],$filename))imgscale($filename,"1000","auto",$filename);else alert(text("error"),1);
	$db->query("update ".$db_prefix."articles set headpicture='".$abs_filename."' where id = '".$_GET['id']."'");
}

if(is_allowed("articles")&&isset($_FILES["attachment-file"]["name"])) {
	smkdir("../files/uploads/");
	$filename="files/uploads/".uniqid()."-".trim($_FILES["attachment-file"]["name"]);
	move_uploaded_file($_FILES["attachment-file"]["tmp_name"],"../".$filename);
	if(is_file("../".$filename))$url=$filename;else $url=$_POST["attachment-url"];
	$result = $db->query("select attachments from ".$db_prefix."articles where id = '".@$_GET['id']."'");
	if(is_object($result)) {
		while($row = $result->fetch_object()) {
			$attachments=json_decode($row->attachments,true);
			$attachments["insert_id"]=$attachments["insert_id"]+1;
			$attachments["attachments"][$attachments["insert_id"]]["title"]=$_POST["attachment-title"];
			$attachments["attachments"][$attachments["insert_id"]]["url"]=$url;
			if(!empty($url)&&$db->query("update ".$db_prefix."articles set attachments='".json_encode($attachments)."' where id = '".@$_GET['id']."'")) {
				alert("Anhang wurde erfolgreich erstellt!",1);
				addlog("aclogfile","Msg","Anhang ".$_POST["attachment-title"]." wurde dem Beitrag ".$_POST['title']." von ".ucfirst(userinfo("username"))." angeh&auml;ngt.");
			} else alert("Es ist ein Fehler aufgetreten",1);
		}
	} else alert(text("error"),1);
}
if(@$_GET["a"]=="colchange" && in_array($_GET["col"],explode(",","orderid,title,editor,timestamp,alias"))) $db->query("update ".$db_prefix."articles set ".$_GET["col"]."='".$_POST["val"]."' where id = '".$_GET['id']."'");
elseif(@$_GET["a"]=="reset_views") {
	if($db->query("update ".$db_prefix."articles set views = 0 where id = '".@$_GET['id']."'"))
		echo "<div class='alert alert-success'>".$text['success'].".</div>";
}
if(isset($_POST["new_pageid"])) {
	$db->query("update ".$db_prefix."articles set pageid = ".$_POST["new_pageid"]." where id = '".$_POST["id"]."'");
}
if(@$_GET["disable_public"]=="true") {
	echo "<div class='alert alert-success'>".$text["success"]."</div>";
	$db->query("update ".$db_prefix."articles set `public` = 'yes' where id = '".@$_GET['id']."'");
}
elseif(@$_GET["disable_public"]=="false") {
	echo "<div class='alert alert-success'>".$text["success"]."</div>";
	$db->query("update ".$db_prefix."articles set `public` = 'no' where id = '".@$_GET['id']."'");
}
if(@$_GET["disable_comments"]=="true") {
	echo "<div class='alert alert-success'>".$text["success"]."</div>";
	$db->query("update ".$db_prefix."articles set `comments` = 'yes' where id = '".@$_GET['id']."'");
}
elseif(@$_GET["disable_footer"]=="false") {
	echo "<div class='alert alert-success'>".$text["success"]."</div>";
	$db->query("update ".$db_prefix."articles set `footer` = 'no' where id = '".@$_GET['id']."'");
}
if(@$_GET["disable_footer"]=="true") {
	echo "<div class='alert alert-success'>".$text["success"]."</div>";
	$db->query("update ".$db_prefix."articles set `footer` = 'yes' where id = '".@$_GET['id']."'");
}
elseif(@$_GET["disable_header"]=="false") {
	echo "<div class='alert alert-success'>".$text["success"]."</div>";
	$db->query("update ".$db_prefix."articles set `header` = 'no' where id = '".@$_GET['id']."'");
}
if(@$_GET["disable_header"]=="true") {
	echo "<div class='alert alert-success'>".$text["success"]."</div>";
	$db->query("update ".$db_prefix."articles set `header` = 'yes' where id = '".@$_GET['id']."'");
}
elseif(@$_GET["disable_comments"]=="false") {
	echo "<div class='alert alert-success'>".$text["success"]."</div>";
	$db->query("update ".$db_prefix."articles set `comments` = 'no' where id = '".@$_GET['id']."'");
}
if(@$_GET["disable_page"]=="true") {
	echo "<div class='alert alert-success'>".$text["success"]."</div>";
	$db->query("update ".$db_prefix."articles set `page` = 'yes' where id = '".@$_GET['id']."'");
}
elseif(@$_GET["disable_page"]=="false") {
	echo "<div class='alert alert-success'>".$text["success"]."</div>";
	$db->query("update ".$db_prefix."articles set `page` = 'no' where id = '".@$_GET['id']."'");
}
if(isset($_POST['code']))$_POST['code']=str_replace("\\'", "'", $_POST['code']);
if(@$_GET['dropdown']=="0") {
	$hide=true;
	if(isset($_POST['submit'])) {
		(isset($_POST['pageyn'])&&$_POST['pageyn']=="yes")?$page2="yes":$page2="no";
		(@$_POST['article_comments']=="yes")?$comments="yes":$comments="no";	
		$publish_ts=(-1+strtotime($_POST["publish_ts"])+1);
		((@$_POST['article_public']=="yes" && ($publish_ts<time() || $publish_ts=="")) || $publish_ts<time())?$public="yes":$public="no";
		if(@$_POST['article_public']=="" && $publish_ts=="")$public="no";
		$_POST["code"]=str_replace("&lt;", "<", $_POST["code"]);
		$_POST["code"]=str_replace("&gt;", ">", $_POST["code"]);
		$_POST["code"]=str_replace("&quot;", "\"", $_POST["code"]);
		$_POST["code"]=str_replace("&#039;", "'", $_POST["code"]);
		$_POST["code"]=str_replace("&amp;", "&", $_POST["code"]);
		$_POST["code"]=str_replace("&#039;", "&", $_POST["code"]);
		$footeryn=(isset($_POST["footeryn"]))?"yes":"no";
		file_put_contents("../var/footeryn.var",$_POST["footeryn"]);
		$query="insert into ".$db_prefix."articles(`attachments`,`headpicture`,`views`,`php`,`orderid`,`footer`,`pageid`,`title`,`alias`,`code`,`editor`,`timestamp`,`public`,`publish_ts`,`page`,`comments`) values('','','0','','0','".$footeryn."','".$_POST['pageid']."','".$_POST['title']."','".make_alias($_POST["title"])."','".str_replace("'","\&\#39;",$_POST['code'])."','".userinfo("username")."','".time()."','".$public."','".$publish_ts."','".$page2."','".$comments."')";
//		echo $query;
		if($db->query($query)) {
			$insertid=$db->insert_id;
			echo "<div class='alert alert-success'>". $text['article_add_success'] .".</div>";
			if(@$_POST["view_next"]=="true" && $_POST["pageidget"]=="" && $db->insert_id!=0 && $_POST["submit"]!="return")
				echo(conf("modrewrite"))?"<script>window.location.href='../".make_alias($_POST["title"]).conf("rewrite_suffix")."';</script>":"<script>window.location.href='../?article=".$db->insert_id."';</script>";
			elseif($_POST["pageidget"]!="")
				echo "<script>window.location.href='../?page=".$_POST['pageid']."';</script>";
				addlog("aclogfile","Msg","Beitrag ".$_POST['title']." von ".ucfirst(userinfo("username"))." erstellt.");
		}
		else {
			echo "<div class='alert alert-danger'>". $text['article_add_success_err'] .".</div>";
			addlog("aclogfile","Msg","FEHLER: Beitrag ".$_POST['title']." von ".ucfirst(userinfo("username"))." nicht erstellt.");
		}
		$show_all=true;
		if($_POST["submit"]=="return")refresh("?action=2&a=edit&id=".$insertid."&add_attachment");
	}
	else {
		echo '<form action="?action='.@$action.'&dropdown='.@$_GET['dropdown'].'" method=post>';
		echo "<h2 style=margin-top:-10px;>".text('add_article')."</h2>";
			echo $text['page'] ."<br>";
			echo "<select style=width:400px;margin-bottom:10px name=pageid>";
			echo "<option value=0>". $text['empty'] ."</option>";
			$result2 = $db->query("select * from ".$db_prefix."pages order by title desc");
			if($result2->num_rows) {
				while($row2 = $result2->fetch_assoc()) {
					echo "<option value=".$row2["id"]." ";
						if(isset($_GET["page"])&&$row2["id"]==$_GET["page"])echo"selected";
					echo ">".$row2["title"]."</option>";
				}
			}
			($row['public']=="yes")?$publish_checked="checked":$publish_checked=false;
			($row['comments']=="yes")?$allow_comments_checked="checked":$allow_comments_checked=false;
			echo "</select><br>";
			echo $text['header2']."<br>
			<input style=width:400px;margin-bottom:20px name='title' placeholder='". $text['header2'] ."' autofocus><br>
			<textarea name='code' class='htmleditor ckeditor' placeholder='". $text['text'] ."' style=width:88%;height:250px;></textarea><br>
			";
			if(pref("attachments")=="true") {
				echo text("attachments")."
				<ul class='bg article-attachments'>";
				echo "<li style='cursor:not-allowed' onmouseup='addAttachment()' style=list-style:none;float:left;border:none><button name='submit' type='submit' value='return' class=bg style=''>+ ".text("add_attachment")."</button></li>";
				echo "<br class=clear></ul><br>";
			}
			echo "
			<input type=checkbox name=article_public value=yes checked> ". $text['publish'].$text['publish_by_time'] ." <input type=text style='background:none;border:none;border-bottom:1px solid black;padding:0 0;border-radius:0;width:90px;text-align:center' name=publish_ts placeholder='MM/DD/YYYY' class='datepicker'> ).<br>
			";echo(pref("comments")=="yes")?"<input type=checkbox name=article_comments value=yes checked> ". $text['allow_comments'] ." <br>":false;echo "
			<input type=checkbox name=view_next value=true ";if(@$_GET["view_next"]=="true") echo "checked";echo "> ".$text['view_next']."<br>
			";
			echo "<input type=checkbox name=pageyn value=yes> ".$text['as']." ".$text['page']."";
			echo "<br><input type=checkbox name=footeryn value=yes";
			if(!is_file("../var/footeryn.var")||file_get_contents("../var/footeryn.var")=="yes")echo" checked";
			echo"> ".text("footer_on_short");
			echo "<br>
			<br>
			<input name='pageidget' value='".@$_GET["pageid"].@$_GET["page"]."' type='hidden'>
			<input name='submit' value='".$text["save"]."' type='submit'>
			</form>
			";
	}
}
elseif(@$_GET['dropdown']=="1") {
	if(isset($_GET["delete"])&&!strstr($_GET["delete"],"../")) {
		@unlink("../files/my_pictures/thumb_".$_GET["delete"]);
		@unlink("../files/my_pictures/".$_GET["delete"]);
		@unlink("../files/my_pictures/hd_".$_GET["delete"]);
	}
	if(isset($_FILES["picture"]["tmp_name"])) {
		smkdir("../files/my_pictures");
		$type=(strstr(@$_FILES["picture"]["type"],"/"))?$_FILES["picture"]["type"]:"jpeg";
		$tmp=@explode("/",$type);
		$fileend=time().rand(0,9e9).".".$tmp[1];
		$filename="../files/my_pictures/".$fileend;
		$thumb_filename="../files/my_pictures/thumb_".$fileend;
		$pre_filename="../files/my_pictures/";
		if(move_uploaded_file($_FILES["picture"]["tmp_name"],$filename))imgscale($filename,"auto","150",$thumb_filename);else echo "<div class='alert alert-danger' style='font-weight:bold;text-algin:center;float:right;position:absolute;bottom:20px;box-shadow:0px 0px 100px 20px black;left:40px;right:40px;z-index:99999999999999999999999;'>".$text["error"]."</div>";
		$tmp=@getimagesize($filename);
		if($tmp[0] > 2000){copy($filename,$pre_filename."hd_".$fileend);imgscale($filename,"auto","2000",$filename);}
		echo '<script>$(function(){$(".magic-blackscreen").show();$(".magic-blackscreen").fadeOut(500);});</script>';
	}
	echo "<div class='blackscreen magic-blackscreen'><img style=height:100px;top:50%;margin-top:-100px;position:fixed; class=rotatefast src=../templates/main/images/load.png></div>";
	echo "<h1>".$text["my_pictures"]."</h1>";
	echo "<div style='height:200px;width:200px;background-image:url(\"../templates/main/images/add-white.gif\");background-size:cover;margin:5px;float:left;border:3px solid white;border-radius:5px'>
	<form action='?action=2&dropdown=1' method='post' enctype='multipart/form-data'>
		<input onchange=\"$('.magic-blackscreen').fadeIn(500);this.form.submit();\" name='picture' type='file' style='width:200px;height:200px;padding:0;opacity:0;cursor:pointer;'>
	</form>
	</div>";
	$picid=null;
	foreach(array_reverse(glob("../files/my_pictures/thumb_*")) as $thumb) {
		$picid++;
		if(file_exists(str_replace("../files/my_pictures/thumb_","../files/my_pictures/",$thumb))) {
			echo "<div class='template-preview picture".$picid."' style='border:3px solid white;padding-bottom:px;'>
			<a onmouseup=\"bootbox.alert('<h3 style=margin:0;>".text("this_is_the_file_url").":</h3><br><div class=form-control>".explode("admin/?",$_SERVER["HTTP_REFERER"])[0].str_replace("../","",str_replace("thumb_","",$thumb))."</div>');\"><div style='cursor:pointer;height:200px;width:200px;'>
				<div class='template-preview-screeny' style='background-image:url(\"".$thumb."\");width:200px;background-position:center center;background-size:cover;'>
			</div></a>
			<div class='template-preview-btns' style='display:none;margin-top:-200px;width:200px;'>";
					echo "<a onmouseup=\"if(confirm('".$text["sure"]."')){\$.get('?action=2&dropdown=1&delete=".str_replace("../files/my_pictures/thumb_","",$thumb)."');\$('.picture".$picid."').fadeOut(500);}\" class='btn-comis' style='position:absolute;top:5px;right:5px;padding:2.5px 8px;color:#400;background:hsla(0,100%,82%,.7)'>X</a> ";
					echo "<a onmouseup=\"$('#beforecontainer').html('<div title=\'URL ".$text["image"]." LD\' class=dialog".time()."><img style=margin-left:2px;border-radius:3px src=".$thumb."><div style=font-size:40px;margin-top:-130px;position:absolute;left:105px;border-style:solid;border-color:gray;border-width:5px;background:black;padding:20px;color:gray;opacity:.8;border-radius:10px>LD</div><br><input style=width:91% class=\'text ui-widget-content ui-corner-all\' value=\'".str_replace("../files/my_pictures/thumb_",str_replace("/admin/?action=2&dropdown=1",null,$_SERVER["HTTP_REFERER"])."/files/my_pictures/thumb_",$thumb)."\'></div>');$('.dialog".time()."').dialog();\" class='btn-comis'>LD</a> ";
					echo "<a onmouseup=\"$('#beforecontainer').html('<div title=\'URL ".$text["image"]." SD\' class=dialog".time()."><img style=margin-left:2px;border-radius:3px src=".$thumb."><div style=font-size:40px;margin-top:-130px;position:absolute;left:102px;border-style:solid;border-color:#444;border-width:5px;background:orange;padding:20px;color:#444;opacity:.8;border-radius:10px>SD</div><br><input style=width:91% class=\'text ui-widget-content ui-corner-all\' value=\'".str_replace("../files/my_pictures/thumb_",str_replace("/admin/?action=2&dropdown=1",null,$_SERVER["HTTP_REFERER"])."/files/my_pictures/",$thumb)."\'></div>');$('.dialog".time()."').dialog();\" class='btn-comis'>SD</a> ";
					echo(file_exists(str_replace("../files/my_pictures/thumb_","../files/my_pictures/hd_",$thumb)))?"<a onmouseup=\"$('#beforecontainer').html('<div title=\'URL ".$text["image"]." HD\' class=dialog".time()."><img style=margin-left:2px;border-radius:3px src=".$thumb."><div style=font-size:40px;margin-top:-130px;position:absolute;left:105px;border-style:solid;border-color:white;border-width:5px;background:cornflowerblue;padding:20px;color:white;opacity:.8;border-radius:10px>HD</div><br><input style=width:91% class=\'text ui-widget-content ui-corner-all\' value=\'".str_replace("../files/my_pictures/thumb_",str_replace("/admin/?action=2&dropdown=1",null,$_SERVER["HTTP_REFERER"])."/files/my_pictures/hd_",$thumb)."\'></div>');$('.dialog".time()."').dialog();\" class='btn-comis'>HD</a>":"<a class='btn-comis' style=opacity:.5;cursor:default>HD</a> ";
			$tmp=getimagesize(str_replace("../files/my_pictures/thumb_","../files/my_pictures/",$thumb));
			echo "</div>
			</div>
			<div class='template-preview-name' style='padding-left:5px;padding-bottom:5px;margin-top:-40px;overflow:hidden;font-size:16px;'><span style=float:left;>".$tmp[0]."x".$tmp[1]."px</span><span style=float:right>".round(filesize(str_replace("../files/my_pictures/thumb_","../files/my_pictures/",$thumb))/1000,2)." KB</span></div>
			</div>";
		}
	}
}
if(@$_GET['a']=="edit") {
	if(isset($_POST['articlesubmit'])) {
		($_POST['article_public']=="yes")?$public="yes":$public="no";			
		(@$_POST['pageyn']=="yes")?$page="yes":$page="no";			
		(isset($_POST['article_comments'])&&$_POST['article_comments']=="yes")?$comments="yes":$comments="no";
		$_POST["code"]=str_replace("&lt;", "<", $_POST["code"]);
		$_POST["code"]=str_replace("&gt;", ">", $_POST["code"]);
		$_POST["code"]=str_replace("&quot;", "\"", $_POST["code"]);
		$_POST["code"]=str_replace("&#039;", "'", $_POST["code"]);
		$_POST["code"]=str_replace("&amp;", "&", $_POST["code"]);
		$_POST["code"]=str_replace("&#039;", "&", $_POST["code"]);
		$footeryn=(isset($_POST["footeryn"]))?"yes":"no";
		$publish_ts=strtotime($_POST["publish_ts"]);
		(($_POST['article_public']=="yes" && ($publish_ts<time() || $publish_ts=="")) || $publish_ts<time())?$public="yes":$public="no";
		if($_POST['article_public']=="" && $publish_ts=="")$public="no";
		if($db->query("update ".$db_prefix."articles set `footer` = '".$footeryn."',`pageid` = '".$_POST['pageid']."', `title` = '".$_POST['title']."', `php` = '".$PHPCODE."', `code` = '".str_replace("'","\&\#39;",$_POST['code'])."', `editor` = '".userinfo('username')."', `timestamp` = '".time()."', `public` = '".$public."', `publish_ts` = '".$publish_ts."', `page` = '".$page."', `comments` = '".$comments."' where id = '".@$_GET['id']."'")) {
			echo "<div class='alert alert-success'>".$text['changes_saved'].".</div>";
			addlog("aclogfile","Msg","Beitrag ".$_POST['title']." von ".ucfirst(userinfo("username"))." bearbeitet.");
			if(@$_POST["view_next"]=="true" && $_POST["pageidget"]=="" && @$_GET['id']!="" && $_POST["articlesubmit"]!="return")
				echo "<script>window.location.href='../?article=".$_GET['id']."';</script>";
			elseif(@$_POST["pageidget"]!="")
				echo "<script>window.location.href='../?page=".$_POST['pageid']."';</script>";
		}
		else {
			echo "<div class='alert alert-danger'>".$text['article_edit_err'].".</div>";
			addlog("aclogfile","Msg","FEHLER: Beitrag ".$_POST['title']." von ".ucfirst(userinfo("username"))." bearbeitet.");
		}
		if($_POST["articlesubmit"]=="return")refresh("?action=2&a=edit&id=".$_GET["id"]."&add_attachment");
	}
	else {
		echo '<form action="?action='.@$action.'&id='.@$_GET['id'].'&a=edit" method="post">';
		echo "<h2 style=margin-top:-10px;>".$text['edit_article']."</h2>";
		$result = $db->query("select * from ".$db_prefix."articles where id = '".@$_GET['id']."'");
		while($row = $result->fetch_assoc()) {
			$result2 = $db->query("select * from ".$db_prefix."pages order by title desc");
			echo $text['page']."<br>";
			echo "<select style=width:400px;margin-bottom:10px name=pageid>";
			echo "<option value=".$row[pageid].">".$text['unchanged']."</option>";
			while($row2 = $result2->fetch_assoc()) {
				if($row2["id"]!=$row[pageid])
					echo "<option value=".$row2["id"].">".$row2["title"]."</option>";
			}
			($row['public']=="yes")?$publish_checked="checked":$publish_checked=false;
			($row['comments']=="yes")?$allow_comments_checked="checked":$allow_comments_checked=false;
			echo "</select><br>";
			echo $text['header2']."<br>
			<input style=width:400px;margin-bottom:20px name='title' value='".$row["title"]."' placeholder='".$text['header2']."' autofocus>
			<textarea name='code' class='htmleditor ckeditor' placeholder='".$text['text']."' style=width:88%;height:250px;>".$row["code"]."</textarea><br>
			<textarea name='php' style=";if(strlen($row["php"])<1)echo"display:none;";echo"white-space:pre-wrap;width:100%;margin-bottom:20px placeholder='PHP - Code'>".$row["php"]."</textarea>";
			if(pref("attachments")=="true") {
				echo text("attachments")."
				<ul data-article-id='".$_GET["id"]."' style='/*border-radius:5px;padding:7px;background:white*/' class='bg article-attachments'>";
				$a=0;
				$attachments=json_decode($row["attachments"],true);
				if(is_array($attachments["attachments"])) {
					foreach($attachments["attachments"] as $index=>$attachment) {
						$a++;
						echo "<li data-attachment-id='$index' class='bg'><span class=data-title>".$attachment["title"]."</span><span class='link' onmouseup=\"window.location.href='".$attachment["url"]."'\"></span>&nbsp;<span class=edit></span>&nbsp;<span class=remove></span>&nbsp;</li>";
					}
				}
				echo "<li style='cursor:not-allowed' onmouseup='addAttachment()' style='list-style:none;float:left;border:none;'><button name='articlesubmit' type='submit' value='return' class='btn btn-default' style='background:hsla(0,0%,98%,.9);border:none;'><span class=' glyphicon glyphicon-paperclip'></span> ".text("add_attachment")."</button></li>";
				
				if(strlen($row["php"])<1)echo "<li style='cursor:not-allowed' onmouseup=\"$('textarea[name=php]').toggle();$(this).hide()\" style='list-style:none;float:left;border:none;'><div class='btn btn-default' style='background:hsla(0,0%,98%,.9);border:none;'><span class=' glyphicon glyphicon-plus'></span> ".text("PHP")."</li>";
				
				echo "<li onmouseup=\"bootbox.alert('<form action=?action=2&a=edit&id=".$_GET["id"]."&btw_headpicture method=post enctype=multipart/form-data><input onchange=this.form.submit(); name=headpicture type=file></form>');\" style='list-style:none;float:left;border:none;'>
				<div class='btn btn-default' style='background:hsla(0,0%,98%,.9);'><span class=' glyphicon glyphicon-plus'></span> ".text("headpicture")."</li>";

				echo "<br class=clear></ul><br>";
			}
			echo "<br>
			<input type=checkbox name=article_public value=yes ".$publish_checked."> ".$text['publish']. $text['publish_by_time'] ." <input type=text style='background:none;border:none;border-bottom:1px solid black;padding:0 0;border-radius:0;width:90px;text-align:center' name=publish_ts placeholder='MM/DD/YYYY' "; echo($row["publish_ts"]=="0")?"":" value='".date("m/d/Y",$row["publish_ts"])."'"; echo " class='datepicker'> ).<br>
			";echo(pref("comments")=="yes")?"<input type=checkbox name=article_comments value=yes ".$allow_comments_checked."> ". $text['allow_comments'] ." <br>":false;echo "
			<input type=checkbox name=view_next value=true ";if(@$_GET["view_next"]=="true")echo "checked";echo "> ".$text['view_next']."<br>
			";
			echo "<input type=checkbox name=pageyn value=yes ";if($row["page"]=="yes") echo "checked";echo "> ".$text['as']." ".$text['page']."";
			echo "<br><input type=checkbox name=footeryn value=yes ";if($row["footer"]=="yes") echo "checked";echo "> ".text("footer_on_short")."";
			echo "<br><br>
			<input name='pageidget' value='".@$_GET["pageid"].@$_GET["page"]."' type='hidden'>
			<input name='articlesubmit' value='".$text["save"]."' type='submit'>
			<br>
			<br>
			";
			}
		echo "</form>";
		?>
		<script type="text/javascript">
		$(function () {
			articleid=$(".article-attachments").attr("data-article-id");
			$(".article-attachments").find("li").find(".remove").on("click",function () {
				$attachment=$(this).parent();
				var id=$attachment.attr("data-attachment-id");
				bootbox.confirm("Wollen Sie diesen Anhang wirklich entfernen? Dieser Anhang besitzt die ID "+id+".",function (confirm) {
					if (confirm===true) {
						$.get('ajax_hook.php?action=remove_attachment&id='+id+"&article_id="+articleid,function (answer) {
							if(answer=="1") {
								$attachment.fadeOut(500);bootbox.alert("Der Anhang wurde entfernt!")
							} else {
								bootbox.alert("Ein Fehler trat auf!")
							}
						});
					}
				});
			});
			$(".article-attachments").find("li").find(".edit").on("click",function () {
				var id=$(this).parent().attr("data-attachment-id");
				$title=$(this).parent().find(".data-title");
				bootbox.prompt("Neuer Titel?",function (title) {
					if (title!==null) {
						$.get('ajax_hook.php?action=edit_attachment&new_title='+title+'&id='+id+"&article_id="+articleid,function (answer) {
							if(answer=="1") {
								$title.html(title);bootbox.alert("Der Anhang steht ab jetzt unter neuem Name bereit!");
							} else {
								bootbox.alert("Ein Fehler trat auf!");
							}
						})
					}
				});
			});
		});
		function addAttachment() {
			id=$(".article-attachments").attr("data-article-id");
			bootbox.dialog({
				title:"Neuer Anhang",
				message:"<form method=post enctype=multipart/form-data action=?action=2&a=edit&id="+id+" class='attachment-add'>Titel:<br><input class=form-control name=attachment-title placeholder='Titel des Anhangs' required><br>Datei ausw&auml;hlen oder URL zu einer Datei einf&uuml;gen:<br><input class=form-control style=display:inline-block;width:40%; name=attachment-file type=file><input class=form-control placeholder='Optional URL zu einer Datei' type=url name=attachment-url style=display:inline-block;width:59%;margin-left:1%><br></div><hr><button type=submit href=# style=float:right class='btn btn-primary' onmouseup=\"\">Speichern</button><br class=clear></form>",
			});
		}
		</script>
		<?php
		if(isset($_GET["add_attachment"]))echo "<script>$(function(){addAttachment();});</script>";
	}
}
if(@$hide==false) {
	$result = $db->query("select * from ".$db_prefix."articles order by id desc");
	if($result) {
		if(@$_GET['a']=="delete") {
			if($db->query("delete from ".$db_prefix."articles where id = '".@$_GET['id']."'")) {
				$db->query("delete from ".$db_prefix."comments where articleid = '".@$_GET['id']."'");
				if(@$_GET["pageid"]!="") echo "<script>window.location.href='../?page=".@$_GET["pageid"]."'</script>";
				addlog("aclogfile","Msg","Beitrag '".@$_GET['id']."' von ".ucfirst(userinfo("username"))." entfernt.");
			}
			else {
				addlog("aclogfile","Msg","FEHLER: Beitrag '".$_POST['id']."' von ".ucfirst(userinfo("username"))." nicht entfernt.");
			}
		}
		if(!(@$_GET['a']=="edit" && !isset($_POST['articlesubmit']))&&!isset($_GET["dropdown"])) {
			echo "<div class='dialog-order-articles dialog-order-list' style='display:none' title='".text("please_order_articles")."'><span onmouseup=\"\$(this).html('".text("order_articles_instructions")."').css('cursor','default');\" style=cursor:pointer> >> ".text("show_instructions")."</span><br><span onmouseup=\"\$(this).html('".text("order_articles_why_answer")."').css('cursor','default');\" style=cursor:pointer> >> ".text("order_articles_why")."</span><br>";
			$result = $db->query("select * from ".$db_prefix."articles where page = 'yes' order by orderid asc");
			while($row = $result->fetch_assoc()) {
				echo "<li id='".$row["id"]."'>".$row["title"]."</li>";
			}
			echo "</div>";
			echo "<div style='left:50px;top:15px;position:absolute'>
				<div onmouseup=\"$('.dialog-order-articles').dialog().fadeIn(0);\" class='btn-comis' style='cursor:pointer;float:left;margin-right:5px;'>".text("order_articles")."</div>
				<div onmouseup=\"$('.alias').toggle({effect:'slide'});\" class='btn-comis' style='cursor:pointer;float:left'>".text("alias")."</div>
			</div>";
			echo "<table width=100% class='table'>";
			echo "<tr>
			<th style=border:0;width:40px>ID</th>
			<th style=border:0;width:50px>OID</th>";
			echo "<th style=border:0;width:100px>".$text['page']."&emsp;</th>";
			echo "<th class=important style=border:0;width:100px>&emsp;".$text['title']."</th>
			<th class=alias style=display:none;border:0;width:100px>&emsp;".$text['alias']."</th>
			<th style=border:0;width:100px>".$text['editor']."</th>
			<th style=border:0;width:200px;text-align:center>".$text['date']."</th>
			<th style=border:0;width:100px>".$text['views']."</th>
			<th style=border:0;width:100px>".$text['comments']."</th>
			<th style=border:0;width:100px>".$text['comments']."</th>
			<th style=border:0;width:100px>".$text['public']."</th>
			<th style=border:0;width:100px>".$text['page']."</th>
			<th style=border:0;width:80px><center>".$text['attachments']."</center></th>
			<th style=border:0;width:200px;text-align:right;padding-right:20px>".$text['actions']."</th>
			</tr>";
			$result = $db->query("select * from ".$db_prefix."articles order by id desc");
			while($row = $result->fetch_assoc()) {
				if($row['public']=="yes") {$public="<a onmouseup=\"$.ajax({data:'',type:'post',url:'index.php?action=2&disable_public=false&id=".$row["id"]."'});window.location.href=window.location.href;\"><img src=../templates/main/images/switch1.png style=height:28px;margin-bottom:-5px onmousedown=$(this).attr('src','../templates/main/images/switch0.png')></a>";} elseif($row['publish_ts']!="0") {$public="<a onmouseup=\"$.ajax({data:'',type:'post',url:'index.php?action=2&disable_public=true&id=".$row["id"]."'});window.location.href=window.location.href;\"><img src=../templates/main/images/switch10.png style=height:28px;margin-bottom:-5px onmousedown=$(this).attr('src','../templates/main/images/switch1.png')></a>";} else {$public="<a onmouseup=\"$.ajax({data:'',type:'post',url:'index.php?action=2&disable_public=true&id=".$row["id"]."'});window.location.href=window.location.href;\"><img src=../templates/main/images/switch0.png style=height:28px;margin-bottom:-5px onmousedown=$(this).attr('src','../templates/main/images/switch1.png')></a>";}
				if($row['comments']=="yes" && pref("comments")=="yes") {$comments="<a onmouseup=\"$.ajax({data:'',type:'post',url:'index.php?action=2&disable_comments=false&id=".$row["id"]."'});window.location.href=window.location.href;\"><img src=../templates/main/images/switch1.png style=height:28px;margin-bottom:-5px onmousedown=$(this).attr('src','../templates/main/images/switch0.png')></a>";} else {$comments="<a onmouseup=\"$.ajax({data:'',type:'post',url:'index.php?action=2&disable_comments=true&id=".$row["id"]."'});window.location.href=window.location.href;\"><img src=../templates/main/images/switch0.png style=";$comments.=(pref("comments")!="yes")?"opacity:.5;height:28px;margin-bottom:-5px>":"height:28px;margin-bottom:-5px onmousedown=$(this).attr('src','../templates/main/images/switch1.png')>";$comments.="</a>";}
				if($row['page']=="yes") {$page="<a onmouseup=\"$.ajax({data:'',type:'post',url:'index.php?action=2&disable_page=false&id=".$row["id"]."'});window.location.href=window.location.href;\"><img src=../templates/main/images/switch1.png style=height:28px;margin-bottom:-5px onmousedown=$(this).attr('src','../templates/main/images/switch0.png')></a>";} else {$page="<a onmouseup=\"$.ajax({data:'',type:'post',url:'index.php?action=2&disable_page=true&id=".$row["id"]."'});window.location.href=window.location.href;\"><img src=../templates/main/images/switch0.png style=height:28px;margin-bottom:-5px onmousedown=$(this).attr('src','../templates/main/images/switch1.png')></a>";}
				echo "<tr>
				<td>".$row["id"]."</td>
				<td>".quickedit("orderid","width:44px;padding-left:0;padding-right:0;text-align:center;","number")."</td>";
				echo '<td>';
				echo "<select onchange=\"$.ajax({data:'id=".$row["id"]."&new_pageid='+$(this).val(),type:'POST',url:'index.php?action=".$action."'});\" style='margin-bottom:-3px;max-width:200px;'>";
				echo "<option value=0>". $text['empty'] ."</option>";
				$result2 = $db->query("select * from ".$db_prefix."pages order by title desc");
				while($row2 = $result2->fetch_assoc()) {
					echo "<option value='".$row2["id"]."' ";echo ($row2["id"]==$row["pageid"])?"selected":"";echo ">".$row2["title"]."</option>";
				}
				$result2 = $db->query("select * from ".$db_prefix."comments where articleid='".$row["id"]."'");
				echo "</td>";
				echo "<td class=important style=padding-left:15px;padding-right:10px>".quickedit("title")."</td>
				<td class=alias style=display:none;padding-left:15px;padding-right:10px>".quickedit("alias",false,false,false," required ")."</td>
				<td>".quickedit("editor","width:100px;")."</td>
				<td style=text-align:center>".date("d.m.y H:i",$row["timestamp"])."</td>
				<td><div style=display:inline-block;min-width:24px;><span class='views".$row["id"]."'>".$row["views"]."</span></div><a onmouseup=\"$.ajax({data:'',type:'post',url: 'index.php?action=".@$action."&a=reset_views&id=".$row["id"]."'});$('.views".$row["id"]."').html('0');$(this).find('img').addClass('rotatefast');setTimeout(function(){\$('.rotatefast').removeClass('rotatefast');},700);\" style='cursor:pointer;'><img src=../templates/main/images/reset.png style=height:25px;opacity:.5;margin-left:5px;margin-top:-5px;margin-bottom:-9px;></a></td>
				<td><div style=display:inline-block;min-width:18px;>".$result2->num_rows."</div><a href='?action=5&article=".$row["id"]."'><img src=../templates/main/images/comments.png style=margin-left:5px;margin-top:-5px;margin-bottom:-9px;></a></td>
				<td>".$comments."</td>
				<td>".$public."</td>
				<td>".$page."</td>
				<td>".count(@json_decode($row["attachments"],1)["attachments"])."</td>
				<td>";
				$i++;
				echo "
				<div style='white-space:no-wrap;width:110px;float:right'>
					<a href='../?article=".$row["id"]."' style=outline:none;float:right><img src=../templates/main/images/preview.png title='Preview' alt='' style=height:27px;margin:-7px;margin-left:10px;margin-right:-2px;></a>
					<a href=# onmouseup=\"if(confirm('".$text['sure']."')){\$(this).parent().parent().fadeOut(400);\$.get('?action=".@$action."&a=delete&id=".$row["id"]."');}\" style=outline:none;float:right><img title='Delete' src=../templates/main/images/delete.png alt='' style=height:27px;margin:-7px;margin-left:14px;margin-right:-2px;></a>
					<a href='?action=".@$action."&a=edit&id=".$row["id"]."' style=outline:none;float:right><img title='Edit' src=../templates/main/images/edit.png alt='' style=height:27px;margin:-7px;margin-left:12px;></a>
				</div>
				</td></tr>";
			}
			echo "</table>";
		}
	}
}
echo '
<script type="text/javascript">
$(function () {
	$(".dialog-order-articles").sortable({
	   update: function(event,ui) {
         var order=$(this).sortable("toArray").toString();
         get="reorder.php?reorder=articles&order="+order;
         $.get(get);
	   }
   });
   $(".article-attachments").sortable().find("li").css("cursor","grab");
});
</script>';